# js
## ES6
### Promise
#### Promise.all
```javascript
    let arrPro = [(()=>{})(),(()=>{})()]; // arrPro里的方法是已经执行的方法，但是因为异步没有执行完的方法;
    let arr = await Promise.all(arrPro); // 
```
