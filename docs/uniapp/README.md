# uniapp知识点
## 使用threejs等技术

renderjs: [renderjs](https://uniapp.dcloud.io/frame?id=renderjs)
```vue
<template>
  <view class="content">
    <!-- #ifdef APP-PLUS || H5 -->
    <view id="live" class="live"></view>
    <!-- #endif -->
    <!-- #ifndef APP-PLUS || H5 -->
    <view>非 APP、H5 环境不支持</view>
    <!-- #endif -->
  </view>
</template>

<script>
export default {};
</script>

<script module="live" lang="renderjs">
  // 多平台dom
  import { main } from 'utils/live2d.js'
	export default {
		mounted() {
      const canvas = document.createElement('canvas');
      document.getElementById('live').appendChild(canvas);
      this.initEcharts.bind(this)();
		},
		methods: {
			initEcharts() {
        live.children[0].style.height = "100%";
        live.children[0].style.width = "100%";
        main(live.children[0]);
			},
		}
	}
</script>

<style lang="scss" scoped>
.live {
  height: 100vh;
  background: rgba(196, 72, 0, 0.733);
}
</style>
```
