const data = require('../../config/nav')
console.log('%c 🥧 data: ', 'font-size:20px;background-color: #FCA650;color:#fff;', data);

module.exports = {
  title: "杨晓龙博客",
  description: "This is a blog.",
  base: '/knowledge/',
  dest: 'public',
  plugins: [
    'flexsearch',
    '@vuepress/active-header-links',
    '@vuepress/back-to-top',
    '@vuepress/blog'
  ],
  themeConfig: {
    nav: data,
    displayAllHeaders: true,
    sidebar: 'auto',
    lastUpdated: 'Last Updated',
    nextLinks: true,
    prevLinks: true,
    smoothScroll: true
  }
}
