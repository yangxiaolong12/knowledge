# docker的简单学习笔记
## docker简介
```
直观上的理解:
 1. docker可以快速安装环境，例如node，直接下载docker中node的镜像，你边可以使用node环境，无需
 下载，一键使用。
 2. docker可以统一环境，例如项目在本地可以运行，但是放到其他电脑或者服务器上就不可以，有可能就
 是因为环境的不同，使用docker，将自己的环境上传，别人下载，就可以一键使用相同的环境部署运行

docker本质：
 1. docker的本质类似于虚拟机，创建一个虚拟的环境去运行程序。
```
## docker镜像
### 镜像基本概念
```
  Docker 镜像 镜像是一种轻量级、可执行的独立软件包，用来打包软件运行环境和基于运行环境开发的程
  序，它包含运行这个程序的所有内容，包括代码、运行时、库、环境变量和配置文件。

  Union 文件系统是 Docker 镜像的基础。镜像可以通过分层来进行继承，基于基础镜像（没有父镜像），
  可以制作各种具体的应用镜像。
```
### 我的直观理解
```
  创建一个可以配置环境、运行代码的文件系统，这是底层，然后可以往这个底层上加东西，形成一个新的
  镜像，例如在一个什么都没有的环境里，加入node环境，做出新的镜像，然后再用这个镜像，加一个
  python环境。这样，我们有了只有Node层的镜像与node+python层的镜像
```

## docker容器

### 容器的实质
```
  容器是基于镜像产生的，容器可以被创建、启动、停止、删除、暂停，即镜像是静态的，容器启动镜像，
  但是容器不会改变原有的镜像
```
### 容器命令
```
docker ps -a
docker start name
docker stop name
docker update --restart=always name
```

## docker仓库
```
  类似于github，将自己的镜像上传，便可以让其他人直接拉下来使用。
  且同样可以建立私有库与公有库
```

## docker基本使用流程
```
  1. 获取镜像：docker pull [选项] [Docker Registry 地址[:端口号]/]仓库名[:标签]
  2. 基于该镜像启动容器: docker run [选项] [Docker Registry 地址[:端口号]/]仓库名[:标签] /bin/bash
  3. 停止容器: docker container stop
```
## 镜像的基本操作
### 获取镜像
```
命令: docker pull [选项] [Docker Registry 地址[:端口号]/]仓库名[:标签]
示例: docker pull node:latest  // 拉取最新node版本的镜像
```
### 镜像展示
```
docker image ls
  REPOSITORY TAG IMAGEID CREATED SIZE
  仓库名      标签 镜像ID 创建时间 所占用的空间
```
### 删除镜像
```
docker image rm 仓库id/仓库名

```
## 容器的基本操作
```
运行docker: docker run -t -i  ubuntu:18.04 /bin/echo 'Hello world' // -t表示进入该容器的伪终端，-i保持打开容器的标准输入
后台运行：docker run -d ubuntu:18.04 /bin/sh -c "while true; do echo hello world; sleep 1; done" // -d 表示后台运行
查看log信息： docker container logs [container ID or NAMES]
终止容器： docker container stop
重启/启动容器： docker container restart/docker container start
进入容器： docker exec -i -t [container ID] /bin/bash // 退出容器不会使容器停止
导出容器到镜像： docker export 7691a814370e > ubuntu.tar
删除容器：docker container rm [container ID or NAMES]
```

## docker学习网站推荐
    [docker入门到实践](https://vuepress.mirror.docker-practice.com/basic_concept/image/)
    [菜鸟教程](https://www.runoob.com/docker)
 
