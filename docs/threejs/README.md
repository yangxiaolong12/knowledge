## 功能实现
### MaskPass 蒙版
```javascript

import { EffectComposer } from "./jsm/postprocessing/EffectComposer.js";
import { ShaderPass } from "./jsm/postprocessing/ShaderPass.js";
import { RenderPass } from "./jsm/postprocessing/RenderPass.js";
import { MaskPass, ClearMaskPass } from "./jsm/postprocessing/MaskPass.js";
import { CopyShader } from "./jsm/shaders/CopyShader.js";

const camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1, 1000);
camera.position.z = 10;


const scene1 = new THREE.Scene();
const box = new THREE.Mesh(new THREE.BoxGeometry(4, 4, 4));
scene1.add(box);

const scene2 = new THREE.Scene();
const torus = new THREE.Mesh(new THREE.TorusGeometry(3, 1, 16, 32));
scene1.add(torus);

const renderPass = new RenderPass(scene1, camera); // 渲染通道
renderPass.clear = false; // 避免影响到其他通道

const renderer = new THREE.WebGLRenderer();
const clearPass = new ClearPass();
const clearMaskPass = new ClearMaskPass();

const maskPass1 = new MaskPass(scene1, camera) // 掩码通道

const outputPass = new ShaderPass(CopyShader) // 输出通道
const parameters = {
    minFilter: THREE.LinearFilter,
    magFilter: THREE.LinearFilter,
    format: THREE.RGBFormat,
    stencilBuffer: true,
}

const renderTarget = new THREE.WebGLRenderTarget(window.innerWidth, window.innerHeight, parameters)
const composer = new EffectComposer(renderer, renderTarget)
composer.addPass(clearPass)
composer.addPass(maskPass1) // 开启
composer.addPass(renderPass) // 只会显示到掩码区域内
composer.addPass(clearMaskPass) // 关闭
composer.addPass(outputPass) 
```
### 参数
#### 材质
```javascript
let material = new THREE.MeshBasicMaterial({
    skinning: true, //允许蒙皮动画
    morphTargets: true, // 顶点动画
    side: THREE.DoubleSide, //两面可见
    transparent: true, // 透明
    wireframe: true,  // 实现三角面
    map: switchWireframe ? false : texture, //设置纹理贴图
    alphaTest: 0.3, // 0~1 像素点透明度小于0.3不显示
}); //材质对象
```
#### 渲染
```javascript
new THREE.WebGLRenderer({
    antialias: true,
    logarithmicDepthBuffer: true,
    alpha: true,
});
```
