# python学习
## 快速安装cv
pip install opencv-python -i https://pypi.tuna.tsinghua.edu.cn/simple

## 特别写法
    a if 0 > 1 else b # 三元
    [0 for i in range(10) for i in range(20)] # 20*10的二维数组
    arr[::-1] #数组取反
## numpy
### zeros_like
    修改数组的属性
    np.zeros_like(arr, dtype = np.uint8)

## opencv

```python
import numpy as np
import cv2 as cv

img_bgr = cv.imread('2.png', 0)  # 读取图片
addRange = 50  # 图片向外增加的大小
clipRange = int(addRange / 2)  # 图片处于的位置
height = len(img_bgr)  # 图片高度
width = len(img_bgr[3])  # 图片宽度
zWidth = width+addRange  # 扩展标点
zHeight = height+addRange  # 扩展标点
clipNum = 30  # 切线数量
idotTop = []  # 上方
idotBottom = []  # 下方数组
idotCenter = []  # 中间点数组
outNum = 10  # 所有指出得点外扩值


# 标记点
def toWhite(z, x, y):
    z[y][x] = 255

# 展示图片
def showImage(z):
    cv.namedWindow('image', 0)
    cv.imshow("image", z)
    cv.waitKey(0)
    cv.destroyAllWindows()

# 制作空白图片, 用以标点
def nullImage():
    bigImg = np.ones((zHeight, zWidth), dtype=np.uint8)
    return np.zeros_like(bigImg, dtype=np.uint8)

# 将图片放到空白图片中间
def extendImage(img_bgr, z):
    for yIndex in range(0, zHeight):
        for xIndex in range(0, zWidth):
            if yIndex >= clipRange and yIndex < clipRange + height and xIndex >= clipRange and xIndex < clipRange+width:
              z[yIndex][xIndex] = img_bgr[yIndex-clipRange][xIndex-clipRange]


# 补充左上、左下、右上、右下的侧边点
def writePointLine(equa, x1, x2, pos):
    pointBet = width / clipNum
    x = x1 + pointBet
    slideArr = []
    while(int(x) < int(x2)):
        y = equa[0]*x + equa[1]
        toWhite(z, int(round(x)), int(round(y)))
        slideArr.append([int(round(x)), int(round(y))])
        x += pointBet

    if pos == 'leftTop':
        idotTop.reverse()
        idotTop.extend(slideArr)
        idotTop.reverse()
    elif pos == 'leftBottom':
        idotBottom.reverse()
        idotBottom.extend(slideArr)
        idotBottom.reverse()
    elif pos == 'rightTop':
        idotTop.extend(slideArr)
    elif pos == 'rightBottom':
        idotBottom.extend(slideArr)

#　合并上中下三个数组为一个
def linkArr():
    return idotTop + idotBottom[::-1] + idotCenter

# 两点生成一元二次方程
def lineEqua(point1, point2):
    k = (point1[1] - point2[1]) / (point1[0] - point2[0])
    b = point1[1] - point1[0]*k
    return [k, b]

# 获取侧边点，添加到数组中
def getSlideDot(z):
    centerEnd = idotCenter[len(idotCenter)-1]
    centerStart = idotCenter[0]
    cOutLeft = [centerStart[0] - outNum, centerStart[1]]
    cOutRight = [centerEnd[0] + outNum, centerEnd[1]]

    idotTopStart = idotTop[0]
    idotBottomStart = idotBottom[0]
    idotTopEnd = idotTop[len(idotTop)-1]
    idotBottomEnd = idotBottom[len(idotTop)-1]

    lineEqua1 = lineEqua(idotTopStart, cOutLeft)
    writePointLine(lineEqua1, cOutLeft[0], idotTopStart[0], 'leftTop')

    lineEqua2 = lineEqua(idotBottomStart, cOutLeft)
    writePointLine(lineEqua2, cOutLeft[0], idotTopStart[0], 'leftBottom')

    lineEqua3 = lineEqua(idotTopEnd, cOutRight)
    writePointLine(lineEqua3, idotTopEnd[0], cOutRight[0], 'rightTop')

    lineEqua4 = lineEqua(idotBottomEnd, cOutRight)
    writePointLine(lineEqua4, idotTopEnd[0], cOutRight[0], 'rightBottom')

    toWhite(z, centerStart[0] - outNum, centerStart[1])
    idotTop.append(cOutLeft)
    idotCenter.append(cOutLeft)

    toWhite(z, centerEnd[0] + outNum, centerEnd[1])
    idotCenter.insert(0, cOutRight)
    idotTop.insert(0, cOutRight)

    return linkArr()


# 获取竖线上两边缘点
def findTopBottom(z, xStart, typeP):
    if typeP == "bottom":
        r = range(0, zHeight)
    else:
        r = range(zHeight-1, -1, -1)
    for i in r:
        if z[i][xStart] != 0:
            return int(i)


# 获取竖线上两边缘点的中间点
def getCenter(y1, y2):
    return int(round((y2 - y1)/2 + y1))

#描点
def getTouchDot(z):
    xclipStart = clipRange
    xclipRange = width / clipNum
    for i in range(1, clipNum):
        xPos = int(xclipStart + xclipRange * i)
        yTop = findTopBottom(z, xPos, 'top')
        idotTop.append([xPos, yTop + outNum])
        toWhite(z, xPos, yTop + outNum)

        yBottom = findTopBottom(z, xPos, 'bottom')
        idotBottom.append([xPos, yBottom - outNum])
        toWhite(z, xPos, yBottom - outNum)

        yCenter = getCenter(yTop, yBottom)
        idotCenter.append([xPos, yCenter])
        toWhite(z, xPos, yCenter)

    return getSlideDot(z)


z = nullImage()  # 空白图片
extendImage(img_bgr, z)  # 图片插入空白图片中
getTouchDot(z)  # 描点
showImage(z)  # 展示图片
```
