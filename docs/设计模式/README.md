# 设计模式

## 单例设计模式

```typescript

class Test {
    static test: Test
    
    staitc create() {
        this.test = new Test;
    }
}
Test.test
```

## 工厂方法模式

```ts
class Test {
    static create(type) {
        switch(type) {
            case 1:
                return 1;
            case 2:
                return 3;
        }
    }
}
```

## 抽象工厂模式

## 建造者模式

## 原型模式

## 享元模式

## 门面模式

## 适配器模式

## 装饰器模式

## 策略模式

## 模板方法模式

## 观察者模式

## 责任链模式

## 代码规约

## 单一职责原则

## 开闭原则

## 里氏替原则

## 接口隔离原则

## 依赖倒置原则

