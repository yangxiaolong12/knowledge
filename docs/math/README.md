# 数学知识点

### 求点到曲线的最短距离
![image.png](./image.png)

### catmullRom 样条插值法
```javascript
class CatmullRom {
  points = [];
  constructor(points) {
    this.points = points;
    this.points.push([0,0]);
    this.points.unshift([1,1]);
  }

  addSE() {
    const p1 = this.points[1];
    const p2 = this.points[2];
    const pe1 = this.points[this.points.length - 2];
    const pe2 = this.points[this.points.length - 3];
    function san(p1, p2) {
      return p1 > p2 ? p1 + 10 : p1 - 10;
    }
    const start = [san(p1[0], p2[0]), san(p1[1], p2[1])];
    const end = [san(pe2[0], pe1[0]), san(pe2[1], pe1[1])];
    this.points[this.points.length-1] = end;
    this.points[0] = start;
  }
  getAllArr() {
    this.addSE();
    const points = this.points;
    if (points == null || points.length <= 0) return;
    const arr = [];
    for (let i = 0; i < points.length - 1; i++) {
      if (i == 0 || i == points.length - 2) continue;
      this.getArr(arr, points[i - 1], points[i], points[i + 1], points[i + 2]);
    }
    return arr;
  }

  getArr(arr, ...data) {
    const {
      a: ax,
      b: bx,
      c: cx,
      d: dx,
    } = this.getABCD(data[0][0], data[1][0], data[2][0], data[3][0]);
    const {
      a: ay,
      b: by,
      c: cy,
      d: dy,
    } = this.getABCD(data[0][1], data[1][1], data[2][1], data[3][1]);
    for (let i = 0; i < 100; i++) {
      arr.push([
        this.getResult(i * 0.01, ax, bx, cx, dx) / 2,
        this.getResult(i * 0.01, ay, by, cy, dy) / 2,
      ]);
    }
  }

  getABCD(p01, p0, p1, p2) {
    return {
      a: this.A(p01, p0, p1, p2),
      b: this.B(p01, p0, p1, p2),
      c: this.C(p01, p0, p1, p2),
      d: this.D(p01, p0, p1, p2),
    };
  }

  getResult(t, a, b, c, d) {
    return a * t ** 3 + b * t ** 2 + c * t + d;
  }

  A(p01, p0, p1, p2) {
    return -p01 + 3 * p0 - 3 * p1 + p2;
  }
  B(p01, p0, p1, p2) {
    return 2 * p01 + -5 * p0 + 4 * p1 + -1 * p2;
  }
  C(p01, p0, p1, p2) {
    return p1 - p01;
  }
  D(p01, p0, p1, p2) {
    return 2 * p0;
  }
}

const points = [
  [50, 50],
  [100, 70],
  [80, 140],
  [100, 200],
  [160, 180],
  [200, 200],
  [200, 300],
  [300, 200],
  [400, 400],
  [500, 300],
];

let test = new CatmullRom(points);
const arr = test.getAllArr();
console.log(
  "%c 🍦 arr: ",
  "font-size:20px;background-color: #2EAFB0;color:#fff;",
  arr
);
```
```

function CatmullRom( t, p0, p1, p2, p3 ) {

	const v0 = ( p2 - p0 ) * 0.5;
	const v1 = ( p3 - p1 ) * 0.5;
	const t2 = t * t;
	const t3 = t * t2;
	return ( 2 * p1 - 2 * p2 + v0 + v1 ) * t3 + ( - 3 * p1 + 3 * p2 - 2 * v0 - v1 ) * t2 + v0 * t + p1;

}

//

function QuadraticBezierP0( t, p ) {

	const k = 1 - t;
	return k * k * p;

}

function QuadraticBezierP1( t, p ) {

	return 2 * ( 1 - t ) * t * p;

}

function QuadraticBezierP2( t, p ) {

	return t * t * p;

}

function QuadraticBezier( t, p0, p1, p2 ) {

	return QuadraticBezierP0( t, p0 ) + QuadraticBezierP1( t, p1 ) +
		QuadraticBezierP2( t, p2 );

}

//

function CubicBezierP0( t, p ) {

	const k = 1 - t;
	return k * k * k * p;

}

function CubicBezierP1( t, p ) {

	const k = 1 - t;
	return 3 * k * k * t * p;

}

function CubicBezierP2( t, p ) {

	return 3 * ( 1 - t ) * t * t * p;

}

function CubicBezierP3( t, p ) {

	return t * t * t * p;

}

function CubicBezier( t, p0, p1, p2, p3 ) {

	return CubicBezierP0( t, p0 ) + CubicBezierP1( t, p1 ) + CubicBezierP2( t, p2 ) +
		CubicBezierP3( t, p3 );

}
```
