# 1. javascript 基础

## this指向问题

```javascript
1. 无论是否在严格模式下，在全局执行环境中（在任何函数体外部）this 都指向全局对象。
2. 在非严格模式下，总是指向一个对象，在严格模式下可以是任意值。
```



## 1.1 执行上下文

```javascript
//可执行的代码
1. 全局代码 2. 函数代码 3. eval代码

//this
1. setTimeout: this指向window
2. 普通函数: this指向调用他得对象
3. 箭头函数: 继承父普通函数
```

## 1.2 RegExp (正则表达式)

```javascript
let str = `1awf,awf215125,:125wdf21,125aawf`;
let reg = /\w+/ig;
----------------------------------
	reg方法
----------
1. reg.exec(str) //只匹配一个
	//["1awf"]
2. reg.test(str) //返回boolean
	//true
3. reg.toString() // \\w+\ig

-------------------------
    reg属性
------------
	reg.global == true // 设置g
	reg.ignoreCase == true // 设置i
	reg.multiline == false //设置m
	reg.source = '\w+'

-----------------------------------
        str与正则相关的方法
--------------
1. str.search(reg|'string');  //返回字符串中查找到位置的首位置 ; 没有返回0
2. str.match(reg); //["1awf", "awf215125", "125wdf21", "125aawf"]
3. str.replace(reg,(x)=>{ 
    if(x.length <= 7) { 
        return "替换" 
    }; 
    return x 
}); // "替换,awf215125,:125wdf21,替换"
4. str.split(reg); //["", ",", ",:", ",", ""]
000000000000000000000000000
	正则
00000000000000
function checkPhone(){
    if(!(/^1[345678]\d{9}$/.test(phone))){
        alert("手机号码有误，请重填");
        return false;
    }
}
```

| Reg方法     | 作用         |
| ----------- | ------------ |
| .exec(str)  | 只匹配一个值 |
| .test(str)  | 返回boolean  |
| .toString() |              |

| str上Reg的相关方法                | 作用                     |
| --------------------------------- | ------------------------ |
| str.search(reg)                   | 返回查找到字符串的首位置 |
| str.match(reg)                    | 返回匹配到的数组         |
| str.replace(reg, function(a,b){}) | 替换                     |
| str.split(reg);                   |                          |



## 1.3 模块化

```javascript
0000000000000000000000000000
	模块化规范
00000000000000
	111111111111111111111111111
		commonJS(NodeJs)
	1111111111111
	111111111111111111111111111
		AMD ( RequireJs)
	1111111111111
	111111111111111111111111111
		CMD
	1111111111111
	111111111111111111111111111
		ES6 Module
	1111111111111
000000000000000000000000000
	require和import的区别
00000000000000
	require/exports 是运行时动态加载，import/export 是静态编译；
    require/exports 输出的是一个值的拷贝，import/export 模块输出的是值的引用
    111111111111111111111111111111
		require/exports
	1111111111111      
        const fs = require('fs')
        exports.fs = fs
        module.exports = fs
	11111111111111111111111111111
		import/export 
	11111111111111
		1. 使用     
            <script 
			type="module" 
			src="./test.js">
            </script>
		2. 可以在浏览器中使用
		import {default as fs} from 'fs'
        import * as fs from 'fs'
        import {readFile} from 'fs'
        import {readFile as read} from 'fs'
        import fs, {readFile} from 'fs'

        export default fs
        export const fs
        export function readFile
        export {readFile, read}
        export * from 'fs'

	
```

| 模块化规范 |
| ---------- |
| AMD        |
| CMD        |
| requireJS  |
| ES6 Module |



### 1.3.1 commonJS和ES6区别

|                    | commonJS                              | ES6                           |
| ------------------ | ------------------------------------- | ----------------------------- |
| 获得的值           | 缓存值, 获得的是拷贝                  | 获得的是引用                  |
| 是否重新赋值       | 可以重新赋值                          | 不可以重新赋值                |
| 是否对内部进行改变 | 可以修改                              | 可以修改                      |
| this指向           | undefined                             | 当前模块                      |
| 获取               | require('js')                         | import * as test from 'js'    |
| 导出               | module.exports = fs  / exports.fs =fs | export fs / export default fs |



## 1.4 Promise

```javascript
let promise1 = new Promise((resolve, reject) => {
    resolve('123');
})
    .then((data) => {
        return data;
    })
    .catch(() => {
        return null;
    });
let promise2 = new Promise((resolve, reject) => {
    resolve('25');
})
    .then((data) => {
        return data;
    })
    .catch(() => {
        return null;
    });

Promise.all([promise1, promise2]).then((values) => {
    console.log('values: ', values);
    //['123',null]
});

```

## 1.5 原型和原型链

```javascript
00000000000000000000000000000
	__proto__/prototype
00000000000000000
	所有的对象都拥有proto属性，它指向对象构造函数的prototype属性
    所有的函数都同时拥有proto和protytpe属性
函数的proto指向自己的函数实现 函数的protytpe是一个对象 所以函数的prototype也有proto属性 指向Object.prototype
0000000000000000000000000000000000
	模拟new方法
000000000000000000
function student(name) {
    this.cons = name;
} 
student.prototype.test = '678'; //创建测试方法

let a = new student('12412412');
console.log('a: ', a); //new 出的结果

function CopyNew(testFun, ...arr) { //模拟 new
    let obj = Object.create(testFun.prototype); //原型赋值
    obj.constructor(...arr); //获取function中的this
    return obj;
}
CopyNew(student);
console.log('CopyNew(student);: ', CopyNew(student, '1252523'));

```

### 1.5.1 继承

```javascript
00000000000000000000000000000000
	原型链继承
000000000000000000
	function Child(){}
	Child.prototype = new parent();
	Child.prototype.constructor = Child;

	缺点: 无法多个继承, 更改子类就会更改父类
		Child无法在继承之前给prototype赋值
00000000000000000000000000000000
	构造函数继承
000000000000000000
	function Child() {
        parent.call(this);
    }
	var child = new Child()
    只能继承父类的实例属性与方法, 无法继承prototype是的方法, 占内存
00000000000000000000000000000000
	组合继承
000000000000000000
	
00000000000000000000000000000000
	类继承
000000000000000000
```

### 1.5.2 new

```javas
当返回值是简单的基本数据类型(undefinded, 数字, 字符串, 布尔)时，则将 obj 返回作为新的对象
```



## 1.6 dom操作

### (event)=>{ } 

```javascript
event.target

返回触发事件的元素
event.currentTarget

返回绑定事件的元素#JavaScript)
```



```javascript
按照W3C的标准，先发生捕获事件，后发生冒泡事件。所有事件的顺序是：其他元素捕获阶段事件 -> 本元素代码顺序事件 -> 其他元素冒泡阶段事件
0000000000000000000000000000
	addEventListener(,,false='默认值')
00000000000000    
	1111111111111111111111111111111111
		冒泡 ( 子 -> 父 )
	1111111111111111111
        冒泡：从里面往外面触发事件，就是alert的顺序是 button、div2、div1。
        要想冒泡，就要将每个监听事件的第三个参数设置为false,也就是默认的值。
	1111111111111111111111111111111111
		捕获 ( 父 -> 子 )
	1111111111111111111
        要想捕获，就要将每个监听事件的第三个参数设置为true。
        捕获：从外面往里面触发事件，就是alert的顺序是div1、div2、button。

** 父设置true之后, 先触发父事件, 再触发孙事件 ,再触发子事件
	1111111111111111111111111111111111
		阻止冒泡, 捕获
	111111111111111
		event.stopPropagation();
	1111111111111111111111111111111111
		阻止默认事件
	11111111111111111
		event.preventDefault()

0000000000000000000000000000
	HTMLCollection和NodeList
000000000000000
	HTMLCollection, 包含的标签元素
	NodeList, 包含文本元素

000000000000000000000000000000000
	常见dom操作
00000000000000000000
    document.body
    document.getElmentById('')
    document.getElmentsByClassName('')
    document.getElmentsByTagName('')
    document.contentType
    document.characterSet
    document.documentURI
    document.images
    document.links 
    document.cookie
    Document.title
    document.createElement()
    ParentNode.append()
    ParentNode.prepend()

    document.createTextNode("Hello World");

000000000000000000000000000000000000
	重点dom操作
000000000000000000
	/**
		用于元素添加
		虚拟dom,
		文档片段
		不会引起回流
	*/
	document.createDocumentFragment();
	


1）创建新节点 createDocumentFragment() 
//创建一个DOM片段 createElement() 
//创建一个具体的元素 createTextNode() 
//创建一个文本节点 
（2）添加、移除、替换、插入 appendChild() removeChild() replaceChild() insertBefore() 

parentDiv.replaceChild(newNode, node);
parentDiv.insertBefore(newNode, node);
//在已有的子节点前插入一个新的子节点 
（3）查找 getElementsByTagName() 
//通过标签名称 getElementsByName() 
//通过元素的Name属性的值(IE容错能力较强，会得到一个数组，其中包括id等于name值的) getElementById() 
//通过元素Id，唯一性
```

## 1.7 事件循环

```javascript
1. 执行顺序
	1.1 调用栈
    		栈执行
	1.2 微任务
    		Promise, process.nextTick
	1.3 宏任务
    		script, setTimeout, setInterval



优先级
process.nextTick() > Promise.then() > setTimeout > setImmediate。
```

## 1.8 数据操作(类型判断)

### 1.8.1 数组操作

```javascript
000000000000000000000000000000000000000000000
	数组操作
00000000000000000000
	arr.map((x)=>{}) //产生新的数组, 不会改变原数组

	Math.round(x * (10**n)) / (10**n) // 保留位数的四舍五入
	Array.sort((x,y)=>{return x-y})
	111111111111111111111111
		判断数据类型
	1111111111111111        
    Object.prototype.toString.call([]) === '[object Array]'//改变toString的this
    [] instanceof Array
    [].constructor === Array
    Array.isArray([])

    function getClass (a) {
      const str = Object.prototype.toString.call(a)
      return str.match(/\w*(?=\])/g)[0]
    }
```

### 1. Array

```javascript
let arr = [];
--------------------------------------------------------------
	Array的遍历方法
---------------------------
    arr.forEach(function (item, index, array) {
		console.log(item, index)
	})  //item每个元素，index每个元素下角标，array整个数组
--------------------------------------------------------------
	Array数据操作（修改自身）
--------------------------
	arr.push('123','456') // ['123', '456']
	arr.pop() // ['123']
	arr.unshift('789') //['798', '123']
	arr.shift() //['123']
	arr.splice(arr.indexOf('123'),1) // '123' 删除元素 arr == []
	arr.copyWithin(); //覆盖
	arr.fill('1'); //填充
	arr.reverse();
----------------------------------------------------------------
	Array数据操作
---------------------------------        
	arr.slice() //产生新的数组 arr.slice(0,1) 获取第一个
	new Array(...arr)
	new Array(length)
---------------------------------------------------
	Array自身方法        
----------------
	Array.form() //创建新数组
	Array.isArray() //判断是否是数组
	Array.of() //
------------------------------------------------------
	Array不修改自身
----------------------------
	arr.concat() //连接
	arr.includes() //返回true false
	arr.join(',') //转字符串
	arr.lastIndexOf()
----------------------------------------------------------
	Array遍历
-----------------------
	arr.forEach((x)=>{ return x }) //遍历
	arr.entries()
	arr.every() //是否每个都符合
	arr.some() //至少一个满足
	arr.filter((x)=>{ return x })
	arr.find()
	arr.findIndex()
	arr.map() //新数组
	arr.reduce() //求和 积
	arr.reduceRight() //从右往左
	arr.values() //所有的值
```

### 2. Date

```javascript
const date1 = new Date('December 17, 1995 03:24:00');
const date2 = new Date('1995-12-17T03:24:00');
var today = new Date();
var birthday = new Date('December 17, 1995 03:24:00');
var birthday = new Date('1995-12-17T03:24:00');
var birthday = new Date(1995, 11, 17);
var birthday = new Date(1995, 11, 17, 3, 24, 0);


Date.now()//返回时间标准号描述
Date.UTC()

date1.getDate() //天
date1.getDay() //星期
date1.getFullYear() //年份
date1.getHours() //小时
date1.getMinutes() //分钟
date1.getMonth() //月份
date1.getSeconds() //秒数

new Date().toDateString()//"Tue Jul 21 2020"
new Date().toJSON() // "2020-07-21T15:40:31.316Z"
new Date().toGMTString()  //"Tue, 21 Jul 2020 15:41:18 GMT"
new Date().toLocaleDateString() //"2020/7/21"
new Date().toLocaleString() //"2020/7/21 下午11:42:09"
new Date().toLocaleTimeString() //"下午11:42:28"
new Date().toString() //"Tue Jul 21 2020 23:42:50 GMT+0800 (中国标准时间)"

```

### 3. Boolean

```javascript
Boolean(NaN) //false
```

### 4. Function

```javascript
let fnc = function () { console.log(this.name) };
fnc.apply({ name:1 }) //1
fnc.bind({ name:1 }) //1
fnc.call({ name:1 }) //1

const adder = new Function("a", "b", "return a + b");
```

### 5. Generator

```javascript
function* idMaker(){
    let index = 0;
    while(true)
        yield index++;
}
let gen = idMaker(); // "Generator { }"

console.log(gen.next().value); 
// 0
console.log(gen.next().value); 
// 1
console.log(gen.next().value); 
// 2
```

### 6. JSON

```javascript
JSON.parse()
JSON.stringify()
```

### 7. Map

```javascript
let test = new Map([[1,51],[1,521]])
test.clear();
test.delete(1);
test.entries();
test.foreach((x,y)=>{});
test.get(key);
test.has(key);
test.keys();
test.set(key, value);
```

### 8. promise

```javascript
----------------------------------
Promise.all();
--------------
const promise1 = Promise.resolve(3);
const promise2 = 42;
const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'foo');
});

Promise.all([promise1, promise2, promise3]).then((values) => {
  console.log(values);
});
// expected output: Array [3, 42, "foo"]

----------------------------------
Promise.allSettled();
---------------
const promise1 = Promise.resolve(3);
const promise2 = new Promise((resolve, reject) => setTimeout(reject, 100, 'foo'));
const promises = [promise1, promise2];

Promise.allSettled(promises).
  then((results) => results.forEach((result) => console.log(result.status)));

// expected output:
// "fulfilled"
// "rejected"
----------------------------------
Promise.any();
----------------------------------
Promise.race();
-------------
    const promise1 = new Promise((resolve, reject) => {
  setTimeout(resolve, 500, 'one');
});

const promise2 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, 'two');
});

Promise.race([promise1, promise2]).then((value) => {
  console.log(value);
  // Both resolve, but promise2 is faster
});
// expected output: "two"

----------------------------------
Promise.reject();
-----------
    function resolved(result) {
  console.log('Resolved');
}

function rejected(result) {
  console.error(result);
}

Promise.reject(new Error('fail')).then(resolved, rejected);
// expected output: Error: fail
----------------------------------
Promise.resolve();
------------------
const promise1 = Promise.resolve(123);

promise1.then((value) => {
  console.log(value);
  // expected output: 123
});

```

### 9. Number

```javascript
isNaN() //判断是否不为数字或者不为null
Number.isNaN() //判断是否是NaN
确定传递的值是否是 NaN。
```

### 10. Object

```javascript

```

### 11. Error

```javascript

```

### 12. new

```javascript
new 运算符创建一个用户定义的|对象类型的实例|或具有|构造函数|的内置对象的实例。
Object.create()
Reflect.construct()
```

### 13. this

```javascript
在箭头函数中，this与封闭词法环境的this保持一致。在全局代码中，它将被设置为全局对象：
//封闭词法环境
词法环境是 整个程序 或 函数调用时函数 或 代码块 关联的一个对象, 也是JavaScript实现作用域和作用域链的具体机制

闭包的定义: 函数保存其外部的变量并且能够访问它们称之为闭包
解释 JavaScript 中所有函数都是闭包
关于词法环境原理和 [[Environment]] 属性的技术细节
```

### 14. regExp

```javascript
let re = /(\w+)\s(\w+)/;
let str = "John Smith";
let newstr = str.replace(re, "$2, $1");
console.log(newstr);
```

### 15. String

```javascript
String.fromCharCode(65, 66, 67);   // 返回 "ABC"
console.log(String.fromCodePoint(9731, 9733, 9842, 0x2F804));
// expected output: "☃★♲你"

let str = '123';
str.concat('123')
str.includes('2'); //true
str.endsWith('3') //true
str.indexOf()
str.lastIndexOf()
str.localeCompare()
str.match() //正则
str.replace() //正则
str.padEnd() //填充
str.padStart() //填充 a.padStart(8,0) 补0
str.repeat(2) //123123
str.slice()
str.split()
str.toUpperCase() //大写
str.toLowerCase() //小写
str.trim() //去除头尾空格
```

### 16. Object

```javascript
Object.assign()
//通过复制一个或多个对象来创建一个新的对象。
Object.create()
//使用指定的原型对象和属性创建一个新对象。
Object.defineProperty()
//给对象添加一个属性并指定该属性的配置。
Object.defineProperties()
//给对象添加多个属性并分别指定它们的配置。
Object.entries()
//返回给定对象自身可枚举属性的 [key, value] 数组。
Object.freeze()
//冻结对象：其他代码不能删除或更改任何属性。
Object.getOwnPropertyDescriptor()
//返回对象指定的属性配置。
Object.getOwnPropertyNames()
//返回一个数组，它包含了指定对象所有的可枚举或不可枚举的属性名。
Object.getOwnPropertySymbols()
//返回一个数组，它包含了指定对象自身所有的符号属性。
Object.getPrototypeOf()
//返回指定对象的原型对象。
Object.is()
//比较两个值是否相同。所有 NaN 值都相等（这与==和===不同）。
Object.isExtensible()
//判断对象是否可扩展。
Object.isFrozen()
//判断对象是否已经冻结。
Object.isSealed()
//判断对象是否已经密封。
Object.keys()
//返回一个包含所有给定对象自身可枚举属性名称的数组。
Object.preventExtensions()
//防止对象的任何扩展。
Object.seal()
//防止其他代码删除对象的属性。
Object.setPrototypeOf()
//设置对象的原型（即内部 [[Prototype]] 属性）。
Object.values()
//返回给定对象自身可枚举值的数组。

Object.prototype.hasOwnProperty()
//返回一个布尔值 ，表示某个对象是否含有指定的属性，而且此属性非原型链继承的。
Object.prototype.isPrototypeOf()
//返回一个布尔值，表示指定的对象是否在本对象的原型链中。
Object.prototype.propertyIsEnumerable()
//判断指定属性是否可枚举，内部属性设置参见 ECMAScript [[Enumerable]] attribute 。
Object.prototype.toSource() 
//返回字符串表示此对象的源代码形式，可以使用此字符串生成一个新的相同的对象。
Object.prototype.toLocaleString()
//直接调用 toString()方法。
Object.prototype.toString()
//返回对象的字符串表示。
Object.prototype.unwatch() 
//移除对象某个属性的监听。
Object.prototype.valueOf()
//返回指定对象的原始值。
Object.prototype.watch() 
//给对象的某个属性增加监听。
```

### 17. Math

```javascript
Math.abs(x) //绝对值
Math.floor(x) //向下取整
Math.fround(x) //浮点型
Math.max(...[x[, y[, …]]]) //最大
Math.min(...[x[, y[, …]]]) //最小
Math.round(x) //四舍五入
Math.sign(x) //正负0 -1 0 1
Math.pow(2,2)  2**2
```

### 18. Document

```javascript

```

### 19. window

```javascript
00000000000000000000000000000
	window属性
000000000000
.content // 获取id为content的dom
.history // 跳转长度
	.history.back() //回跳
	.history.forward() //前进
.innerHeight // 包含水平滚动条
.innerWidth // 包含垂直滚动条
.location // 获取当前url
.navigator // 浏览器属性
.screen // 屏幕对象
.screenX // 屏幕对象
.screenY // 屏幕对象

0000000000000000000000000000000000000
	window方法
00000000000000000    
.close();
.alert();
.confirm();
.prompt();

.moveTo(x,y); //移动窗口 绝对位置
.moveBy();  //相对位置
.resizeBy(); //相对改变
.resizeTo(); //绝对改变
.scroll(); //滚动 
.scrollTo();
```



### 20. history

| history属性                          | 值          | 作用                 |
| ------------------------------------ | ----------- | -------------------- |
| .length                              |             | 会话历史中元素的数目 |
| .scrollRestoration *默认滚动恢复行为 | auto manual | 自动还原 不还原      |
| .state                               |             | 历史堆栈顶部的状态值 |

| history方法     | 值                                                           | 作用                                                         |
| --------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| .back()         |                                                              | 前往上一页 === .go(-1)                                       |
| .forward()      |                                                              | 前往下一页 === .go(1)                                        |
| .go()           |                                                              | 前往某一页                                                   |
| .pushState()    | const state = { 'page_id': 1, 'user_id': 5 } <br/>const title = '' <br />const url = 'hello-world.html'  <br />history.pushState(state, title, url) | 通过设置state,title和url创建一条新的历史记录。               |
| .replaceState() | history.replaceState(stateObj, title[, url]);<br />var stateObj = { foo: "bar" }; history.pushState(stateObj, "", "bar.html"); | 修改当前历史记录实体，如果你想更新当前的state对象或者当前历史实体的URL来响应用户的的动作的话这个方法将会非常有用。 |



### 21. location(前端路由)

| location       |      |      |
| -------------- | ---- | ---- |
| hash(前端路由) |      |      |
| host           |      |      |
| href           |      |      |



## 1.9 ES6

### 1.9.1 let const var

```javascript
00000000000000000000000000000000000
	let const var
00000000000000000
	var 全局变量, 可以重复定义, 具有变量提升
    let 局部变量, 拥有块级定义域, 有暂时性死区,无变量提升, 不可以重复定义
    const let的基础上, 必须赋初值, 值不可修改 
    不可window获取
    
    { 
        var i=0; setTimeout(()=>{console.log(i),10}); 
     {
         var i =1; setTimeout(()=>{console.log(i),10}); 
         { 
             var i=2; setTimeout(()=>{console.log(i),10}) 
         } 
     } 
    } // 2 2 2
	/**
	* 拥有函数定义域，不拥有块级定义域，导致i 只有一个
	* 而for 的循环就是块级定义域
	*/
	{
        let i=0; setTimeout(()=>{console.log(i),10}); 
     {
         let i =1; setTimeout(()=>{console.log(i),10}); 
         { 
             let i=2; setTimeout(()=>{console.log(i),10}) 
         } 
     } 
    } // 0 1 2
	/**
	* 拥有函数定义域，拥有块级定义域，这是三个独立的i
	* 而for 的循环就是块级定义域
	*/



```

### 1.9.2  promise

```javascript

0000000000000000000000000000000000000
	promise
00000000000000000000    
let promise1 = new Promise((resolve, reject) => {
  resolve('123');
})
  .then((data) => {
    //返回的值会给到 .all
    return data;
  })
  .catch(() => {
    return null;
  });
let promise2 = new Promise((resolve, reject) => {
  reject('25');
})
  .then((data) => {
    return data;
  })
  .catch(() => {
    return '132';
  });

/**
 * 不管promise1/2 是否resolve, 只要有返回值即可
 */
Promise.all([promise1, promise2]).then((values) => {
  console.log('values: ', values); // ['123', '132']
});
```



### 1.9.3 proxy

```javascript

00000000000000000000000000000000000000000
	proxy
000000000000000000000000
let a: any = function () {};
a.prototype.testP1 = 'P1';
a.prototype.testP2 = 'P2';
let obj = new a();
console.log('obj: ', obj);
var proxy = new Proxy(obj, {
  /**
   * 得到触发 
   * @param target
   * @param propKey
   * @param receiver
   */
  get(target, propKey, receiver) {},
  /**
   * 设置触发
   * @param target
   * @param propKey
   * @param value
   * @param receiver
   */
  set(target, propKey, value, receiver) {
    return true;
  },
  /**
   * 是否包含
   * @param target
   * @param propKey
   */
  has(target, propKey) {
    return true;
  },
  /**
   * 删除
   * @param target
   * @param propKey
   */
  deleteProperty(target, propKey) {
    return true;
  },
  /**
   * 获取键名
   * 拦截Object.getOwnPropertyNames();
   * 拦截Object.getOwnPropertySymbols(rpxy)
   * Object.keys(proxy)
   * for...in //原型上的键名同样获取
   * 获取name list
   * @param target
   */
  ownKeys(target) {
    return [];
  },
  /**
   * 获取 值 与 值是否可写与枚举
   * 拦截 Object.getOwnPropertyDescriptor(target,proxyKey)
   * @param target
   * @param propKey
   */
  getOwnPropertyDescriptor(target, propKey) {
    return undefined;
  },
  /**
   * 设置对象上的值, 并且可以设置是否可写
   * 拦截Object.defineProperty(proxy, propKey, propDesc)
   * @param target
   * @param propKey
   * @param propDesc
   */
  defineProperty(target, propKey, propDesc) {
    return true;
  },
  /**
   * 阻止扩展
   * 拦截Object.preventExtensions(proxy)
   * @param target
   */
  preventExtensions(target) {
    return true;
  },
  /**
   * 获取原型上的值
   * 拦截Object.getPrototypeOf(proxy)
   * @param target
   */
  getPrototypeOf(target) {
    return {};
  },
  /**
   * 判断是否可以扩展
   * 拦截Object.isExtensible(proxy)
   * @param target
   */
  isExtensible(target) {
    return true;
  },
  /**
   * 设置原型上的值
   * 拦截Object.setPrototypeOf(proxy, proto)
   * @param target
   * @param proto
   */
  setPrototypeOf(target, proto) {
    return true;
  },
  /**
   * 拦截
   * proxy(...args)、
   * proxy.call(object, ...args)、
   * proxy.apply(...)
   * @param target
   * @param object
   * @param args
   */
  apply(target, object, args) {},
  /**
   * new proxy(...args)
   * @param target
   * @param args
   */
  construct(target, args) {
    return {};
  },
});
```

### 1.9.4 解构赋值

```javascript
00000000000000000000000000000000000
	极其有用
00000000000000000000000
	冒泡替换: [a, b] = [b, a];
1. 数组解构
var [one, two] = ['one', 'two'] ;
console.log(one,two)//onetwo
2.对象解构
var {p, q} = {p: 42, q: true}; //p 42; q true
3. 已声明对象赋值
var a, b;
({a, b} = {a: 1, b: 2}); //a 1;b 2;
*****4. 第二种对象解构
var o = {p: 42, q: true};
var {p: foo, q: bar} = o; //foo 42; bar true;
5. 赋初值
var {a = 10, b = 5} = {a: 3}; //a 3; b 5;
*****6. 传参默认值
function drawES2015Chart({size = 'big', cords = { x: 0, y: 0 }, radius = 25} = {}) 
{
  console.log(size, cords, radius);
  // do some chart drawing
}

drawES2015Chart({
  cords: { x: 18, y: 30 },
  radius: 30
});
7. 非标识符
let key = "z";
let { [key]: foo } = { z: "bar" };
8. 原型链查找
// 声明对象 和 自身 self 属性
var obj = {self: '123'};
// 在原型链中定义一个属性 prot
obj.__proto__.prot = '456';
// test
const {self, prot} = obj;
// self "123"
// prot "456"（访问到了原型链）
```

### 1.9.5 Set/Map

```javascript
000000000000000000000000000
	Set 集合
000000000000
	1.特性
    	1. 不重复
	2.方法
    	.add(value)：新增，相当于 array里的push
        .delete(value)：存在即删除集合中value
        .has(value)：判断集合中是否存在 value
        .clear()：清空集合
    3.转数组
    	Array.from(Set)
		[...Set]
	4.遍历方法
    	keys()：返回一个包含集合中所有键的迭代器

        values()：返回一个包含集合中所有值得迭代器

        entries()：返回一个包含Set对象中所有元素得键值对迭代器
		forEach(callbackFn, thisArg)：用于对集合成员执行callbackFn操作，如果提供了 thisArg 参数，回调中的this会是这个参数，没有返回值
        for of;
	11111111111111111111111111111111
		weakSet
	111111111111111   
		1. 不能是直接值, 必须是引用
		2. 无法遍历
         3. 成员可能被垃圾回收
         
00000000000000000000000000000000000000
	Map 字典
0000000000000000    
	集合 是以 [value, value]的形式储存元素，字典 是以 [key, value] 的形式储存
    const map = new Map([
      ['name', 'An'],
      ['des', 'JS']
    ]);
    1. 方法
    	1. set(key, value)：向字典中添加新元素
        2. get(key)：通过键查找特定的数值并返回
        3. has(key)：判断字典中是否存在键key
        4. delete(key)：通过键 key 从字典中移除对应的数据
        5. clear()：将这个字典中的所有元素删除
	2. 属性
    	1. size 2
	3. 遍历方法
    	1. Keys()：将字典中包含的所有键名以迭代器形式返回
        2. values()：将字典中包含的所有数值以迭代器形式返回
        3. entries()：返回所有成员的迭代器
        4. forEach()：遍历字典的所有成员
```

### 1.9.6 class

```javascript
class 
	1. 声明会提升, 但不会初始化赋值(和let const一样)
	2. 内部会启用严格模式
	3. 的所有方法都是不可枚举的
```



## 1.9.1 箭头函数

```javascript

```

## 1.9.2 闭包

```javascript

```

## 1.9.3 数据类型

### 1.9.3.1 Object

```javascript
let a = function () { this.test1="1";this.test2="2" };
a.prototype.testP1 = "P1";
a.prototype.testP2 = "P2";
let obj = new a;
/**
* 获取key值
*/
Object.getOwnPropertyNames(obj) // ["test1", "test2"]
Object.keys(obj) // ["test1", "test2"]
for(let x in obj) { console.log(x) } //test1, test2, testP2, testP1
/**
* 根据键获取值
*/
Object.getOwnPropertyDescriptor(obj,'test1') // {value:1,....}
/**
* 设置键与值, 设置可写, 可枚举
* 设置到对象上, 不是原型
* 设置的键, 修改时会被拦截
*/
Object.defineProperty(obj, 'name', {
        enumerable: true,
        configurable: true,
        v: '',
        get(value) {
            console.log(value)
            console.log('get执行了')
            return this.v
        },
        set(newValue) {
            console.log('set方法执行了');
            console.log(newValue)
            this.v = newValue
            return newValue
        }
    })
/**
* 不能增加新的键
*/
Object.preventExtensions(obj) ;
/**
* 判断是否可扩展
*/
Object.isExtensible(obj);
/**
* 获取原型链上的值
*/
Object.getPrototypeOf(obj) // {testP2: "P2", testP1: "P1", constructor: ƒ}
/**
* 修改原型
*/
Object.setPrototypeOf(obj1, { testP3: "P3"}) 
```

### 1.9.3.2 类数组

```javascript
var obj = { 
    0: 'a', 
    1: 'b', 
    length:2, 
    push: Array.prototype.push
}
obj.push('c'); 
/*
	{ 
        0: 'a', 
        1: 'b', 
        2: 'c'
        length:3, 
        push: Array.prototype.push
    }
*/
obj.length; //3
obj[3]; // c
```



## 1.9.4 正则表达式

```javascript
//匹配URL
let reg = /(?<=\?|&)(\w+)=(\w*)/g
str.match();
reg.exec();
var pattern = /^([A-Za-z0-9_\-\.\u4e00-\u9fa5])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,8})$/;
```

## 1.9.5 this

```javascript
/**
* 普通函数this
*/
function a() {
    this //继承定义时, 所处的对象
}
/**
* 箭头函数this
*/
let a = ()=>{
    this //指向, 父函数的this, 没有就是windows
}
/**
* 定时器中this
*/
setTimeout(function(){ this/*指向window*/ },10)
```

## 1.9.6 节流和防抖

```javascript
/**
 * 闭包实现防抖和节流
 */
let btn = document.createElement('button');
let btn2 = document.createElement('button');

function fncTest() {
  console.log('fncTest');
}
/**
 * 防抖
 * 短时间内一直点击, 只会触发一次
 */
function antiShake(
  fnc: { (test: string): void; (): void },
  duration: number | undefined,
) {
  let time: number | undefined;
  return function (target: any) {
    if (!time) {
      fnc();
    }
    clearTimeout(time);
    time = setTimeout(() => {
      time = undefined;
    }, duration);
  };
}
let den = antiShake;
/**
 * 节流
 * 短时间内一直点击, 按间隔时间逐渐触发
 */
function throttling(
  fnc: { (test: string): void; (): void },
  duration: number | undefined,
) {
  let time: number | null;
  return function (target: any) {
    if (!time) {
      fnc();
      time = setTimeout(() => {
        time = null;
      }, duration);
    }
  };
}
btn.addEventListener('click', antiShake(fncTest, 1000));
btn.innerText = 'antiShake';
document.body.append(btn);

btn2.addEventListener('click', throttling(fncTest, 1000));
btn2.innerText = 'throttling';
document.body.append(btn2);

```

## 1.9.7 restFul(post、get等区别)

```javascript
类型有：get、post、delete、put、head、option
/**
* post/get区别
*/
post请求: 不会缓存, 用户直接是看不见的, 没有长度限制
```

|          | post                     | get                 | head             |      |
| -------- | ------------------------ | ------------------- | ---------------- | ---- |
| 组成     | 请求头、请求行、请求体   | 请求头、请求行      | 响应头           |      |
| 用途     | 提交信息                 | 查询                | 检查文件是否存在 |      |
| 缓存     | 请求放到请求体，不会缓存 | 放到URL后会被缓存   |                  |      |
| 长度限制 | 没有                     | 有限制,由浏览器决定 |                  |      |

### 1.9.7.1 请求响应头

Http报头分为通用报头，请求报头，响应报头和实体报头。

请求方的http报头结构：通用报头|请求报头|实体报头

响应方的http报头结构：通用报头|响应报头|实体报头

| 请求头       | 内容                                                         | 作用                                               | 类型   |
| ------------ | ------------------------------------------------------------ | -------------------------------------------------- | ------ |
| Accept       | text/html，image/*，text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8 | 告知客户端可以处理的内容类型                       | 请求头 |
| Content-type | text/html;                                                   | 服务器使用 Content-Type 应答头通知客户端它的选择。 | 实体头 |
|              |                                                              |                                                    |        |
|              |                                                              |                                                    |        |
|              |                                                              |                                                    |        |



## 1.9.8 xmlHttpRequest

```javascript
0000000000000000000000000000000000000
	请求
0000000000000000000000
	组成 - 请求行+请求头+请求体
		1. 请求行: restful+请求连接+HTTP协议版本
		2. 请求头: accept等各种头
		3. 请求体: 请求的数据
0000000000000000000000000000000000000
	响应
0000000000000000000000
	组成 - 响应头 + 响应体

```

https://blog.csdn.net/fuxiaohui/article/details/72725500

| XMLHttpRequest 属性 | 作用                                        |                                                              |
| ------------------- | ------------------------------------------- | ------------------------------------------------------------ |
| .onreadystatechange | 当readState属性发生变化时，调用EventHandler | XMLHttpRequest.onreadystatechange = callback;<br />当状态码发生变换时, 调用回调事件 |
| .readyState         | 状态                                        | https://developer.mozilla.org/zh-CN/docs/Web/API/XMLHttpRequest/readyState |
| .response           | 返回 值                                     |                                                              |
| .responseText       | 返回的数据                                  |                                                              |
| .presponseType      | 确定response类型                            | arraybuffer blob document json text                          |
| .responseURL        | 返回响应的序列化URL                         | xhr.onload = function () {   console.log(xhr.responseURL); // http://example.com/test }; |
| .responseXML        |                                             |                                                              |
| .status             | 返回状态码                                  | xhr.status                                                   |
| .statusText         |                                             |                                                              |

| XMLHttpRequest 属性                 | 作用                                                         |                                                              |
| ----------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| .timeout                            | 示该请求的最大请求时间（毫秒），若超出该时间，请求会自动终止 |                                                              |
| .ontimeout                          | 当请求超时调用的 [`EventHandler`](https://developer.mozilla.org/zh-CN/docs/Web/API/EventHandler) |                                                              |
| .upload                             | [`XMLHttpRequestUpload`](https://developer.mozilla.org/zh-CN/docs/Web/API/XMLHttpRequestUpload)，代表上传进度。 |                                                              |
| .withCredentials                    | 一个[`布尔值`](https://developer.mozilla.org/zh-CN/docs/Web/API/Boolean)，用来指定跨域 `Access-Control` 请求是否应当带有授权信息，如 cookie 或授权 header 头。 | var xhr = new XMLHttpRequest(); xhr.open('GET', 'http://example.com/', true); xhr.withCredentials = true; xhr.send(null); |
| .channel                            | 一个 `nsIChannel`，对象在执行请求时使用的通道。              |                                                              |
| .mozAnon                            | 一个布尔值，如果为真，请求将在没有 cookie 和身份验证 header 头的情况下发送。 |                                                              |
| .mozSystem                          | 一个布尔值，如果为真，则在请求时不会强制执行同源策略。       |                                                              |
| XMLHttpRequest.mozBackgroundRequest | 一个布尔值，它指示对象是否是后台服务器端的请求。             |                                                              |



| XMLHttpRequest 方法  | 作用                                                         |                                                              |
| -------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| .abort()             | 请求已被发出, 则立刻终止请求                                 |                                                              |
| .getResopnseHeader() | 返回包含指定响应头的字符串，如果响应尚未收到或响应中不存在该报头 | 获取content-type, client.getResponseHeader("Content-Type")); |
| .open()              | 初始化一个请求<br />xhrReq.open(method, url, async, user, password); | method: get,post,put,delete...<br />async 表示要不要异步     |
| .setRequestHeader()  | 此方法必须在  [`open()`](https://developer.mozilla.org/zh-CN/docs/Web/API/XMLHttpRequest/open) 方法和 [`send()`](https://developer.mozilla.org/zh-CN/docs/Web/API/XMLHttpRequest/send)  之间调用 | myReq.setRequestHeader(header, value);                       |
| .send()              |                                                              |                                                              |
|                      |                                                              |                                                              |
|                      |                                                              |                                                              |
|                      |                                                              |                                                              |
|                      |                                                              |                                                              |

| XMLHttpRequest 事件                                          | 作用 |      |
| ------------------------------------------------------------ | ---- | ---- |
| https://developer.mozilla.org/zh-CN/docs/Web/API/XMLHttpRequest/abort_event |      |      |
|                                                              |      |      |
|                                                              |      |      |



### 1.9.8.1 ajax/axios/fetch

```javascript
000000000000000000000000000000
	ajax
000000000000000
	let xmlhttp = new XMLHttpRequest();
	let url = "https://***";
	xmlhttp.onreadystatechange = function () {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            console.log(xmlhttp.responseText);
        }
    }
	/**
		规定请求类型
	*/
	xmlhttp.open('get',url,true);  //异步
	/**
		请求发送到服务器
	*/
	xmlhttp.send();

readyState属性有五个状态值。
0：是uninitialized，未初始化。已经创建了XMLHttpRequest对象但是未初始化。
1：是loading.已经开始准备好要发送了。
2：已经发送，但是还没有收到响应。
3：正在接受响应，但是还不完整。
4：接受响应完毕。
responseText：服务器返回的响应文本。只有当readyState>=3的时候才有值，根据readyState的状态值，可以知道，当readyState=3，返回的响应文本不完整，只有readyState=4，完全返回，才能接受全部的响应文本。
responseXML：response  as Dom Document object。响应信息是xml，可以解析为Dom对象。
status：服务器的Http状态码，若是200，则表示OK，404，表示为未找到。
statusText：服务器http状态码的文本。比如OK，Not Found。
000000000000000000000000000000
	axios
000000000000000
	async function getData() {
        let url = "";
        const response = await axios(url)
    }
000000000000000000000000000000
	fetch
000000000000000

    let url = "https://www.jianshu.com/p/b65c887cb317";
    const response = await fetch(url, {
        method: 'GET',
    })
    const ports = await response.json()
    console.log(ports)
```



### 1.9.8.1 跨域方式

```javascript
00000000000000000000000000000
  方式1: JSONP 
00000000000000
	使用script访问远程JS文件, 再通过回调获得数据
00000000000000000000000000000
  方式2: CORS 
00000000000000
	设置 Access-Control-Allow
							-Origin: *
							-methods: restFul
							-Header:content-type
							-Credentail: true
条件 1：使用下列方法之一：
	GET
    HEAD
	POST
条件 2：Content-Type 的值仅限于下列三者之一：
	text/plain
	multipart/form-data
	application/x-www-form-urlencoded
00000000000000000000000000000
  方式3: 降域 
00000000000000
a.yilia.com 调用 b.yilia.com
document.domain = yilia.com；
00000000000000000000000000000
  方式4: nginx反向代理 
00000000000000
server {
    listen       81;
    server_name  www.domain1.com;
    location / {
        proxy_pass   http://www.domain2.com:8080;  #反向代理
        proxy_cookie_domain www.domain2.com www.domain1.com; #修改cookie里域名
        index  index.html index.htm;

        # 当用webpack-dev-server等中间件代理接口访问nignx时，此时无浏览器参与，故没有同源限制，下面的跨域配置可不启用
        add_header Access-Control-Allow-Origin http://www.domain1.com;  #当前端只跨域不带cookie时，可为*
        add_header Access-Control-Allow-Credentials true;
    }
}
00000000000000000000000000000
  方式5: webpack的porxy代理
00000000000000
00000000000000000000000000000
  方式6: Socket.io 
00000000000000
```

### 1.9.8 设置不缓存

```javascript
1、在ajax发送请求前加上 anyAjaxObj.setRequestHeader(“If-Modified-Since”,”0”)。

2、在ajax发送请求前加上 anyAjaxObj.setRequestHeader(“Cache-Control”,”no-cache”)。 3、在URL后面加上一个随机数： “fresh=” + Math.random();。 4、在URL后面加上时间戳：”nowtime=” + new Date().getTime();。 5、如果是使用jQuery，直接这样就可以了 $.ajaxSetup({cache:false})。这样页面的所有ajax都会执行这条语句就是不需要保存缓存记录。
```







## 1.9.9深克隆\浅克隆\JSON转换

```javascript
000000000000000000000000000000000
	Object, Array, Date, RegExp, Function
	都为引用类型
00000000000000	
	深拷贝的方法有:
		1. JSON.parse(JSON.stringify())
		2. [...arr], new Array(...arr) arr.concat()
		3. Object.assign({},obj)
0000000000000000000000000
	JSON转换方式
00000000000
	11111111111111111111111111111
		转换为JSON
	111111111111111
		JSON.stringify(jsonObj);
		eval('(' + jsonStr + ')')
```

## 1.9.9.1 call/bind/apply

```javascript
call(this,can1,can2) //执行函数
apply(this,[can1,can2]) //执行函数
bind(this,can1,can2) //返回函数
```

## 1.9.9.2 设计模式

```javascript

```

## 1.9.9.3 垃圾回收

```javascript
1. 全局变量不会被垃圾回收
2. 闭包函数里的变量
3. 没有清理的dom元素
```

## 1.9.9.4 window/document(height width)

|                          | 宽高值                                      | 使用方法 |
| ------------------------ | ------------------------------------------- | -------- |
| innerHeight/innerWidth   | 视图大小                                    | window.  |
| outerHeight/outerWidth   | 包括侧边栏、窗口镶边                        | window.  |
| clientWidth/clientHeight | padding+content                             | Element. |
| offsetWidth/offsetHeight | border+padding+content                      | Element. |
| scrollWidth/scrollHieght | 包括overflow溢出的地方宽高；padding+content | Element. |
| clientTop/clientLeft     | 距离边框的宽高                              | Element. |
| offsetTop/offsetLeft     | 边框到margin的宽高                          | Element. |
| scrollTop/scrollLeft     | 读取或设置元素滚动条到元素上左边的距离      | Element. |

## 1.9.9.5 前端缓存

```javascript
1. 如果开启了Service Worker首先会从Service Worker中拿
2. 如果新开一个以前打开过的页面缓存会从Disk Cache中拿（前提是命中强缓存）
3. 刷新当前页面时浏览器会根据当前运行环境内存来决定是从 Memory Cache 还是 从Disk Cache中拿(可以看到下图最后几个文件有时候是从 Memory Cache中拿有时候是从Disk Cache中拿)
00000000000000000000000000000000
	查找过程
00000000000000
    Service Worker
    Memory Cache //内存种的缓存
    Disk Cache //硬盘上的缓存
    网络请求
000000000000000000000000000000000
	请求网络
0000000000000000000
	1. 根据 Service Worker 中的 handler 决定是否存入 Cache Storage (额外的缓存位置)。
	2. 根据 HTTP 头部的相关字段(Cache-control, Pragma 等)决定是否存入 disk cache
	3. memory cache 保存一份资源 的引用，以备下次使用。
```

### 1.9.8.1 强制缓存、协商缓存

```javascript
00000000000000000000000000000000000
	相关请求头
0000000000000000000
	cache-control字段：
		no-cache: 不使用缓存
        no-store: 不会缓存到临时文件
        max-age=delta-seconds：缓存时间
	11111111111111111111111
		强缓存
	1111111111111111
		1. expires: 存储时间 （当前时间 + 缓存时间）
			缺点：用户更改本地时间会导致缓存失效（时差 误差等也会）
             写法复杂：表示时间的字符串多个空格，会导致非法属性从而失效
            
         2. cache-control:max-age=number, 同样存储时间
		expires > cache-control
	1111111111111111111111111111111
		协商缓存
	1111111111111111
		1. Last-Modified/if-Modified-Since
			资源在服务器上最后的修改时间
		2. Etag/If-None-Match
		Etag > Last-Modified
```

|      | 强缓存 | 协商缓存 |
| ---- | ------ | -------- |
|      |        |          |
|      |        |          |
|      |        |          |

![image-20200715230443167](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20200715230443167.png)

## 1.9.9.6 use strict

```javascript
使JS编码更加规范化的模式,消除Javascript语法的一些不合理、不严谨之处，减少一些怪异行为。 
默认支持的糟糕特性都会被禁用，比如不能用with，也不能在意外的情况下给全局变量赋值; 
全局变量的显示声明,函数必须声明在顶层，不允许在非函数代码块内声明函数,arguments.callee也不允许使用； 
保证代码运行的安全,限制函数中的arguments修改; 
提高编译器效率，增加运行速度；
```

## 1.9.9.6 new

```javascript
1、创建一个空对象，并且 this 变量引用该对象，同时还继承了该函数的原型。
2、属性和方法被加入到 this 引用的对象中。
3、新创建的对象由 this 所引用，并且最后隐式的返回 this 。
```

## 1.9.9.7 前端路由

#### hash路由

```html
        <a href="#/a">a</a>
        <a href="#/b">b</a>
        <div></div>
        <script>
            window.onhashchange = function (evt) {
                console.log('evt: ', evt);
                $('div')[0].innerHTML = location.hash;
            }

        </script>
```

#### history路由

```html
<button onclick="go('/index1/history/a')">跳转 A 页面</button>
<button onclick="go('/index1/history/b')">跳转 b 页面</button>
<div></div>
<script>
    window.onpopstate = (e) => {
    console.log('e: ', e);
}
window.onhashchange = function (evt) {
    console.log('evt: ', evt);
    $('div')[0].innerHTML = location.hash;
}

function go(path) {
    $('div')[0].innerHTML = path;
    history.pushState({
        path
    }, '', path);
}

</script>
```





## JS基本代码规范

```javascript
（1）不要在同一行声明多个变量 
（2）如果你不知道数组的长度，使用 push 
（3）请使用 ===/!== 来比较 true/false 或者数值 
（4）对字符串使用单引号 ‘’(因为大多时候我们的字符串。特别html会出现”) 
（5）使用对象字面量替代 new Array 这种形式 
（6）绝对不要在一个非函数块里声明一个函数，把那个函数赋给一个变量。浏览器允许你这么做，但是它们解析不同 
（7）不要使用全局函数 
（8）不要总是使用 var 来声明变量，如果不这么做将导致产生全局变量，我们要避免污染全局命名空间 
（9）Switch 语句必须带有 default 分支 
（10）使用 /*…*/ 进行多行注释，包括描述，指定类型以及参数值和返回值 
（11）函数不应该有时候有返回值，有时候没有返回值 
（12）语句结束一定要加分号 
（13）for 循环必须使用大括号 
（14）if 语句必须使用大括号 
（15）for-in 循环中的变量应该使用 var 关键字明确限定作用域，从而避免作用域污染 
（16）避免单个字符名，让你的变量名有描述意义
（17）当命名对象、函数和实例时使用驼峰命名规则 
（18）给对象原型分配方法，而不是用一个新的对象覆盖原型，覆盖原型会使继承出现问题
```



# 1A. typescript

```typescript
yarn global add typescript
```



# 2.HTML基础location

## 2.1 文档模式

```javascript
000000000000000000
	混杂模式
0000000000
	1. 默认开启
000000000000000000
	标准模式
0000000000
	1. <!DOCTYPE html> html5

```

## 2.2 noScript

```html
<noscript>
    script无效时, 显示信息
</noscript>
```

## 2.3 常用标签

```html
<a download="资信证明.pdf" thref="http://localhost:57483/App/templateFile/05.pdf">下载测试</a> 

0000000000000000000000000000000
	插入用标签
00000000000000
<audio>, <canvas>, <iframe>, <img>, <math>, <object>, <svg> ，<video>
    
    <embed type="video/quicktime" src="movie.mov" width="640" height="480">
    <iframe id="inlineFrameExample"
    title="Inline Frame Example"
    width="300"
    height="200"
    src="https://www.baidu.com">
</iframe>
    
<base href="http://www.w3school.com.cn/i/" /> 
    //规定相对地址
<base target="_blank" />
```



```html
<html>
    <form>
      <fieldset>
        <legend>健康信息</legend> //title
        身高：<input type="text" />
        体重：<input type="text" />
      </fieldset> //外层
    </form>
</html>
```

## 2.4 语义化标签

```html
/* awfawf */
<header></header>
<hgroup></hgroup>
<nav></nav> 导航
<aside></aside> 侧边栏 广告
<footer></footer> 版权信息 联系方式等
<article></article> 一篇博客、一个论坛帖子、一篇新闻报道
<section></section> 有标题区域
<time datetime="2008-02-14">情人节</time> 
<mark>123</mark> 高亮
<address></address> 地址
<small></small>
<strong></strong> 强调文本

1. html语义化让页面的内容结构化，便于对浏览器、搜索引擎解析; 
2. 即使在没有样式CSS情况下也以一种文档格式显示，并且是容易阅读的; 
3. 搜索引擎的爬虫也依赖于HTML标记来确定上下文和各个关键字的权重，利于SEO; 
4. 使阅读源代码的人对网站更容易将网站分块，便于阅读维护理解。
```



![img](https://upload-images.jianshu.io/upload_images/15827882-4057d561069e7a15.png?imageMogr2/auto-orient/strip|imageView2/2/w/484/format/webp)

## 2.5 提高html加载速度

```javascript
1.js外联文件放到body底部，css外联文件放到head内 
2.http静态资源尽量用多个子域名 
3.服务器端提供html和http静态资源时最好开启gzip 
4.在js,css,img等资源响应的http headers里设置expires,last-modified 
5.尽量减少http requests的数量 
6.js/css/html/img资源压缩 
7.使用css spirtes，可以减少img请求次数 
8.大图使用lazyload懒加载 
9.避免404，减少外联js 
10.减少cookie大小可以提高获得响应的时间 
11.减少dom elements的数量 
12.使用异步脚本，动态创建脚本
```

## 2.6 iframe缺点

```javascript
1. iframe会阻塞主页面的Onload事件； 
2. 搜索引擎的检索程序无法解读这种页面，不利于SEO; 
3. *iframe和主页面共享连接池，而浏览器对相同域的连接有限制，所以会影响页面的并行加载。 
4. 使用iframe之前需要考虑这两个缺点。如果需要使用iframe，最好是通过javascript 动态给iframe添加src属性值，这样可以绕开以上两个问题。
```

## 2.7 script

```javascript
1.async标记的Script异步执行下载，并执行。这意味着script下载时并不阻塞HTML的解析，并且下载结束script马上执行。 

2.defer标签的script顺序执行。这种方式也不会阻断浏览器解析HTML。 跟 async不同, defer scripts在整个文档里的script都被下载完才顺序执行。
```

浏览器是遇到一个script标记执行一个，当seajs.js正在执行的时候，document.scripts获取到的最后一个script就是当前正在执行的script。所以我们可以通过`scripts[scripts.length - 1]`拿到引用seajs.js的那个script节点引用。

# 3.CSS 基础

## 	3.1 选择器

```javascript
1.id选择器（ # myid）

2.类选择器（.myclassname）

3.标签选择器（div, h1, p）

4.相邻选择器（h1 + p）

5.子选择器（ul > li）

6.后代选择器（li a）

7.通配符选择器（ * ）

8.属性选择器（a[rel = "external"]）

9.伪类选择器（a:hover, li:nth-child）

优先级：!important > 行内样式 > id选择器 > 类选择器 > 标签选择器 > 通配符选择器 > 继承样式 > 浏览器默认样式



常见的基于关系的选择器
选择器	选择的元素
A E	元素A的任一后代元素E (后代节点指A的子节点，子节点的子节点，以此类推)
A > E	元素A的任一子元素E(也就是直系后代)
E:first-child	任一是其父母结点的第一个子节点的元素E
B + E	元素B的任一下一个兄弟元素E
B ~ E	B元素后面的拥有共同父元素的兄弟元素E
```

### 3.1.1 选择器的优先级

```javascript
CSS 选择器的优先级是如何计算的？
浏览器通过优先级规则，判断元素展示哪些样式。优先级通过 4 个维度指标确定，我们假定以a、b、c、d命名，分别代表以下含义：

a表示是否使用内联样式（inline style）。如果使用，a为 1，否则为 0。
b表示 ID 选择器的数量。
c表示类选择器、属性选择器和伪类选择器数量之和。
d表示标签（类型）选择器和伪元素选择器之和

优先级的结果并非通过以上四个值生成一个得分，而是每个值分开比较。a、b、c、d权重从左到右，依次减小。判断优先级时，从左到右，一一比较，直到比较出最大值，即可停止。所以，如果b的值不同，那么c和d不管多大，都不会对结果产生影响。比如0，1，0，0的优先级高于0，0，10，10。

当出现优先级相等的情况时，最晚出现的样式规则会被采纳。如果你在样式表里写了相同的规则（无论是在该文件内部还是其它样式文件中），那么最后出现的（在文件底部的）样式优先级更高，因此会被采纳。

在写样式时，我会使用较低的优先级，这样这些样式可以轻易地覆盖掉。尤其对写 UI 组件的时候更为重要，这样使用者就不需要通过非常复杂的优先级规则或使用!important的方式，去覆盖组件的样式了。
```



## 3.2 盒子模型

```css
（1）有两种， IE 盒子模型、W3C 盒子模型； 

（2）盒模型： 内容(content)、填充(padding)、边界(margin)、 边框(border)； 

（3）区 别： IE的content部分把 border 和 padding计算了进去;

body {
    margin:10px;
    border:10px solid black;
    padding:10px;
    width:100px;
    height:100px;
}

box-sizing: border-box
	// Endwidth = content+border+padding
```

## 3.3 BFC

```javascript
Block Formatting Contexts(块级格式化上下文)
1. 决定了其子元素将如何定位, 以及和其他元素的关系和相互作用
2. BFC如同隔离的独立容器, 元素不会再布局上影响外面的元素, 具有普通容器没有的一些特性
3. 子元素不会margin，重叠，本身会

BFC 块级格式化上下文
特点: 
	1. 形成一个完全独立的空间 不受外界影响
    2. 也不会影响外界
    
解决问题:
	1. 消除高度塌陷
    2. 解决自适应布局
    3. 外边距垂直方向重合(同一个BFC中，body下的所有元素都是在同一个BFC，包括子子孙孙)
    
触发方式:
	1. 根元素, html元素
	2. float不为none
    3. overflow 不为visible
    4. displaly inline-block/table-cells/flex
	5. position absolute/fixed

布局规则:
	1. 一个接一个放
    2. margin重叠
    3. 不会于float box重叠
    4. 浮动元素也参与计算
    

IFC inline    
GFC grid
FFC flex
```

|          | 普通流   | 浮动                                     | 绝对定位       |
| -------- | -------- | ---------------------------------------- | -------------- |
| 实习方法 | 默认     | float                                    | absolute/fixed |
| 排列方式 | 按块排列 | 首先按照普通流位置出现, 根据浮动方向移动 | 脱离普通流     |
|          |          |                                          |                |

## 3.6 none/hidden/0

|            | display:none                   | visibility:hidden;                 | opacity:0;                                                   |
| ---------- | ------------------------------ | ---------------------------------- | ------------------------------------------------------------ |
| dom结构    | 不占据空间                     | 占据空间                           | 占据空间                                                     |
| 事件监听   | 无法进行dom事件监听            | 无法进行DOM事件监听                | 可以进行dom事件监听                                          |
| 性能       | 引起重排，性能较高             | 引起重绘，性能较高                 | 不会触发重绘，性能较高                                       |
| 继承       | 不会被子元素继承，子元素也没了 | 会被子元素继承，子元素可以单独显示 | 会被子元素继承，子元素不能单独显示。除非设置background:rgba(0,0,0,1) |
| transition | 不支持                         | 立即显示，隐藏延时                 | 可以同时延时                                                 |



## 3.4 水平垂直居中

```css
00000000000000000000000000000
	方法1 position
000000000000
	/* div 宽高未定 */
      html,
      body {
        height: 100%;
        margin: 0;
      }

      body {
        position: relative;
      }

      div {
        background-color: red;
        position: absolute;
        width: 100px;
        height: 100px;
        left: 50%;
        top: 50%;
        /* 关键步骤, 将元素向左上分别移动50% */
        transform: translate(-50%, -50%);
      }
00000000000000000000000000000
	方法2 position
000000000000
      html,
      body {
        height: 100%;
        margin: 0;
      }

      body {
        position: relative;
      }

      div {
        width: 600px;
        height: 600px;
        background: red;
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
        margin: auto;
      }
00000000000000000000000000000
	方法3 flex
000000000000
	.box{
    height:800px;
    -webkit-display:flex;
    display:flex;
    -webkit-align-items:center;
    align-items:center;
    -webkit-justify-content:center;
    justify-content:center;
    border:1px solid #ccc;
}
div.child{
    width:600px;
    height:600px;
    background-color:red;
}
000000000000000000000000000000
	方法4 table-cell
00000000000
/* 文字居中 */
body {
    display: table-cell;
    vertical-align: middle;
    text-align: center;
    border: 1px solid #666;
    width: 600px;
    height: 600px;
}
```

## 3.5 浏览器兼容

```javascript
-moz代表firefox浏览器私有属性
-ms代表IE浏览器私有属性
-webkit代表chrome、safari私有属性
```

## 3.7 !important 覆盖

```html
<body>
	<img src="1.jpg?imageView2/0/w/818" style="width:480px!important; max-width: 300px">
	<img src="1.jpg?imageView2/0/w/818" style="width:480px!important; transform: scale(0.625, 1);" >
	<img src="1.jpg?imageView2/0/w/818" style="width:480px!important; width:300px!important;">

</body>
```

## 3.8 多行文本溢出省略

```html
<body>
    <div style='width:400px;
    height:70px;
    border:1px solid red;'>
      <p style='display:
           -webkit-box;
           /*对象作为弹性伸缩盒子模型显示 */
           -webkit-box-orient: vertical;
           /*设置或检索伸缩盒对象的子元素的排列方式 */
           -webkit-line-clamp: 2;
           /*溢出省略的界限*/
           overflow:hidden;
           /*设置隐藏溢出元素*/'>
        这是一些文本这是一些文本这是一些文本这是一些文本这是一些文本这是一些文本这是一些文本这是一些文本这是一些文本这是一些文本
      </p>
    </div>
</body>
```

## 3.9 禁用点击事件

```css
.disabled {
    /* 禁用点击事件 */
    pointer-events: none;
    cursor: default;
    opacity: 0.6;
}
```

## 3.9.1 网格实现

```css
// odd表示基数，此时选中基数行的样式，even表示偶数行
.row:nth-child(odd){
    background: #eee;
}
.row:nth-of-type(odd){
    background: #eee;
}
.row:nth-of-type(2n+1){
    background: #eee;
}
// 类为row 的奇数行
```

## 3.9.2 渐变

```css

div:nth-child(1) {
    background: beige;
    background-size: auto 2em;
    background-origin: content-box;
    background-image: linear-gradient(rgba(0, 0, 0, .2) 50%, transparent 0);
    height: 100px;
}
```

## 3.9.3 css禁止用户选择（文本）

```css
body{
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
```

## 3.9.4 视觉落差（background-attachment）

|        | 本身滚动 | 父滚动 |
| ------ | -------- | ------ |
| scroll | 不会     | 会     |
| fixed  | 不会     | 不会   |
| local  | 会       | 会     |

## 3.9.5 link和@imoprt

|         | 出处                                   | 加载                              | 兼容性  | JS     | 权重 |
| ------- | -------------------------------------- | --------------------------------- | ------- | ------ | ---- |
| link    | XHTML，还有rel等连接属性<br />HTML标签 | 页面被加载时，link会同时加载      | 无兼容  | 支持   | 高   |
| @import | CSS，只能加载CSS                       | 引用的CSS会等到页面被加载完在加载 | IE5以上 | 不支持 | 低   |

*（1）link属于XHTML标签，除了加载CSS外，还能用于定义RSS, 定义rel连接属性等作用；而*[@import](https://github.com/import)*是CSS提供的，只能用于加载CSS; （2）页面被加载的时，link会同时被加载，而*[@import](https://github.com/import)*引用的CSS会等到页面被加载完再加载; （3）import是CSS2.1 提出的，只在IE5以上才能被识别，而link是XHTML标签，无兼容问题; (4)link支持使用js控制DOM去改变样式，而*[@import](https://github.com/import)*不支持;*

## 3.9.6 position

|          | 定位原点                                 |      |
| -------- | ---------------------------------------- | ---- |
| absolute | 相对于值不为static的第一个父元素进行定位 |      |
| relative | 正常位置时                               |      |
| fixed    | 浏览器窗口                               |      |

## 3.9.7 background

| background-属性名 | 属性1                                 | 属性2                         | 属性3                      | 属性4         |
| ----------------- | ------------------------------------- | ----------------------------- | -------------------------- | ------------- |
| -attachment       | scroll : 图片不跟随内容移动(随父移动) | fixed: 滚动落差(完全不会滚动) | local: 图片与内容固定      |               |
| -clip             | border-box: 图片包含border            | padding-box: 图片包含padding  | content-box:图片只包含内容 | text:文字背景 |
| -origin           | border-box: 原点位置                  | padding-box:                  | content-box:               |               |
| -position         | bottom 50px right 100px;              | right 35% bottom 45%;         | 25% 75%;                   |               |
| -size             | contain                               | cover                         | 305                        | 200px 100px   |

## 3.9.8 animation

| animation-name             |                                                              |                   |                          |                                                              |              |
| -------------------------- | ------------------------------------------------------------ | ----------------- | ------------------------ | ------------------------------------------------------------ | ------------ |
| -name                      | none                                                         | 名字              | 名字1, 名字2             |                                                              |              |
| -duration                  | 6s                                                           | 120ms             | 1s, 15s                  | 10s, 30s, 230ms                                              |              |
| -timing-function(默认ease) | ease: 慢快慢<br />ease-out:快慢<br />ease-in:慢快<br />ease-in-out:ease | linear: 速度相同  | step-start<br />step-end | cubic-bezier(0.1, 0.7, 1.0, 0.1):速度控制<br />steps(4, end)<br />frames(10) |              |
| -delay                     | 3s                                                           | 2s, 4ms           |                          |                                                              |              |
| -iteration-count           | 次数                                                         | 1                 | 2                        | 1.5                                                          | **infinite** |
| -direction                 | normal: 重置到起点                                           | reverse: 由尾到头 | alternate:交替反向运动   | alternate-reverse: 反向交替, 反向开始交替                    |              |
| -fill-mode                 | none                                                         | forwards 尾停止   | backwards                | both                                                         |              |
| -play-state                | running                                                      | paused            |                          |                                                              |              |

## 3.9.9 canvas和svg区别

```javascript
1.从图像类别区分，Canvas是基于像素的位图，而SVG却是基于矢量图形。可以简单的把两者的区别看成photoshop与illustrator的区别。 2.从结构上说，Canvas没有图层的概念，所有的修改整个画布都要重新渲染，而SVG则可以对单独的标签进行修改。 3.从操作对象上说，Canvas是基于HTML canvas标签，通过宿主提供的Javascript API对整个画布进行操作的，而SVG则是基于XML元素的。 4.从功能上讲，SVG发布日期较早，所以功能相对Canvas比较完善。 5.关于动画，Canvas更适合做基于位图的动画，而SVG则适合图表的展示。 6.从搜索引擎角度分析，由于svg是有大量标签组成，所以可以通过给标签添加属性，便于爬虫搜索
```

## 3.9.9.1 transform

| transform:                                                   |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| matrix(1.0, 2.0, 3.0, 4.0, 5.0, 6.0)                         | matrix( scaleX(), skewY(), skewX(), scaleY(), translateX(), translateY() ) |
| translate(12px, 50%) - translateX(2em) - translateY(3in)     | 平面位移                                                     |
| scale(2, 0.5) - scaleX(2) - scaleY(0.5)                      | 大小的改变                                                   |
| rotate(0.5turn)                                              | rotate(180deg)                                               |
| skew(30deg, 20deg) - skewX(30deg) - skewY(1.07rad)           | 横纵坐标的倾斜角度                                           |
| matrix3d(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0) |                                                              |
| translate3d(12px, 50%, 3em)                                  |                                                              |
| scale3d(2.5, 1.2, 0.3)                                       |                                                              |
| rotate3d(1, 2.0, 3.0, 10deg)                                 |                                                              |

| transform:                                      |                       |
| ----------------------------------------------- | --------------------- |
| perspective(17px)                               | 观察者与Z=0屏幕的距离 |
| translateX(10px) rotate(10deg) translateY(5px); |                       |
| **perspective-origin：**                        |                       |
| center;                                         | 中心                  |
| top;                                            |                       |
| bottom right;                                   |                       |
| -170%;                                          |                       |
| 500% 200%;                                      | x, y                  |

transform-style: preserve-3d;

transform-origin: center;

backface-visibility：visible; //hidden

## 3.9.9.2 z-index

```javascript
CSS 中的z-index属性控制重叠元素的垂直叠加顺序。z-index只能影响position值不是static的元素。

父子不会叠
```

## 3.9.9.3 文本超出部分显示省略号

```javascript
// 单行
overflow: hidden;
text-overflow: ellipsis;
white-space: nowrap;
// 多行
display: -webkit-box;
-webkit-box-orient: vertical;
-webkit-line-clamp: 3; // 最多显示几行
overflow: hidden;
```

## 3.9.9.4 object-fit

|            |                                                              |
| ---------- | ------------------------------------------------------------ |
| fill       | 默认，不保证保持原有的比例，内容拉伸填充整个内容容器。       |
| contain    | 保持原有尺寸比例。内容被缩放                                 |
| cover      | 保持原有尺寸比例。但部分内容可能被剪切。                     |
| none       | 保留原有元素内容的长度和宽度，也就是说内容不会被重置。       |
| scale-down | 保持原有尺寸比例。内容的尺寸与 none 或 contain 中的一个相同，取决于它们两个之间谁得到的对象尺寸会更小一些。 |



# 4.框架/架构attachement

## 4.1 MVVM

```javascript
MVC: VIEW MODLE CONTROLLER
MVVM: Model-View-ViewModel(双向绑定) 
1. Model 数据模型
2. View 视图
3. ViewModel 连接Model和view
```

## 4.2VUE

### ! vue-cli !

```javascript
.
|-- build                            // 项目构建(webpack)相关代码
|   |-- build.js                     // 生产环境构建代码
|   |-- check-version.js             // 检查node、npm等版本
|   |-- dev-client.js                // 热重载相关
|   |-- dev-server.js                // 构建本地服务器
|   |-- utils.js                     // 构建工具相关
|   |-- webpack.base.conf.js         // webpack基础配置
|   |-- webpack.dev.conf.js          // webpack开发环境配置
|   |-- webpack.prod.conf.js         // webpack生产环境配置
|-- config                           // 项目开发环境配置
|   |-- dev.env.js                   // 开发环境变量
|   |-- index.js                     // 项目一些配置变量
|   |-- prod.env.js                  // 生产环境变量
|   |-- test.env.js                  // 测试环境变量
|-- src                              // 源码目录
|   |-- components                     // vue公共组件
|   |-- store                          // vuex的状态管理
|   |-- App.vue                        // 页面入口文件
|   |-- main.js                        // 程序入口文件，加载各种公共组件
|-- static                           // 静态文件，比如一些图片，json数据等
|   |-- data                           // 群聊分析得到的数据用于数据可视化
|-- .babelrc                         // ES6语法编译配置
|-- .editorconfig                    // 定义代码格式
|-- .gitignore                       // git上传需要忽略的文件格式
|-- README.md                        // 项目说明
|-- favicon.ico 
|-- index.html                       // 入口页面
|-- package.json                     // 项目基本信息
.
```

#### 1.路由

```html
<router-link to="/">回到首页</router-link>
<router-view /> <!--模版显示的地方-->
```

```javascript
import Vue from 'vue'; // 引入Vue
import Router from 'vue-router'; // 引入vue-router
import HelloWorld from '@/components/HelloWorld'; // 引入根目录下的Hello.vlue
import Hi from '@/components/Hi';
import Hi1 from '@/components/Hi1';
import Hi2 from '@/components/Hi2';
import ErrorPage from '@/components/Error';
import Count from '@/components/Count';

Vue.use(Router); // Vue全局使用Router

export default new Router({
  mode: 'hash',
  routes: [ // 配置路由,这是个数组
    { // 每一个链接都是一个对象
      path: '/', // 连接路径
      name: 'HelloWorld', // 路由名称
      component: HelloWorld // 对应的组件模版
    },
    { // 每一个链接都是一个对象
      path: '/Count', // 连接路径
      name: 'Count', // 路由名称
      component: Count // 对应的组件模版
    },
    {
      path: '/Hi',
      name: 'Hi',
      component: Hi,
      children: [
        {path: 'hi1', name: 'Hi/hi1', component: Hi1},
        {path: 'hi2', name: 'Hi/hi2', component: Hi2}
      ],
      beforeEnter: function(to, from, next) {
        // console.log('next: ', next)
        console.log('from: ', from);
        console.log('to: ', to);
        // next({path: '/Error'})
        next();
      }
    }, {
      path: '/goback',
      redirect: '/'
    }, {
      path: '*',
      component: ErrorPage
    }
  ]
});
```



### 4.2.1 组件

#### 1.组件通信

```html
0000000000000000000000000000000000000000
	子父传值
0000000000000000000

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div id="app">
        <!-- 方法1 -->
        <prop-slotname :parent="parent" text="this is a text">
            default
            <template v-slot:header="slotProps">
                <!-- 子传父亲数据 通过
                    父 ：v-slot:header="自定义名字"
                    子 ：<slot name="header" :user="user">
                -->
                <div>
                    header
                    {{ slotProps.child.value }}
                    <!-- 方法1 -->
                </div>
            </template>
            <template #footer="slotfooter">
                <div>
                    footer
                    {{ slotfooter.child.value }}
                </div>
            </template>
        </prop-slotname>
        <!-- 方法2 -->
        <event-slot>
        </event-slot>
        <!-- 方法1 -->
        <func @fatherfunc="parentFunc">
            <!-- @fatherfunc必须是小写 -->
        </func>
        <!-- 方法4 -->
        <attrs-listeners url="123" gg="125" @fatherfunc="parentFunc" :parent="parent">
        </attrs-listeners>
        <!-- 方法5 -->
        <provide-inject>
        </provide-inject>
        <!-- 方法6 -->
        <children-parent>
        </children-parent>
    </div>
    <script type="module">
        /**
         * 
         * 方法1 props-slotName ------------ 父向子互传数据  子向父 一般 麻烦
         * 方法2 bus event --------- 父子互相 挺好 但是要考虑执行顺序
         * 方法3 $emit ------------ 一般，只能接受父亲的方法
         * 方法4 $attrs $listener ------------ 挺好使，不过不能往父亲传值
         * 方法5 provide-inject ------------ 感觉作用比较小
         * 方法6 $parent $children ----------- 挺好 就是获取子麻烦
        */
        import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.11/dist/vue.esm.browser.js';
        var Event = new Vue(); //方法2的公交车
       /**
        * 
        * 方法六
        * 
       */
        Vue.component('children-parent',{
            mounted(){
                console.log("this.$parent", this.$parent) //直接获取父vue节点
            },
            template: `
                <div>
                    children-parent
                </div>
            `
        }) //方法六 children parent 父子互传 较好
       /**
        * 
        * 方法五
        * 
       */
        Vue.component('provide-inject',{
            inject:['test'],
            mounted(){
                console.log('test: ', this.test); //父亲provide给的值
            },
            template: `
                <div>
                    provide-inject
                </div>
            `
        }) //方法5 provide-inject 父传子数据 一般
       /**
        * 
        * 方法四
        * 
       */
        Vue.component('attrs-listeners',{
            mounted(){
                console.log('this.$attrs: ', this.$attrs); //接受组件头的所有数据
                console.log('this.$listener: ', this.$listeners); //接受组件头的所有方法
            },
            template: `
                <div>
                    attrs-listeners
                </div>
            `
        }) //父亲给儿子传方法与数据 比较好 方法4 attrs-lisnters 父传子方法与数据 还行
       /**
        * 
        * 方法三
        * 
       */
        Vue.component('func',{
            methods: {
                toFather: function(){
                    Event.$emit('tofather','func传值给event-slot') //兄弟传送方法
                }
            },
            mounted(){
                this.$emit("fatherfunc","儿子的传值 func") //父亲 传方法
            },
            template: `
                <div>
                   <button @click="toFather">childBtn</button>
                </div>
            `
        }) //父亲给儿子传方法 方法命必须为小写 方法3 func传值 父传子方法 一般
       /**
        * 
        * 方法二
        * 
       */
        Vue.component('event-slot', {
            mounted(){
                Event.$on('tofather',this.toFather) //给busEvent 上方法
            },
            template:`
                <div>eventSlot</div>
            `
        }) //方法2 事件中心 按顺序mounted执行 理论上可任意传数据方法 挺好
       /**
        * 
        * 方法一 使用props
        * 
       */
        Vue.component('prop-slotname', {
            props: ['parent'], //父向子传递数据
            //方法1
            data() {
                return {
                    child: {
                        value:"I am child"
                    }
                }
            },
            template: `
                <div :child="child"> 
                    
                    <!-- <div :child="child" text="this is a text">  --|-- text="this is a text" 会被渲染到外层头 -->
                    
                    {{ child.value }} 
                    {{ parent.value }}
                    <slot></slot>
                    
                    <h6><slot name="header" :child="child"></slot></h6>
                    <h1><slot name="footer" :child="child"></slot></h1>
                </div>
                
            `
        }) //方法1 父子互传数据 还行
       /**
        * 
        * 父亲节点
        * 
       */
        var vm = new Vue({
            el: '#app',
            data: {
                parent: {
                    value: 'I am father'
                }
            },
            provide(){
                return{
                    test:'parentProvide' //方法5
                }
            },
            mounted() {
                // console.log("father", childrenFunc)
                console.log('this.$children', this.$children); //方法六
                Event.$emit('tofather','father传值给event-slot') //方法2
            },
            methods: {
                parentFunc(son) {
                    console.log("这是父亲方法,儿子传值：",son)
                }
            },
        })
    </script>
</body>

</html>
```

#### 2.component标签

```html
<component v-bind:is="who"></component>
<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            who: 'componentA'
        },
        components: {
            "componentA": componentA,
            "componentB": componentB,
            "componentC": componentC,
        },
    })

</script>
```



### 4.2.2 生命周期

![image-20200712173137615](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20200712173137615.png)

|                 | 作用                                              |
| --------------- | ------------------------------------------------- |
| beforeCreated   | 数据初始化之前                                    |
| created         | data和method已经挂载到vue实例上                   |
| beforeMounted   | 模板编译完成，dom挂载到vue实例上之前，无法获取dom |
| mounted         | 挂载完成，可以获取dom                             |
| beforeUpdated   | 组件更新前                                        |
| updated         | 组件更新后                                        |
| beforeDestroyed |                                                   |
| destroyed       |                                                   |



### 4.2.3 vuex

```javascript
State、getter、mutation、action、module

state ：基本数据
getter ： 加工数据
mutations ： 提交更改数据，同步！
actions ：用于异步，请求数据
moduels ：模块化vue
```

#### vuex定义

```javascript
import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

/**
 * 状态对象
 */
const state = {
  count: 1
};
const mutations = {
  add(state, n) {
    state.count += n;
  },
  reduce(state, n) {
    !isNaN(n) ? state.count -= n : state.count--;
  }
};
const getters = {
  count() {
    return state.count + 99;
  }
};
const actions = {
  addAction(context) {
    context.commit('add', 100);
  },

  reduceAction({commit}) {
    commit('reduce', 10);
  }
};
export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions
});

```



#### vuex使用

```html
<template>
<div>
  <div>{{test}}</div>
  <button @click="changeTest">changeTest</button>
  <div>{{count}}</div>
  <button @click="add(10)">+</button>
  <button @click="reduce">-</button>
  <button @click="addAction">Action+</button>
  <button @click="reduceAction">Action-</button>
</div>
</template>

<script>
import store from '@/vuex/store';
import {
  mapState,
  mapMutations,
  mapGetters,
  mapActions
} from 'vuex';
export default {
  name: 'Count',
  data() {
    return {
      test: 'test'
    };
  },
  // computed: mapState({
  //   count: state => state.count * 2
  // }),
  computed: {
    ...mapState(['count']),
    ...mapGetters(['count'])
  },
  methods: {
    ...mapMutations(['add', 'reduce']),
    ...mapActions(['addAction', 'reduceAction']),
    changeTest() {
      this.test = 'AFTERtEST';
    }
  },
  store

};
</script>

<style scoped>
</style>

```



### 4.2.4 指令

#### 	1. directive 自定义指令

```html

<div v-jspang="color" id="demo"> 123 </div>
<script>
    Vue.directive('jspang', {
        bind: function (el, binding, vnode) { //被绑定
            console.log('1 - bind');
        },
        inserted: function () { //绑定到节点
            console.log('2 - inserted');
        },
        update: function () { //组件更新
            console.log('3 - update');
        },
        componentUpdated: function (el, binding, vnode) { //组件更新完成
            console.log('4 - componentUpdated');
        },
        unbind: function () { //解绑
            console.log('5 - bind');
        }
    });
</script>
```

#### 2. extend扩展

```html
<author></author>
<div id="author"></div>

<script type="text/javascript">
    var authorExtend = Vue.extend({
        template: "<p><a :href='authorUrl'>{{authorName}}</a></p>",
        data: function () {
            return {
                authorName: 'JSPang',
                authorUrl: 'http://www.jspang.com'
            }
        }
    });
    new authorExtend().$mount('#author');
    new authorExtend().$mount('author');

</script>
```

#### 3. 常见指令

v-text v-clock v-pre v-once v-html v-on @keyup.enter

v-model.number v-model.lazy v-model.trim

### 4.绑定

```html

<!-- 多选按钮绑定一个数组, 会显示选中的数据数组 -->
<h3>多选绑定一个数组</h3>
<p>
    <input type="checkbox"
           id="JSPang"
           value="JSPang"
           v-model="web_Names">
    <label for="JSPang">
        JSPang</label><br />
    <input type="checkbox"
           id="Panda"
           value="Panda"
           v-model="web_Names">
    <label for="Panda">Panda</label><br />
    <input type="checkbox"
           id="PanPan"
           value="PanPan"
           v-model="web_Names">
    <label for="PanPan">PanPan</label>
<p>{{web_Names}}</p>
</p>
<!-- 单选按钮 直接给sex值 -->
<h3>单选按钮绑定</h3>
<input type="radio"
       id="one"
       value="男"
       v-model="sex">
<label for="one">男</label>
<input type="radio"
       id="two"
       value="女"
       v-model="sex">
<label for="one">女</label>
<p>{{sex}}</p>
<!-- class绑定 -->
<div :class="{classA:isTrue}">2、绑定class中的判断</div>
<div :class="[classA,classB]">3、绑定class中的数组</div>
<!-- style -->
<div :style="{color:red,fontSize:font}">5、绑定style</div>
<div :style="styleObject">6、用对象绑定style样式</div>
```



### 4.2.5 mini

```html
<script type="text/javascript">
    //额外临时加入时，用于显示日志
    var addLog = {
        updated: function () {
            if (this.num < 100) {
                this.num++
            }
            console.log("数据放生变化,变化成" + this.num + ".");
        },
        methods: function dub() {
            this.num--
        }
    }
    var app = new Vue({
        el: '#app',
        data: {
            num: 1
        },
        methods: {
            add: function () {
                this.num++;
            }
        },
        mixins: [addLog] //混入
    })

</script>
```

### 4.2.6 this.$set(this.arr, 1, 111)

## 4.3 webpack

```javascript
loader 实现对不同文件的打包
Babel 语言版本编码
```

### 4.3.1 webpack 热更新

```javascript
1. 页面首次打开后，服务端与客户端通过 EventSource 建立通信渠道，把下一次的 hash 返回前端
2. 客户端获取到hash，这个hash将作为下一次请求服务端 hot-update.js 和 hot-update.json的hash
3. 修改页面代码后，Webpack 监听到文件修改后，开始编译，编译完成后，发送 build 消息给客户端
4. 客户	端获取到hash，成功后客户端构造hot-update.js script链接，然后插入主文档
5. hot-update.js 插入成功后，执行hotAPI 的 createRecord 和 reload方法，获取到 Vue 组件的 render方法，重新 render 组件， 继而实现 UI 无刷新更新。
```



# 5.工程化

# 6.性能优化

# 7.Typescript

## 7.1 tsconfig.json

```typescript
{
    "files":[""],
	"include":[""],        
	"exclude":[""],   
	"compilerOptions": {
        "outdir": "./build",  // 导出文件
        "rootdir": "./src", //入口文件
        "sourceMap": true, //对应位置
        "noUnusedLocals": true,//未使用的变量不打包
    	"noUnusedParameters": true,
        "outFile": "./build/page.js",//生成一个js    
        "strict": true,
        "noImplicitAny": true, //false则可以不写any
        "strictNullChecks": true,//不用许设置null
    }
}
```



# 8.网络原理

## 1.1 小知识点

```javascript
访问某大学web网站主页的协议有
	1. APP ARP UDP
```

## 1.2 七层网络协议

```javascript
00000000000000000000
	应用层
00000000
	HTTP 超文本协议
00000000000000000000
	表示层
00000000
00000000000000000000
	会话层
00000000
00000000000000000000
	传输层
00000000
	TCP传输协议
    UDP传输协议
00000000000000000000
	网络层
00000000
00000000000000000000
	数据链路层
00000000
00000000000000000000
	物理层
00000000
```

<img src="https://images2018.cnblogs.com/blog/1300057/201808/1300057-20180806111900283-1087963773.png" alt="img"  />

## 1.3 http

```javascript
HTTP - 基于TCP/IP传输协议, 超文本传输协议
0000000000000000000000000000000
	原理
0000000000000000000
	HTTP是一个基于TCP/IP通信协议来传递数据的协议，传输的数据类型为HTML 文件,、图片文件, 查询结果等。

	HTTP协议一般用于B/S架构（浏览器/服务器）。浏览器作为HTTP客户端通过URL向HTTP服务端即WEB服务器发送所有请求。
000000000000000000000000000000000
	特点
0000000000000000000000
	1. http协议支持客户端/服务端模式，也是一种请求/响应模式的协议。
	2. 简单快速：客户向服务器请求服务时，只需传送请求方法和路径。请求方法常用的有GET、HEAD、POST。
	3. 灵活：HTTP允许传输任意类型的数据对象。传输的类型由Content-Type加以标记。
	4. 无连接：限制每次连接只处理一个请求。服务器处理完请求，并收到客户的应答后，即断开连接，但是却不利于客户端与服务器保持会话连接，为了弥补这种不足，产生了两项记录http状态的技术，一个叫做Cookie,一个叫做Session。
	5. 无状态：无状态是指协议对于事务处理没有记忆，后续处理需要前面的信息，则必须重传。
000000000000000000000000000000000
	报文组成
00000000000000000
请求报文构成
    请求行：包括请求方法、URL、协议/版本 //GET URL
    请求头(Request Header)
    请求正文
响应报文构成
    状态行 //200 304
    响应头
    响应正文
000000000000000000000000000000000
	存在问题
000000000000000000
    1. 请求信息明文传输，容易被窃听截取。
    2. 数据的完整性未校验，容易被篡改
    3. 没有验证对方身份，存在冒充危险
000000000000000000000000000000000
	http请求过程
0000000000000000000
	1. 输入域名, 通过DNS寻找, 公网IP.
    2. 向指定公网ip的服务器发送请求。
    3. 服务器返回html代码。
    4. 浏览器得到html代码，开始渲染
    
    ---------------------------
    http1
-----------------
    非持久连接，一个tcp连接只传输一个web对象
	缓存：expires和cache-control
    	
---------------------------
    http1.1
-----------------
	1.支持长连接
	可以传输多个HTTP请求和相应	
    --------------
	keep-alive
	-------------
        connection:keep-alive
		connection:closs
        
     2.引入ETAG，用于重激活机制；
     引入cacheControl头域
     
     3.节约贷款
     引入range，允许只请求资源的某个部分；
     
     4.支持host头域
     
     5.新增多个状态码，410某个资源被永久性删除；
     
        
---------------------------
    http2
-----------------
    同一个连接并发处理多个请求

	提出对请求和响应头压缩
    
    请求过程：
    	客户端发起https请求
        服务端将公钥证书发给客户端。
        客户端解析证书，TLS完成，验证公钥完整性等。
        判断公钥无问题，生成随机值用于对称加密，用证书	对随机值进行非对称加密
        讲证书非对称加密后的随机值传到服务器；
        服务器用私钥进行解密后，得到随机值，并用随机值进行对称加密
        服务端将对称加密的信息发送给客户端
        客户端用生成的随机值进行解密
        
        将密钥通过 公私钥匙传递后进行数据传递
```

### 1.3.1 同源

```javascript
同源条件: 
1. 域名相同
2. 端口号相同 
3. 协议相同 http/https
```

### 1.3.2 HTTPS

```javascript
1. HTTPS - HTTP+SSL/TLS 是一种通过计算机网络进行安全通信的传输协议。
 1.1 HTTPS使用目的 - 提供对网址服务器的身份认证，同时保护数据的隐私与完整性
2. SSL证书 - 验证服务器的身份，并未浏览器与服务器之间的通信进行加密
00000000000000000000000000000000000
	缺点
0000000000000000000
    1. HTTPS协议多次握手，导致页面的加载时间延长近50%；
    2. HTTPS连接缓存不如HTTP高效，会增加数据开销和功耗；
    3. 申请SSL证书需要钱，功能越强大的证书费用越高。
    4. SSL涉及到的安全算法会消耗 CPU 资源，对服务器资源消耗较大。
    
    时间慢、对服务器压力大、申请证书要钱
```

#### 	1.3.2.1 HTTPS加密

```javascript
1. 先进行TCP连接,
2. 在TCP连接基础上进行TSL连接 
3. 服务器发送证书给客户端
4. 客户端验证证书
5. 验证证书成功后, 客户端生成随机数, 通过证书中的公钥进行非对称加密, 发送到服务器
6. 服务器使用私钥解密, 获取到改随机数, 将该随机数设置为密钥, 使用对称加密发送数据
7. 客户端解密数据, SSL通信开始

证书、非对称公钥 由服务器发送
对称加密密钥 由客户端发送
```



### 1.3.3 对称加密和非对称加密

```javascript
0000000000000000000000000000000
	对称加密
000000000000000000
	原文+密钥=密文
	密文-密钥=原文
	1111111111111111111111111111111111
		优缺点
	1111111111111111
		优点: 计算量小, 加密速度快, 加密效率搞
		缺点: 私钥被泄露, 信息会不安全, 密钥管理负担大
0000000000000000000000000000000
	非对称加密
000000000000000000
	公钥加密 私钥解密
    11111111111111111111111111111111
		非对称密钥
	111111111111111
	 1. 私钥: 由一方安全保管。
     2. 优点：安全性高
     3. 缺点：花费时间长、速度慢
     
```

### 1.3.4 dns查询过程

```javascript
000000000000000000000000000000000000
	dns解析过程
00000000000000000000
	1. 域名解析成公网ip
    2. 发起Tcp三次握手
    3. 建立TCP连接后发送http请求
    4. 服务器响应http请求, 浏览器得到html代码
	5. 浏览器解析Html代码, 并请求html代码的资源
	6. 浏览器对页面进行渲染呈现用户
0000000000000000000000000000000000
	dns查询过程
00000000000000000000
	1. 浏览器dns缓存查询
    2. 操作系统过自身dns缓存 //ipconfig
    3. 读取hosts文件 
    4. dns系统调用, 向本地首选的DNS服务器发送域名解析
```

### 1.3.5 HTTP2

```javascript
1. 多路复用
	HTTP/2的 多路复用 就是为了解决上述的两个性能问题。 在 HTTP/2 中，有两个非常重要的概念，分别是帧（frame）和流（stream）。 帧代表着最小的数据单位，每个帧会标识出该帧属于哪个流，流也就是多个帧组成的数据流。 多路复用，就是在一个 TCP 连接中可以存在多条流。换句话说，也就是可以发送多个请求，对端可以通过帧中的标识知道属于哪个请求。通过这个技术，可以避免 HTTP 旧版本中的队头阻塞问题，极大的提高传输性能。
```

### 1.3.6 状态码

```javascript
00000000000000000000
	响应头
000000000
状态码分类：
    1XX- 信息型，服务器收到请求，需要请求者继续操作。
    2XX- 成功型，请求成功收到，理解并处理。
    3XX - 重定向，需要进一步的操作以完成请求。
    4XX - 客户端错误，请求包含语法错误或无法完成请求。
    5XX - 服务器错误，服务器在处理请求的过程中发生了错误。
常见状态码：
    200 OK - 客户端请求成功
    301 - 资源（网页等）被永久转移到其它URL
    302 - 临时跳转
    400 Bad Request - 客户端请求有语法错误，不能被服务器所理解
    401 Unauthorized - 请求未经授权，这个状态代码必须和WWW-Authenticate报头域一起使用
    404 - 请求资源不存在，可能是输入了错误的URL
    500 - 服务器内部发生了不可预期的错误
    503 Server Unavailable - 服务器当前不能处理客户端的请求，一段时间后可能恢复正常。
```

### 1.3.7 HTTPS和HTTP的区别

|          | HTTPS   | HTTTP  |
| -------- | ------- | ------ |
| 传输     | 非明文  | 明文的 |
| 连接方式 | TCP+TSL | TCP    |
| 默认端口 | 443     | 80     |



## 1.4 TCP/UDP （三次握手）

```javascript
000000000000000000
	三次握手
00000000
	1. khd 向 fwd 发送一段TCP报文，标记为SYN，表示“请求建立连接”，Seq=X，随后客户端进入(SYN-SENT)阶段
	SYN=1 SEQ=X
	2. FWD 向 KHD 发送一段TCP报文，标志位为SYN何ACK，表示“确认KHD的报文Seq序号有效，服务器能正常接收客户端发送的数据，并同意创建新连接” (SYN-RCVD)
    SYN=1 ACK=1 SEQ=Y ack=X+1
	3.KHD 向 FWD 发送一段TCP报文，标志位为ACK，表示”确认收到服务端同意连接的信号“，序号为SEQ，确认号ACK
    ACK=1,SEQ=X+1,ack=Y+1

	SYN：表示请求连接
	SEQ：表示请求序号（有效，就同意连接）
	ACK：表示确认收到服务器同意连接的信号 
    
    自我理解：1. khd向fwd发送一段TCP报文，添加SYN表示请求连接，添加SEQ序号表示自身。2. fwd收到khd的TCP报文，向khd发送一段TCP报文，标记上SYN请求连接，SEQ序号表示自身，ACK表示同意连接，ack=x+1表示请求的连接序号。3.客户端，标志位ACK表示同意连接，SEQ=X+1表示收到客户端的Ack，并将其值作为自己的序号值。
    
    4. 理由：为了防止服务端开启一些无用的连接增加服务器开销以及防止已失效的连接请求报文段突然又传送到了服务端，因而产生错误。
    fwd向khd发送请求后，还没处理，此时fwd取消这个请求，但是khd还是收到了并与fwd连接，这个连接会一直连接，而且是无效的
000000000000000000
	四次挥手
00000000
	1. khd 向 fwd 发送关闭请求
    2. FWD 向 KHD 发送请求，表示接受关闭请求
    3. FWD 向 KHD 发送请求，表示完成关闭请求
    4. KHD 向 FWD 发送请求，表示知道断了吧
```



### 	1.41 UDP/TCP 区别

| 区别       | TCP                  | UDP                |
| ---------- | -------------------- | ------------------ |
| 面向方式   | 面向连接协议, 字节流 | 面向报文的, 无连接 |
| 可靠性     | 可靠的               | 不可靠的           |
| 有序性     | 有序的               | 无序的             |
| 速度与量级 | 慢, 重量级           | 快, 轻量级         |
| 细差       |                      |                    |



## 1.5 状态码

```javascript
100 - 收到第一部分请求, 正在等待其余部分
101 - 协议升级
200 - 请求成功
/**
* 访问旧的网站, 返回URL
*/
301 - 资源被永久转移到其他URL
302 - 资源被暂时转移到其他URL
304 - 协商缓存
400 - 数据格式的区别
401 - 未授权
404 - 页面不存在
500 - 内部服务器错误(有可能是请求数据类型不同入text/html, application/json)
502 - Bad Gateway 无效响应
503 - 服务器超时
504 - 超时
505 - 服务器不支持http版本

2**(响应成功)：表示动作被成功接收、理解和接受
200——表明该请求被成功地完成，所请求的资源发送回客户端
201——提示知道新文件的URL
202——接受和处理、但处理未完成
203——返回信息不确定或不完整
204——请求收到，但返回信息为空
205——服务器完成了请求，用户代理必须复位当前已经浏览过的文件
206——服务器已经完成了部分用户的GET请求
3**(重定向类)：为了完成指定的动作，必须接受进一步处理
300——请求的资源可在多处得到
301——本网页被永久性转移到另一个URL
302——请求的网页被转移到一个新的地址，但客户访问仍继续通过原始URL地址，重定向，新的URL会在response中的Location中返回，浏览器将会使用新的URL发出新的Request。
303——建议客户访问其他URL或访问方式
304——自从上次请求后，请求的网页未修改过，服务器返回此响应时，不会返回网页内容，代表上次的文档已经被缓存了，还可以继续使用
305——请求的资源必须从服务器指定的地址得到
306——前一版本HTTP中使用的代码，现行版本中不再使用
307——申明请求的资源临时性删除
4**(客户端错误类)：请求包含错误语法或不能正确执行
400——客户端请求有语法错误，不能被服务器所理解
401——请求未经授权，这个状态代码必须和WWW-Authenticate报头域一起使用
HTTP 401.1 - 未授权：登录失败
  HTTP 401.2 - 未授权：服务器配置问题导致登录失败
  HTTP 401.3 - ACL 禁止访问资源
  HTTP 401.4 - 未授权：授权被筛选器拒绝
HTTP 401.5 - 未授权：ISAPI 或 CGI 授权失败
402——保留有效ChargeTo头响应
403——禁止访问，服务器收到请求，但是拒绝提供服务
HTTP 403.1 禁止访问：禁止可执行访问
  HTTP 403.2 - 禁止访问：禁止读访问
  HTTP 403.3 - 禁止访问：禁止写访问
  HTTP 403.4 - 禁止访问：要求 SSL
  HTTP 403.5 - 禁止访问：要求 SSL 128
  HTTP 403.6 - 禁止访问：IP 地址被拒绝
  HTTP 403.7 - 禁止访问：要求客户证书
  HTTP 403.8 - 禁止访问：禁止站点访问
  HTTP 403.9 - 禁止访问：连接的用户过多
  HTTP 403.10 - 禁止访问：配置无效
  HTTP 403.11 - 禁止访问：密码更改
  HTTP 403.12 - 禁止访问：映射器拒绝访问
  HTTP 403.13 - 禁止访问：客户证书已被吊销
  HTTP 403.15 - 禁止访问：客户访问许可过多
  HTTP 403.16 - 禁止访问：客户证书不可信或者无效
HTTP 403.17 - 禁止访问：客户证书已经到期或者尚未生效
404——一个404错误表明可连接服务器，但服务器无法取得所请求的网页，请求资源不存在。eg：输入了错误的URL
405——用户在Request-Line字段定义的方法不允许
406——根据用户发送的Accept拖，请求资源不可访问
407——类似401，用户必须首先在代理服务器上得到授权
408——客户端没有在用户指定的饿时间内完成请求
409——对当前资源状态，请求不能完成
410——服务器上不再有此资源且无进一步的参考地址
411——服务器拒绝用户定义的Content-Length属性请求
412——一个或多个请求头字段在当前请求中错误
413——请求的资源大于服务器允许的大小
414——请求的资源URL长于服务器允许的长度
415——请求资源不支持请求项目格式
416——请求中包含Range请求头字段，在当前请求资源范围内没有range指示值，请求也不包含If-Range请求头字段
417——服务器不满足请求Expect头字段指定的期望值，如果是代理服务器，可能是下一级服务器不能满足请求长。
5**(服务端错误类)：服务器不能正确执行一个正确的请求
HTTP 500 - 服务器遇到错误，无法完成请求
  HTTP 500.100 - 内部服务器错误 - ASP 错误
  HTTP 500-11 服务器关闭
  HTTP 500-12 应用程序重新启动
  HTTP 500-13 - 服务器太忙
  HTTP 500-14 - 应用程序无效
  HTTP 500-15 - 不允许请求 global.asa
  Error 501 - 未实现
HTTP 502 - 网关错误
HTTP 503：由于超载或停机维护，服务器目前无法使用，一段时间后可能恢复正常
复制代码
```



# 9.设计模式

# 10.数据结构/算法

## 10.1树

```javascript
------------------------------------
	二叉树公式
-------------
    二叉树的第n层 最多为 2^(n-1)
	1.叶子节点=度为2的节点+1.
	2.节点数-1=度数。

```

## 10.2排序

```javascript
-----------------------------------
	拓扑排序算法
--------------
-----------------------------------
	堆排序算法
--------------
-----------------------------------
	快速排序算法
--------------
	对于已排序数组, 最差
```

### 10.2.1 复杂度

![image-20200717025002912](C:\Users\lenovo\AppData\Roaming\Typora\typora-user-images\image-20200717025002912.png)

## 10.3数组

    数组A的每个元素需要4个字节存放，数组有8行 10列，若数组以行为主顺序存放在内存SA开始的存储区中，则A中8行5列的元素的位置是
    	题目要求8行5列地址，其实求的就是A[7][4]地址，故位置为（7*10 + 4）* 4 = 296

## 10.4栈

```javascript
先进后出, 只在队尾操作
```

## 10.5队列

```javascript
先进先出, 尾插头出
```

## 10.6链表

```javascript

```

## 10.7堆

```javascript
1. 
	最大堆：子节点小于父节点
	最小堆：子节点大于父节点    
	
2. 完全二叉树
```

## 10.8 递归和循环

```javascript

```

|        | 递归 | 循环 |
| ------ | ---- | ---- |
| 效率   | 低   | 高   |
| 可读性 | 高   | 差   |
|        |      |      |

## 10.9 diff算法

```javascript

```



# 11.安全

```javascript
DDos攻击: 控制肉鸡, 向网站发送大量请求
csrf攻击: 让用户在不知情的情况, 冒用其身份发请求
xss攻击: 使用script 发送请求

----------------------------
xss
------------
概念：网页里插入script脚本；例如从cookie里获取数据；
防御手段：对用户提交的url参数进行过滤，对输出进行编码。
//http://localhost:1521/?from=<script>alert(1)</script>bing
富文本
触发标签:img\video\audio\a
onload属性 onfocus属性 onerror属性
--------------------------
csrf
---------------
请求端添加token;
验证http referer字段 记录来源地址
```

## 11.1 接口如何防刷

```javascript 
1：网关控制流量洪峰，对在一个时间段内出现流量异常，可以拒绝请求

2：源ip请求个数限制。对请求来源的ip请求个数做限制

3：http请求头信息校验；（例如host，User-Agent，Referer）

4：对用户唯一身份uid进行限制和校验。例如基本的长度，组合方式，甚至有效性进行判断。或者uid具有一定的时效性

5：前后端协议采用二进制方式进行交互或者协议采用签名机制

6：人机验证，验证码，短信验证码，滑动图片形式，12306形式


```

## 11.2 xss csrf

|          | xss                                                          | csrf                                                         |
| -------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 攻击方式 | 用户通过各种方式将恶意代码注入到其他用户的界面，就可以通过脚本获取信息，发起请求之类的操作<br /><br />URL注入<br /> | 攻击者通过技术手段欺骗用户的浏览器去访问一个自己曾经认证的网站并进行操作<br />**简单的身份验证只能保证请求发自某个用户的浏览器，却不能保证请求本身是用户自愿发出的**。 |
| 举例     | 1. 评论发送代码                                              | 1. 在一个网站登陆，然后去访问了个恶意网站，此时先前的网站登陆信息还没消失，攻击者通过用户浏览器可以访问先前网站的请求 |
| 攻击目标 | cookie和token窦娥可以                                        | 只劫持cookie<br />对方登陆信息还存在。发送东西               |
| 如何防范 | 将重要的cookie标记为http only, 这样的话Javascript 中的document.cookie语句就不能获取到cookie了. 表单数据规定值的类型，例如：年龄应为只能为int、name只能为字母数字组合。。。。 对数据进行Html Encode 处理 过滤或移除特殊的Html标签， 例如:<br /><br /><br />1. 设置http only<br />2. 屏蔽script<br />3. 转移<> <br /> |                                                              |



## 11.3 XSS

```javascript
XSS
XSS是什么
XSS是一种经常出现在web应用中的计算机安全漏洞，它允许恶意web用户将代码植入到提供给其它用户使用的页面中。

比如这些代码包括HTML代码和客户端脚本。攻击者利用XSS漏洞旁路掉访问控制——例如同源策略(same origin policy)。

这种类型的漏洞由于被黑客用来编写危害性更大的网络钓鱼(Phishing)攻击而变得广为人知。

对于跨站脚本攻击，黑客界共识是：跨站脚本攻击是新型的“缓冲区溢出攻击“，而JavaScript是新型的“ShellCode”。

示例：
<script>alert(document.cookie)</script>
特点
能注入恶意的HTML/JavaScript代码到用户浏览的网页上，从而达到Cookie资料窃取、会话劫持、钓鱼欺骗等攻击。
<攻击代码不一定（非要）在 中>

原因
Web浏览器本身的设计不安全。浏览器能解析和执行JS等代码，但是不会判断该数据和程序代码是否恶意。

输入和输出是Web应用程序最基本的交互，而且网站的交互功能越来越丰富。如果在这过程中没有做好安全防护，很容易会出现XSS漏洞。

程序员水平参差不齐，而且大都没有过正规的安全培训，没有相关的安全意识。

XSS攻击手段灵活多变。

危害
盗取各类用户帐号，如机器登录帐号、用户网银帐号、各类管理员帐号
控制企业数据，包括读取、篡改、添加、删除企业敏感数据的能力
盗窃企业重要的具有商业价值的资料
非法转账
强制发送电子邮件
网站挂马
控制受害者机器向其它网站发起攻击
如何防范
将重要的cookie标记为http only, 这样的话Javascript 中的document.cookie语句就不能获取到cookie了.
表单数据规定值的类型，例如：年龄应为只能为int、name只能为字母数字组合。。。。
对数据进行Html Encode 处理
过滤或移除特殊的Html标签， 例如:
```



# 12.Node (后端)

## 12.1 版本

```javascript
11 以前 按自身
11 以后 按浏览器
```

## 12.2 process

```javascript
.nextTick(()=>{ console.log('异步')})
```

## 12.3 后端存储

```javascript
cookie：登陆后后端生成一个sessionid放在cookie中返回给客户端，并且服务端一直记录着这个sessionid，客户端以后每次请求都会带上这个sessionid，服务端通过这个sessionid来验证身份之类的操作。所以别人拿到了cookie拿到了sessionid后，就可以完全替代你。

token：登陆后后端返回一个token给客户端，客户端将这个token存储起来，然后每次客户端请求都需要开发者手动将token放在header中带过去，服务端每次只需要对这个token进行验证就能使用token中的信息来进行下一步操作了。
```

## 12.4 Token

```javascript
1. 需要一个secret（随机数）
2. 后端利用secret和加密算法(如：HMAC-SHA256)对payload(如账号密码)生成一个字符串(token)，返回前端
3. 前端每次request在header中带上token
4. 后端用同样的算法解密
```

### 12.4.1 Token刷新

```javascript
当token过期，用户需要刷新token。刷新token本质上是这样的：
	服务端：这个token是你的吗？
	客户端：是的。
	服务端：当初我给你token的时候，还给了一个摘要，你把摘要拿过来证明。
```



## 12.5 缓存 

## 12.6 判断当前脚本运行环境

```javascript
this === window ? 'browser' : 'node';
```

## 12.7 express

```javascript
0000000000000000000000000000000
var express = require('express');
var app = express();
 
app.get('/', function (req, res) {
   res.send('Hello World');
})
 
var server = app.listen(8081, function () {
 
  var host = server.address().address
  var port = server.address().port
 
  console.log("应用实例，访问地址为 http://%s:%s", host, port)
 
})
111111111111111111111111
var express = require('express');
var app = express();
 
app.use('/public', express.static('public'));
 
app.get('/index.html', function (req, res) {
   res.sendFile( __dirname + "/" + "index.html" );
})
 
app.get('/process_get', function (req, res) {
 
   // 输出 JSON 格式
   var response = {
       "first_name":req.query.first_name,
       "last_name":req.query.last_name
   };
   console.log(response);
   res.end(JSON.stringify(response));
})
 
var server = app.listen(8081, function () {
 
  var host = server.address().address
  var port = server.address().port
 
  console.log("应用实例，访问地址为 http://%s:%s", host, port)
 
})
22222222222222222222222
const expressServer = require('express');
const appS = expressServer();

appS.use('/public', expressServer.static('public'));

appS.get('/index1', function (req: any, res: any) {
    res.sendFile(__dirname + '/public/' + 'index1.html');
});
appS.get('/index1/history', function (req: any, res: any) {
    res.sendFile(__dirname + '/public/' + 'history.html');
});
appS.get('/index1#/about', function (req: any, res: any) {
    res.sendFile(__dirname + '/public/' + 'index3.html');
});

appS.listen(3000, () => {
    console.log('服运行');
});


```

## 12.8数据库

### 1.Sequelize

```javascript
(() => {
    const Sequelize = require('sequelize');
    const config = require('../config/config');

    let sequelize = new Sequelize(
        config.database,
        config.username,
        config.password,
        {
            host: config.host,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 30000,
            },
        },
    );

    let Book = sequelize.define(
        'book',
        {
            bookID: {
                type: Sequelize.STRING(255),
                primaryKey: true,
            },
            bookName: Sequelize.STRING(255),
            author: Sequelize.STRING(255),
            publisher: Sequelize.STRING(255),
            major: Sequelize.STRING(255),
            beiZhu: Sequelize.STRING(255),
        },
        {
            timestamps: false,
            freezeTableName: true,
        },
    );
    /**
     * 插入数据
     */
    var now = new Date();
    Book.create({
        bookID: 'g-' + now,
        bookName: 'test1',
        author: 'test2',
        publisher: 'test3',
        major: 'test4',
        beiZhu: 'test5',
    })
        .then(function (p) {})
        .catch(function (err) {
            console.log('err: ', err);
        });
    Book.create({
        bookID: 'g-1' + now,
        bookName: 'test1',
        author: 'test2',
        publisher: 'test3',
        major: 'test4',
        beiZhu: 'test5',
    })
        .then(function (p) {})
        .catch(function (err) {
            console.log('err: ', err);
        });
    /**
     * 查找
     * */
    (async () => {
        var findAll = await Book.findAll({
            where: {
                bookName: 'test1',
            },
        });
        console.log('findAll: ', findAll);
    })();
    /**
     * 更新
     */

    (async () => {
        var update = await Book.update(
            {
                bookName: '杨晓龙',
            },
            {
                where: {
                    bookName: 'test1',
                },
            },
        );
        console.log('update: ', update);
    })();
    /**
     * 删除
     */

    (async () => {
        var destroy = await Book.destroy({
            where: {
                bookID: 'g-Sun Oct 04 2020 23:42:33 GMT+0800 (GMT+08:00)',
            },
        });
        console.log('destroy: ', destroy);
    })();
})();

```



## !! 框架 !!

### 1.Koa

#### 1.1获取get中的数据

```javascript
/**
 * 知识点:
 * 1. ctx.query
 * 2. ctx.querystring
 */
import { createSecureContext } from "tls";
const Koa = require('koa');
const app = new Koa();
let port = 3000;
app.use(async(ctx:any)=>{
  let url =ctx.url;
  let request =ctx.request;
  /**
   * 获取get请求?后的数据
   */
  let req_query = request.query;
  let req_querystring = request.querystring;

  //从上下文中直接获取
  let ctx_query = ctx.query; //☆
  let ctx_querystring = ctx.querystring; //☆
  ctx.body={
      url,
      req_query,
      req_querystring,
      ctx_query,
      ctx_querystring
  }

});

app.listen(port,()=>{
  console.log(`http://localhost:${port}`);
})
```

#### 1.2获取post数据

```javascript
(()=>{
/**
 * ctx.request:是Koa2中context经过封装的请求对象，它用起来更直观和简单。
 * ctx.req:是context提供的node.js原生HTTP请求对象。这个虽然不那么直观，但是可以得到更多的内容，适合我们深度编程。
 */
//知识点
/**
 * yarn add koa-bodyparser
 * ctx.method
 * ctx.request.body
 */
const Koa = require('koa');
const app = new Koa();
const bodyParser = require('koa-bodyparser');
let port = 3000;

app.use(bodyParser());
app.use(async(ctx: any)=>{
  //当请求为get时
  if(ctx.method === 'GET') {
    let html = `
      <h1>Koa2 request post demo</h1>
      <form method="POST"  action="${ctx.url}">
          <p>userName</p>
          <input name="userName" /> <br/>
          <p>age</p>
          <input name="age" /> <br/>
          <p>webSite</p>
          <input name='webSite' /><br/>
          <button type="submit">submit</button>
      </form>
    `;
    ctx.body = html;
  } else if(ctx.method === 'POST') {
    let postData= ctx.request.body;
    ctx.body=postData;
  } else {
    ctx.body = '404'
  }
})

app.listen(port,()=>{
  console.log(`http://localhost:${port}`);
})
})()
```

#### 1.3koa-router

```javascript
/**
     * 知识点:
     * 添加前缀
     */
const router = require('koa-router')({
    prefix:'/yxl'
})
(()=>{
  /**
   * 知识点: router.use('/url',home.routes())
   */
  const Koa = require('koa');
  const Router = require('koa-router')
  const app = new Koa();
  
  let router = new Router();
  let home = new Router();
  let page = new Router();

  home.get('/jspang',async (ctx:any)=>{
    ctx.body = 'home jspang';
  }).get('/todo',async (ctx:any)=>{
    ctx.body = 'home todo';
  })

  page.get('/jspang',async (ctx:any)=>{
    ctx.body = 'page jspang';
  }).get('/todo',async (ctx:any)=>{
    ctx.body = 'page todo';
  })
  router.use('/home',home.routes());
  router.use('/page',page.routes());

  app.use(router.routes()).use(router.allowedMethods());
  app.listen(3000)

})()
```

#### 1.4cookie

```javascript
设置cookie
    ctx.cookies.set('myname','yxl',{
        domain:'127.0.0.1',
        path:'/',
        maxAge:1000*60*60*24,
        expires: new Date('2020-10-19');
        httpOnly: flase,
        overwrite: false
    })
获取cookie
	ctx.cookies.get('myname')
```

#### 1.5ejs

```javascript
(()=>{
  /**
   * 知识点: 
   *  ejs
   *  views()
   *  ctx.render()
   */
  const Koa = require('koa');
  const views = require('koa-views');
  const path = require('path');
  const app = new Koa();

  app.use(views(path.join(__dirname, './view'),{
    extension:'ejs'
  }))

  app.use(async (ctx: any)=>{
    let title = 'Hello YXL';
    await ctx.render('index',{title});
  })

  app.listen(3000)

})()
```

```ejs
<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <title><%= title %></title>
  </head>

  <body>
    <h1><%= title %></h1>
    <h6>EJS WELCOME ! <%= title %></h6>
  </body>

</html>

```

#### 1.6static

```javascript
  /**
   * 知识点:
   *  静态文件
   */
  const Koa = require('koa');
  const Path = require('path');
  const Static = require('koa-static');
  const app = new Koa();

  const staticPath = './static';
  app.use(Static(Path.join(__dirname,staticPath)));
  app.use(async (ctx: any)=>{
    ctx.body = 'test'
  });
  app.listen(3000)
```



# 13.数据库

```javascript
where不能使用聚合函数、having中可以使用聚合函数
select sum(population),region FROM T01_Beijing GROUP BY region; 

数据库系统达到了数据独立性
	采用了:  三级模式结构
    
--------------------------------
	索引
--------------
```

## 1.PostgreSQL

### 1.1创建数据库

```sql
--创建数据库-------------------------------
create databse dbname; --1
createdb -h localhost -p 5432 -U postgres runoobdb --2
--使用pgAdmin 3
```

### 1.2选择数据库

```sql
\l; --查找已存在数据库
\c dbname --进入数据库
```

### 1.3删除数据库

```sql
drop database dbname;
```

### 1.4创建表格

```sql
CREATE TABLE COMPANY(
   ID INT PRIMARY KEY     NOT NULL,
   NAME           TEXT    NOT NULL,
   AGE            INT     NOT NULL,
   ADDRESS        CHAR(50),
   SALARY         REAL
);
\d --查询表格是否创建成功
\d tablename --查看表格信息;

```

### 1.5删除表格

```sql
drop table table_name; --删除表格
```

### 1.6模式（SCHEMA）

```sql
create schema myschema;
create table myschema.company(
   ID   INT              NOT NULL,
   NAME VARCHAR (20)     NOT NULL,
   AGE  INT              NOT NULL,
   ADDRESS  CHAR (25),
   SALARY   DECIMAL (18, 2),
   PRIMARY KEY (ID)
);
select * from myschema.company;
DROP SCHEMA myschema; --删除为空的
DROP SCHEMA myschema CASCADE; --删除一个模式及其中包含的所有对象
```

### 1.7insert into语句

```sql
INSERT INTO TABLE_NAME (column1, column2, column3,...columnN) VALUES (value1, value2, value3,...valueN); --一一对应
INSERT INTO TABLE_NAME VALUES (value1,value2,value3,...valueN); --顺序全写
```

### 1.8select语句

```sql
SELECT column1, column2,...columnN FROM table_name; --选择性筛选
SELECT * FROM table_name; --获取全部
```

### 1.9WHERE (运算符)

```sql
SELECT * FROM COMPANY WHERE SALARY > 50000; --条件
SELECT * FROM COMPANY WHERE SALARY <> 20000; --不等于
SELECT COUNT(*) AS "RECORDS" FROM COMPANY; --
--逻辑运算符and not or
SELECT * FROM COMPANY WHERE AGE >= 25 AND SALARY >= 65000;--and
SELECT * FROM COMPANY WHERE AGE >= 25 OR SALARY >= 65000;--or
SELECT * FROM COMPANY WHERE AGE IS NOT NULL; --is not null;
SELECT * FROM COMPANY WHERE NAME LIKE 'Pa%'; --like
SELECT * FROM COMPANY WHERE AGE IN ( 25, 27 ); --in;
SELECT * FROM COMPANY WHERE AGE NOT IN ( 25, 27 ); --not in;
SELECT * FROM COMPANY WHERE AGE BETWEEN 25 AND 27; --between
SELECT AGE FROM COMPANY WHERE EXISTS (SELECT AGE FROM COMPANY WHERE SALARY > 65000); -- 列举存在的数据
SELECT * FROM COMPANY WHERE AGE > (SELECT AGE FROM COMPANY WHERE SALARY > 65000); -- 单数据比对
```

### 2.0 and & or运算符

```sql
SELECT * FROM COMPANY WHERE AGE >= 25 AND SALARY >= 65000; --
```

### 2.1update更新

```sql
UPDATE COMPANY
	SET SALARY = 15000
	WHERE ID = 3;  -- 更新
UPDATE COMPANY SET ADDRESS = 'Texas', SALARY=20000; -- 多段更新	
```

### 2.2delete

```sql
delete from company where id =2; --删除行
delete from company; --删除表
```

### 2.3通配符(like)

```sql
SELECT FROM table_name WHERE column LIKE '_XXXX_';
200% --200开头
%200% --含有200
_200% --第2 3 4 位置 分别为 2 0 0
2 % % --以2开头长度大于3
%2 --以2结尾
_2%3 --2在第二个位置上以3结尾
2___3 --2开头 3结尾 5位数据
```

### 2.4LIMIT

```sql
SELECT * FROM COMPANY LIMIT 4; --选四个数据
SELECT * FROM COMPANY LIMIT 3 OFFSET 2; --从第二个开始选三个数据
```

### 2.5orderby

```sql
SELECT * FROM COMPANY ORDER BY AGE ASC; --ASC升序/DESC降序
SELECT * FROM COMPANY ORDER BY NAME, SALARY ASC; --多字段排序
```

### 2.6gourpby 集合

```sql
SELECT NAME, SUM(SALARY) FROM COMPANY GROUP BY NAME; --所有人的薪水集合
```

### 2.7with 视图

```sql
With CTE AS
(Select
 ID
, NAME
, AGE
, ADDRESS
, SALARY
FROM COMPANY )
--创建视图
Select * From CTE;
------------------------------------
WITH RECURSIVE t(n) AS (
   VALUES (0)
   UNION ALL
   SELECT SALARY FROM COMPANY WHERE SALARY < 20000
)
SELECT sum(n) FROM t;
```

### 2.8having子句  集合中的where

```sql
SELECT
FROM
WHERE
GROUP BY
HAVING
ORDER BY
--语句顺序
SELECT NAME FROM COMPANY GROUP BY name HAVING count(name) < 2; --名字字段小于2
```

### 2.9distinct 去重

```sql
SELECT DISTINCT name FROM COMPANY; --去重
```



# 14.其他

# 15.面经

# 16.操作系统

```javascript
用于设备和设备控制器（I/O 接口）之间互连的接口标准（ USB ）。

-------------------------------
    冯诺依曼结构
----------------
 1. CPU：控制器、运算器、寄存器
 2. 存储器：内存
 3. I/O设备：输入输出设备
	工作过程： 人们预先编制程序，利用输入设备将程序输入到计算机内，并转换成二进制代码，计算机在控制器的控制下，从内存中逐条取出程序中的每一指令交给计算机去执行，并将运算结果送回存储器指定的单元中，当所有的运算任务完成后，程序执行结果利用输出设备输出。

---------------------------------
    线程与进程
-------------------
        进程 = 火车, 线程 = 车厢
	0000000000000000000000000
		进程
	00000000000
		1. 资源分配的最小单位
		2. 进程有自己独立的地址空间, 每启动一个进程, 系统都会为其分配地址空间
		3. CPU 切换与创建 进程花费多
		4. 占用资源多
		5. 效率低, 开销大       
		6. 虚拟内存、栈、全局变量等用户空间的资源，还包括了内核堆栈、寄存器等内核空间的资源。
	0000000000000000000000000
		线程
	00000000000
		1. 程序执行(CPU调度)的最小单位
		2. 没有独立的地址空间, 使用相同的地址空间共享数据       
		3. CPU 切换与创建 进程花费小
		4. 占用资源少   
		5. 开笑笑, 效率高

00000000000000000000000000000000000
	用户态和内核态
00000000000000000
	内核态：控制计算机的硬件资源，并提供上层应用程序运行的环境。
    
	用户态：上层应用程序的活动空间，应用程序的执行必须依托于内核提供的资源。
    
	系统调用：为了使上层应用能够访问到这些资源，内核为上层应用提供访问的接口。
    
	用户态 较于 内核态 执行权限低, 很多操作不被允许。
```

###  16.1 线程与进程

|      | 最小单位    | 拟态             | 进程/线程之间 | 内存           |
| ---- | ----------- | ---------------- | ------------- | -------------- |
| 进程 | CPU资源分配 | 单独的工程       | 独立          | 内存共享       |
| 线程 | CPU调度     | 多个工人同个工厂 |               | 使用线程的内存 |

```javascript
00000000000000000000000000
	多进程
00000000000
	比如你可以听歌的同时，打开编辑器敲代码，编辑器和听歌软件的进程之间丝毫不会相互干扰。
    浏览器的一个tab就说一个进程。
00000000000000000000000000
	多线程
00000000000
	程序中包含多个执行流，即在一个程序中可以同时运行多个不同的线程来执行不同的任务，也就是说允许单个程序创建多个并行执行的线程来完成各自的任务。
```



# 17.浏览器

## 17.1 浏览器渲染过程

```javascript
000000000000000000000000
	浏览器渲染过程
0000000000
1. 解析HTML，得到一个DOM tree
2. 解析CSS，得到CSSOM tree
3. 将两者整合成渲染树，render tree
4. 布局（layout）， 根据Render Tree计算每个节点的位置大小等信息 （在草稿本上画了个草图）（重排）
5. 绘制（Painting ）根据计算好的信息绘制整个页面（重绘）

```

### 超详细版

```javascript
1、浏览器会开启一个线程来处理这个请求，对 URL 分析判断如果是 http 协议就按照 Web 方式来处理;
2、调用浏览器内核中的对应方法，比如 WebView 中的 loadUrl 方法;
  3、通过DNS解析获取网址的IP地址，设置 UA 等信息发出第二个GET请求;
4、进行HTTP协议会话，客户端发送报头(请求报头);
  5、进入到web服务器上的 Web Server，如 Apache、Tomcat、Node.JS 等服务器;
  6、进入部署好的后端应用，如 PHP、Java、JavaScript、Python 等，找到对应的请求处理;
7、处理结束回馈报头，此处如果浏览器访问过，缓存上有对应资源，会与服务器最后修改时间对比，一致则返回304;
  8、浏览器开始下载html文档(响应报头，状态码200)，同时使用缓存;
  9、文档树建立，根据标记请求所需指定MIME类型的文件（比如css、js）,同时设置了cookie;
  10、页面开始渲染DOM，JS根据DOM API操作DOM,执行事件绑定等，页面显示完成。
```



### 17.1.1 回流/重绘

```javascript

0000000000000000000000000000
	重排(回流)和重绘
000000000000
	回流/重排,重新计算节点的位置大小。
	111111111111111111111
		性能优化
	111111111111
		1. 减少DOM操作
		3. 采用更优的API代替消费高的api
		5. CSS及动画处理 设置absolute或fixed
         6. transform 替代 top
         7. visibility 替换 display:none
         8. 避免使用table布局
         9. 尽可能在dom数根部变化
         10. 避免设置多层内敛样式
         11. 设置图层
         
         documentFragment
```

## 17.2 浏览器本地存储

### 17.2.1 cookie/session

```javascript
 cookie: 前后端均可以设置
 	最大4kb
 	firefox 可存150个
	ie6 50个
 请求头 包含 Cookie
 相应头包含 Set-Cookie
//设置方式
document.cookie = "user=John; path=/; expires=Tue, 19 Jan 2038 03:14:07 GMT;domain=site.com"

```

区别

|          | cookie     | session |
| -------- | ---------- | ------- |
| 存储位置 | 客户浏览器 | 服务器  |
| 安全性   | 不安全     | 安全    |
| 性能     | 高性能     | 性能低  |



### 17.2.2 LocalStorage/sessionStorage



区别 5M或更大

|          | localStorage                     | sessionStorage |
| -------- | -------------------------------- | -------------- |
| 生命周期 | 永久存储                         | 窗口关闭则消失 |
| 设置方法 | .setItem("lastname", "Smith");   |                |
| 获取方法 | .getItem("lastname");            |                |
| 删除方法 | .removeItem("lastname");.clear() |                |
| 获得键名 | .key("lastname")                 |                |
| 长度获取 | .length;                         |                |

## 17.3 浏览器内核

```javascript
浏览器内核是多线程，在内核控制下各线程相互配合以保持同步，一个浏览器通常由以下常驻线程组成：
    GUI 渲染线程
    JavaScript引擎线程
    定时触发器线程
    事件触发线程
    异步http请求线程
    
1.GUI渲染线程
    主要负责页面的渲染，解析HTML、CSS，构建DOM树，布局和绘制等。
    当界面需要重绘或者由于某种操作引发回流时，将执行该线程。
    该线程与JS引擎线程互斥，当执行JS引擎线程时，GUI渲染会被挂起，当任务队列空闲时，主线程才会去执行GUI渲染。
2.JS引擎线程
    该线程当然是主要负责处理 JavaScript脚本，执行代码。
    也是主要负责执行准备好待执行的事件，即定时器计数结束，或者异步请求成功并正确返回时，将依次进入任务队列，等待 JS引擎线程的执行。
    当然，该线程与 GUI渲染线程互斥，当 JS引擎线程执行 JavaScript脚本时间过长，将导致页面渲染的阻塞。
3.定时器触发线程
    负责执行异步定时器一类的函数的线程，如： setTimeout，setInterval。
    主线程依次执行代码时，遇到定时器，会将定时器交给该线程处理，当计数完毕后，事件触发线程会将计数完毕后的事件加入到任务队列的尾部，等待JS引擎线程执行。
4.事件触发线程
    主要负责将准备好的事件交给 JS引擎线程执行。
    比如 setTimeout定时器计数结束， ajax等异步请求成功并触发回调函数，或者用户触发点击事件时，该线程会将整装待发的事件依次加入到任务队列的队尾，等待 JS引擎线程的执行。

5.异步http请求线程
    负责执行异步请求一类的函数的线程，如： Promise，axios，ajax等。
    主线程依次执行代码时，遇到异步请求，会将函数交给该线程处理，当监听到状态码变更，如果有回调函数，事件触发线程会将回调函数加入到任务队列的尾部，等待JS引擎线程执行。
```

## 17.4 兼容性

-   浏览器默认的margin和padding不同。解决方案是加一个全局的*{margin:0;padding:0;}来统一。

-   IE6双边距bug:块属性标签float后，又有横行的margin情况下，在ie6显示margin比设置的大。

    浮动ie产生的双倍距离 #box{ float:left; width:10px; margin:0 0 0 100px;}

    这种情况之下IE会产生20px的距离，解决方案是在float的标签样式控制中加入 ——*display:inline;将其转化为行内属性。(*这个符号只有ie6会识别)

    渐进识别的方式，从总体中逐渐排除局部。

    首先，巧妙的使用“\9”这一标记，将IE游览器从所有情况中分离出来。 接着，再次使用“+”将IE8和IE7、IE6分离开来，这样IE8已经独立识别。

    css .bb{ background-color:red;/*所有识别*/ background-color:#00deff\9; /*IE6、7、8识别*/ +background-color:#a200ff;/*IE6、7识别*/ _background-color:#1e0bd1;/*IE6识别*/ }

-   IE下,可以使用获取常规属性的方法来获取自定义属性, 也可以使用getAttribute()获取自定义属性; Firefox下,只能使用getAttribute()获取自定义属性。 解决方法:统一通过getAttribute()获取自定义属性。

-   IE下,even对象有x,y属性,但是没有pageX,pageY属性; Firefox下,event对象有pageX,pageY属性,但是没有x,y属性。

-   解决方法：（条件注释）缺点是在IE浏览器下可能会增加额外的HTTP请求数。

-   Chrome 中文界面下默认会将小于 12px 的文本强制按照 12px 显示, 可通过加入 CSS 属性 -webkit-text-size-adjust: none; 解决。

超链接访问过后hover样式就不出现了 被点击访问过的超链接样式不在具有hover和active了解决方法是改变CSS属性的排列顺序: L-V-H-A : a:link {} a:visited {} a:hover {} a:active {}

## 17.5 浏览器版本检测

```javascript
  功能检测、userAgent特征检测
  比如：navigator.userAgent
  //"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36
    (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36"
```



# 18. jquery



```javascript

$('').hide()

/** 实现
var $ = function(selector, context) {
    return new $.fn.init(selector, context);
};

$.fn = $.prototype;

$.fn.init = function(selector, context) {
    var nodeList = (context || document).querySelectorAll(selector);
    this.length = nodeList.length;
    for (var i=0; i<this.length; i+=1) {
        this[i] = nodeList[i];    
    }
    return this;
};
$.fn.each = function(fn) {
    var i=0, length = this.length;
    for (; i<length; i+=1) {
        fn.call(this[i], i, this[i]);
    }
    return this;
};

$.fn.hide = function() {
    this.each(function() {
       this.style .display = "none";
    });
};

$.fn.init.prototype = $.fn;


$("button")[0].onclick = function() {
    $("img").hide();  
};
*/
```

# 19. 兼容性

```javascript
触发事件的元素被认为是目标（target）。而在 IE 中，目标包含在 event 对象的 srcElement 属性；

获取字符代码、如果按键代表一个字符（shift、ctrl、alt除外），IE 的 keyCode 会返回字符代码（Unicode），DOM 中按键的代码和字符是分离的，要获取字符代码，需要使用 charCode 属性；

阻止某个事件的默认行为，IE 中阻止某个事件的默认行为，必须将 returnValue 属性设置为 false，Mozilla 中，需要调用 preventDefault() 方法；

停止事件冒泡，IE 中阻止事件进一步冒泡，需要设置 cancelBubble 为 true，Mozzilla 中，需要调用 stopPropagation()；

```

IE的 script 元素支持onreadystatechange事件，不支持onload事件。

FF的script 元素不支持onreadystatechange事件，只支持onload事件。

## 19.1 Polyfill （兼容处理工具）

1.  html5shiv

# 20. 优化

## 20.1 前端性能优化

```javascript
（1） 减少http请求次数：CSS Sprites, JS、CSS源码压缩、图片大小控制合适；网页Gzip，CDN托管，data缓存 ，图片服务器。
（2） 前端模板 JS+数据，减少由于HTML标签导致的带宽浪费，前端用变量保存AJAX请求结果，每次操作本地变量，不用请求，减少请求次数
（3） 用innerHTML代替DOM操作，减少DOM操作次数，优化javascript性能。
（4） 当需要设置的样式很多时设置className而不是直接操作style。
（5） 少用全局变量、缓存DOM节点查找的结果。减少IO读取操作。
（6） 避免使用CSS Expression（css表达式)又称Dynamic properties(动态属性)。
（7） 图片预加载，将样式表放在顶部，将脚本放在底部  加上时间戳。
（8） 避免在页面的主体布局中使用table，table要等其中的内容完全下载之后才会显示出来，显示比div+css布局慢。
对普通的网站有一个统一的思路，就是尽量向前端优化、减少数据库操作、减少磁盘IO。向前端优化指的是，在不影响功能和体验的情况下，能在浏览器执行的不要在服务端执行，能在缓存服务器上直接返回的不要到应用服务器，程序能直接取得的结果不要到外部取得，本机内能取得的数据不要到远程取，内存能取到的不要到磁盘取，缓存中有的不要去数据库查询。减少数据库操作指减少更新次数、缓存结果减少查询次数、将数据库执行的操作尽可能的让你的程序完成（例如join查询），减少磁盘IO指尽量不使用文件系统作为缓存、减少读写文件次数等。程序优化永远要优化慢的部分，换语言是无法“优化”的。
复制代码
```



# 21. 坑

```javascript
1、父元素设置了border-radius，子元素应用了transform，并且父元素设置了overflow:hidden，但是却失效？
{
    position:relative;
    z-index:1;
}
2、设置input 文本框的 placeholder 的颜色
input::-webkit-input-placeholder{
    color:rgba(144,147,153,1);
}
复制代码
3、如何设置body背景色，height:100%,不生效？
同时设置html，body的高度
html,body{
    height:100%；
} 
或
body{
  height: 100vh; // 代表占屏幕100%
}
复制代码
4、一像素边框的问题
.row {
  position: relative;
  &:after{
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 200%;
    border-bottom:1px solid #e6e6e6;
    color: red;
    height: 200%;
    -webkit-transform-origin: left top;
    transform-origin: left top;
    -webkit-transform: scale(0.5);
    transform: scale(0.5);
    pointer-events: none; /* 防止点击触发 */
    box-sizing: border-box;
  }
}
复制代码
5、css属性touch-action:none;
该属性会导致安卓页面无法滚动，慎用!
复制代码
6、去除ios 手机端input输入框的内阴影
input{ 
    -webkit-appearance: none; 
}
复制代码
7、安卓手机端div里面在包一层div，文字展示不居中的问题。
最好给div设置padding ，不固定高度和不设置line-height；
复制代码
8、iOS端input输入框光标错位
是由于fixed定位引起的，改成absolute就解决了。

.box{
    position: absolute; 
}
复制代码
9、div实现背景色和背景图片同时存在
{
    background-color: #fff;
    background-image:url('../../assets/img/model-bg.png');
    background-repeat: no-repeat;
}
复制代码
10、css 制作椭圆
border-radius可以单独设置水平和垂直的半径，只需要用一个斜杠（/）分隔这二个值就行。

此外我们还要知道border-radius不仅可以接受长度值还可以接受百分比值。

{
    width: 150px;
    height: 100px;
    border-radius: 50%/50%; //简写属性：border-radius:50%
    background: brown;
}
复制代码
11、图片居中显示
object-fit: cover;
复制代码
兼容问题
1、iconfont 字体在钉钉应用里面加载不出来（或者是乱码）的问题引入iconfont字体的时候，需要按照顺序把字体依次引入。正确的顺序如下：
@font-face {
    font-family: "djicon";
    src: url('./iconfont.eot'); /* IE9*/
    src: url('./iconfont.svg#iconfont') format('svg'), /* iOS 4.1- */
    url('./iconfont.woff') format('woff'), /* chrome、firefox */
    url('./iconfont.ttf') format('truetype'); /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
}
// 参考文档：https://www.cnblogs.com/Megasu/p/4305116.html
复制代码
2、PC端ueditor 编辑的文案及图片拉伸问题，背景是 后端返回是html片段
node - cheerio模块，操作dom字符串比较方便，实际案例：
解决了 PC端图片在移动端展示的问题参考文档：
www.jianshu.com/p/e6d73d8fa…
www.npmjs.com/package/che…

width: number = 784 代表pc端宽度 
regHtml(str: string){
    // 参数是html片段 
    let _this = this; 
    const $ = cheerio.load(str);
    $('img').each(function(index,element){
        let attr = element.attribs //元素的属性 
        // 屏幕宽度 
        let docEl = document.documentElement 
        let clientWidth = docEl.clientWidth
        if(attr.width){ //如果设置了宽
            // 图片宽度/屏幕宽度的比例 
            let rate = attr.width /_this.width 
            //图片的宽高比例 
            let wh = attr.width/attr.height 
            $(element).attr('height', _this.rate*clientWidth/wh)
            $(element).attr('width', _this.rate*clientWidth)
            $(element).attr('style', '') 
            $(element).attr('class', 'img-skew') 
        } 
    }) 
    return $.html() 
}
复制代码
3、IOS 点击input不聚焦问题。
ios系统中点击Input输入框，没有反应，无法聚集光标，调不起键盘。

解决方案：强制性给加上点击事件，点击后给input框聚集光标。

cilckTextarea(){
    document.getElementsByClassName('cont-inp')[0].focus();
},
复制代码
4、上传图片，iPhone7 iPhone7p在上传图片的时候，传不过去图片的name
解决方案：手动添加图片name

let data = new FormData();
data.append("fileName", file[0],file[0].name); 
复制代码
5、ios微信打开网页键盘弹起后页面上滑，导致弹框里的按钮响应区域错位
解决方案：手动把滚动条滚到底部写一个自定义指令。

import Vue from 'vue';
Vue.directive('blur', {
    'bind'(el) {
        el.addEventListener("click", function(){
            window.scrollTo(0,0);
        })
    }
}); 
//在点击页面提交按钮的时候，把滚动条滚到底部就OK了
复制代码
6、微信浏览器调整字体后，页面错位。
解决方案：阻止页面字体自动调整大小

// 安卓：
(function() {
  if (typeof WeixinJSBridge == "object" && typeof WeixinJSBridge.invoke == "function") {
    handleFontSize();
  } else {
    if (document.addEventListener) {
      document.addEventListener("WeixinJSBridgeReady", handleFontSize, false);
    } else if (document.attachEvent) {
      //IE浏览器，非W3C规范
      document.attachEvent("onWeixinJSBridgeReady", handleFontSize);
    }
  }
  function handleFontSize() {
    // 设置网页字体为默认大小
    WeixinJSBridge.invoke('setFontSizeCallback', { 'fontSize' : 0 });
    // 重写设置网页字体大小的事件
    WeixinJSBridge.on('menu:setfont', function() {
      WeixinJSBridge.invoke('setFontSizeCallback', { 'fontSize' : 0 });
    });
  }
})();
//iOS：
// ios使用-webkit-text-size-adjust禁止调整字体大小
body{-webkit-text-size-adjust: 100%!important;}
复制代码
7、关于移动端样式兼容的问题
设置meta标签viewport属性，使其无视设备的真实分辨率，直接通过dpi，在物理尺寸和浏览器之间重设分辨率，从而达到能有统一的分辨率的效果。并且禁止掉用户缩放
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
复制代码
使用rem进行屏幕适配，设置好root元素的font-size大小，然后在开发的时候，所有与像素有关的布局统一换成rem单位。
8、iOS下取消input在输入的时候英文首字母的默认大写
<input type="text" autocapitalize="none">
复制代码
9、禁止 iOS 识别长串数字为电话
<meta name="format-detection" content="telephone=no" />
复制代码
10、禁止ios和android用户选中文字
-webkit-user-select: none;
复制代码
11、一些情况下对非可点击元素如(label,span)监听click事件，ios下不会触发
只需要对不触发click事件的元素添加一行css代码即可：

cursor: pointer;
复制代码
调试工具
1、Weinre远程调试工具
简单的步骤：

- 本地全局安装weinre , 命令行：npm install -g weinre
- 在本地启动一个检测器：weinre --boundHost 1.2.3.4 （IP为本地IP地址）
- 在浏览器访问此地址：http://1.2.3.4:8080
- 把下面这一串东西，放在你需要调试的页面里：
<script src="http://1.2.3.4:8080/target/target-script-min.js#anonymous"></script>
- 点击链接打开：http://1.2.3.4:8080/client/#anonymous
复制代码
2、vconsole 日志打印
import VConsole from 'vconsole'
var vConsole = new VConsole();
复制代码
3、fiddler抓包也需要支持https
解决方案：fiddler需要安装信任证书，手机也需要安装信任证书，在浏览器里面打开

http://1.2.3.4:8888 // 本地IP地址+端口号
复制代码
4、charles 手机端抓包，需要安装证书
手机浏览器输入：chls.pro/ssl ，去下载证书。
手机-设置-安全与隐私-更多安全下载-从sd卡安装证书 - 找到之前在浏览器下载的证书
复制代码
vue相关问题
1、vue 如何获取拉回数据后图片的高度？
<img
    :src="userInfo.profilePicture"
    alt
    class="img-picture"
    v-if="userInfo.profilePicture"
    ref="myImg"
    @load="imageFn"
>
 imageFn() {
    console.log(this.$refs.myImg.offsetHeight);
    console.log(this.$refs.myImg.offsetWidth);
 },
复制代码
2、vue中同一个dom节点，v-if与v-for不推荐同时使用，官方解答：
不推荐同时使用 v-if 和 v-for。 当 v-if 与 v-for 一起使用时，v-for 具有比 v-if 更高的优先级

3、vue v-for 中更改item 属性值后，v-show不生效的问题
添加this.$forceUpdate();进行强制渲染，效果实现。
因为数据层次太多，render函数没有自动更新，需手动强制刷新。
复制代码
4、这个离开守卫通常用来禁止用户在还未保存修改前突然离开。该导航可以通过 next(false) 来取消
beforeRouteLeave(to, from, next) {
    if (to.path === '/votes/subject') {
        next('/task-list');
    } else {
        next();
    }
}
复制代码
5、element UI 自定义传参的解决方法
这里的handleSelect默认绑定的参数是选中的那条数据。如果一个页面有好几个相同的组件，要想知道选的是哪个？

<el-autocomplete
    v-model="state4"
    :fetch-suggestions="querySearchAsync"
    placeholder="请输入内容"
    @select="handleSelect"
></el-autocomplete>
复制代码
解决方案：

<el-autocomplete
    v-model="state4"
    :fetch-suggestions="querySearchAsync"
    placeholder="请输入内容"
    @select="((item)=>{handleSelect(item, index)})"
    // 写个闭包就可以了，index表示第几个组件
></el-autocomplete>
复制代码
6、element-UI 框架 el-input 触发不了 @key.enter事件
<el-input v-model="form.loginName" 
placeholder="账号" 
@keyup.enter="doLogin">
</el-input>
复制代码
解决方案：使用@key.center.native

<el-input v-model="form.loginName"
placeholder="账号" 
@keyup.enter.native="doLogin">
</el-input>
```

# 22 -- vue

**1.vue优点？**
答：轻量级框架：只关注视图层，是一个构建数据的视图集合，大小只有几十kb；
简单易学：国人开发，中文文档，不存在语言障碍 ，易于理解和学习；
双向数据绑定：保留了angular的特点，在数据操作方面更为简单；
组件化：保留了react的优点，实现了html的封装和重用，在构建单页面应用方面有着独特的优势；
视图，数据，结构分离：使数据的更改更为简单，不需要进行逻辑代码的修改，只需要操作数据就能完成相关操作；
虚拟DOM：dom操作是非常耗费性能的， 不再使用原生的dom操作节点，极大解放dom操作，但具体操作的还是dom不过是换了另一种方式；
运行速度更快:相比较与react而言，同样是操作虚拟dom，就性能而言，vue存在很大的优势。
**2.vue父组件向子组件传递数据？**
答：通过props
**3.子组件像父组件传递事件？**
答：$emit方法
**4.v-show和v-if指令的共同点和不同点？**
答: 共同点：都能控制元素的显示和隐藏；
不同点：实现本质方法不同，v-show本质就是通过控制css中的display设置为none，控制隐藏，只会编译一次；v-if是动态的向DOM树内添加或者删除DOM元素，若初始值为false，就不会编译了。而且v-if不停的销毁和创建比较消耗性能。
总结：如果要频繁切换某节点，使用v-show(切换开销比较小，初始开销较大)。如果不需要频繁切换某节点使用v-if（初始渲染开销较小，切换开销比较大）。
**5.如何让CSS只在当前组件中起作用？**
答：在组件中的style前面加上scoped
**6.的作用是什么?**
答:keep-alive 是 Vue 内置的一个组件，可以使被包含的组件保留状态，或避免重新渲染。
**7.如何获取dom?**
答：ref=”domName” 用法：this.$refs.domName
**8.说出几种vue当中的指令和它的用法？**
答：v-model双向数据绑定；
v-for循环；
v-if v-show 显示与隐藏；
v-on事件；v-once: 只绑定一次。
**9. vue-loader是什么？使用它的用途有哪些？**
答：vue文件的一个加载器，将template/js/style转换成js模块。
用途：js可以写es6、style样式可以scss或less、template可以加jade等
**10.为什么使用key?**
答：需要使用key来给每个节点做一个唯一标识，Diff算法就可以正确的识别此节点。
作用主要是为了高效的更新虚拟DOM。
**11.axios及安装?**
答：请求后台资源的模块。npm install axios —save装好，
js中使用import进来，然后.get或.post。返回在.then函数中如果成功，失败则是在.catch函数中。
**12.v-modal的使用。**
答：v-model用于表单数据的双向绑定，其实它就是一个语法糖，这个背后就做了两个操作：
v-bind绑定一个value属性；
v-on指令给当前元素绑定input事件。
**13.请说出vue.cli项目中src目录每个文件夹和文件的用法？**
答：assets文件夹是放静态资源；components是放组件；router是定义路由相关的配置; app.vue是一个应用主组件；main.js是入口文件。
**14.分别简述computed和watch的使用场景**
答：computed:
　　　　当一个属性受多个属性影响的时候就需要用到computed
　　　　最典型的栗子： 购物车商品结算的时候
watch:
　　　　当一条数据影响多条数据的时候就需要用watch
　　　　栗子：搜索数据
**15.v-on可以监听多个方法吗？**
答：可以，栗子：。
**16.$nextTick的使用**
答：当你修改了data的值然后马上获取这个dom元素的值，是不能获取到更新后的值，
你需要使用$nextTick这个回调，让修改后的data值渲染更新到dom元素之后在获取，才能成功。
**17.vue组件中data为什么必须是一个函数？**
答：因为JavaScript的特性所导致，在component中，data必须以函数的形式存在，不可以是对象。
　　组建中的data写成一个函数，数据以函数返回值的形式定义，这样每次复用组件的时候，都会返回一份新的data，相当于每个组件实例都有自己私有的数据空间，它们只负责各自维护的数据，不会造成混乱。而单纯的写成对象形式，就是所有的组件实例共用了一个data，这样改一个全都改了。
**18.渐进式框架的理解**
答：主张最少；可以根据不同的需求选择不同的层级；
**19.Vue中双向数据绑定是如何实现的？**
答：vue 双向数据绑定是通过 数据劫持 结合 发布订阅模式的方式来实现的， 也就是说数据和视图同步，数据发生变化，视图跟着变化，视图变化，数据也随之发生改变；
核心：关于VUE双向数据绑定，其核心是 Object.defineProperty()方法。
**20.单页面应用和多页面应用区别及优缺点**
答：单页面应用（SPA），通俗一点说就是指只有一个主页面的应用，浏览器一开始要加载所有必须的 html, js, css。所有的页面内容都包含在这个所谓的主页面中。但在写的时候，还是会分开写（页面片段），然后在交互的时候由路由程序动态载入，单页面的页面跳转，仅刷新局部资源。多应用于pc端。
多页面（MPA），就是指一个应用中有多个页面，页面跳转时是整页刷新
单页面的优点：
用户体验好，快，内容的改变不需要重新加载整个页面，基于这一点spa对服务器压力较小；前后端分离；页面效果会比较炫酷（比如切换页面内容时的专场动画）。
单页面缺点：
不利于seo；导航不可用，如果一定要导航需要自行实现前进、后退。（由于是单页面不能用浏览器的前进后退功能，所以需要自己建立堆栈管理）；初次加载时耗时多；页面复杂度提高很多。
**21.v-if和v-for的优先级**
答：当 v-if 与 v-for 一起使用时，v-for 具有比 v-if 更高的优先级，这意味着 v-if 将分别重复运行于每个 v-for 循环中。所以，不推荐v-if和v-for同时使用。
如果v-if和v-for一起用的话，vue中的的会自动提示v-if应该放到外层去。
**22.assets和static的区别**
答：相同点：assets和static两个都是存放静态资源文件。项目中所需要的资源文件图片，字体图标，样式文件等都可以放在这两个文件下，这是相同点
不相同点：assets中存放的静态资源文件在项目打包时，也就是运行npm run build时会将assets中放置的静态资源文件进行打包上传，所谓打包简单点可以理解为压缩体积，代码格式化。而压缩后的静态资源文件最终也都会放置在static文件中跟着index.html一同上传至服务器。static中放置的静态资源文件就不会要走打包压缩格式化等流程，而是直接进入打包好的目录，直接上传至服务器。因为避免了压缩直接进行上传，在打包时会提高一定的效率，但是static中的资源文件由于没有进行压缩等操作，所以文件的体积也就相对于assets中打包后的文件提交较大点。在服务器中就会占据更大的空间。
建议：将项目中template需要的样式文件js文件等都可以放置在assets中，走打包这一流程。减少体积。而项目中引入的第三方的资源文件如iconfoont.css等文件可以放置在static中，因为这些引入的第三方文件已经经过处理，我们不再需要处理，直接上传。
**23.vue常用的修饰符**
答：.stop：等同于JavaScript中的event.stopPropagation()，防止事件冒泡；
.prevent：等同于JavaScript中的event.preventDefault()，防止执行预设的行为（如果事件可取消，则取消该事件，而不停止事件的进一步传播）；
.capture：与事件冒泡的方向相反，事件捕获由外到内；
.self：只会触发自己范围内的事件，不包含子元素；
.once：只会触发一次。
**24.vue的两个核心点**
答：数据驱动、组件系统
数据驱动：ViewModel，保证数据和视图的一致性。
组件系统：应用类UI可以看作全部是由组件树构成的。
**25.vue和jQuery的区别**
答：jQuery是使用选择器（$）选取DOM对象，对其进行赋值、取值、事件绑定等操作，其实和原生的HTML的区别只在于可以更方便的选取和操作DOM对象，而数据和界面是在一起的。比如需要获取label标签的内容：$(“lable”).val();,它还是依赖DOM元素的值。
Vue则是通过Vue对象将数据和View完全分离开来了。对数据进行操作不再需要引用相应的DOM对象，可以说数据和View是分离的，他们通过Vue对象这个vm实现相互的绑定。这就是传说中的MVVM。
**26. 引进组件的步骤**
答: 在template中引入组件；
在script的第一行用import引入路径；
用component中写上组件名称。
**27.delete和Vue.delete删除数组的区别**
答：delete只是被删除的元素变成了 empty/undefined 其他的元素的键值还是不变。Vue.delete 直接删除了数组 改变了数组的键值。
**28.SPA首屏加载慢如何解决**
答：安装动态懒加载所需插件；使用CDN资源。
**29.Vue-router跳转和location.href有什么区别**
答：使用location.href=’/url’来跳转，简单方便，但是刷新了页面；
使用history.pushState(‘/url’)，无刷新页面，静态跳转；
引进router，然后使用router.push(‘/url’)来跳转，使用了diff算法，实现了按需加载，减少了dom的消耗。
其实使用router跳转和使用history.pushState()没什么差别的，因为vue-router就是用了history.pushState()，尤其是在history模式下。
**30. vue slot**
答：简单来说，假如父组件需要在子组件内放一些DOM，那么这些DOM是显示、不显示、在哪个地方显示、如何显示，就是slot分发负责的活。
**31.你们vue项目是打包了一个js文件，一个css文件，还是有多个文件？**
答：根据vue-cli脚手架规范，一个js文件，一个CSS文件。
**32.Vue里面router-link在电脑上有用，在安卓上没反应怎么解决？**
答：Vue路由在Android机上有问题，babel问题，安装babel polypill插件解决。
**33.Vue2中注册在router-link上事件无效解决方法**
答： 使用[@click](https://github.com/click).native。原因：router-link会阻止click事件，.native指直接监听一个原生事件。
**34.RouterLink在IE和Firefox中不起作用（路由不跳转）的问题**
答: 方法一：只用a标签，不适用button标签；方法二：使用button标签和Router.navigate方法
**35.axios的特点有哪些**
答：从浏览器中创建XMLHttpRequests；
node.js创建http请求；
支持Promise API；
拦截请求和响应；
转换请求数据和响应数据；
取消请求；
自动换成json。
axios中的发送字段的参数是data跟params两个，两者的区别在于params是跟请求地址一起发送的，data的作为一个请求体进行发送
params一般适用于get请求，data一般适用于post put 请求。
**36.请说下封装 vue 组件的过程？**
答：1. 建立组件的模板，先把架子搭起来，写写样式，考虑好组件的基本逻辑。(os：思考1小时，码码10分钟，程序猿的准则。)

　　2. 准备好组件的数据输入。即分析好逻辑，定好 props 里面的数据、类型。
　　3. 准备好组件的数据输出。即根据组件逻辑，做好要暴露出来的方法。
　　4. 封装完毕了，直接调用即可

37.params和query的区别**
答：用法：query要用path来引入，params要用name来引入，接收参数都是类似的，分别是this.$route.query.name和this.$route.params.name。
url地址显示：query更加类似于我们ajax中get传参，params则类似于post，说的再简单一点，前者在浏览器地址栏中显示参数，后者则不显示
注意点：query刷新不会丢失query里面的数据
params刷新 会 丢失 params里面的数据。** 38.vue初始化页面闪动问题**
答：使用vue开发时，在vue初始化之前，由于div是不归vue管的，所以我们写的代码在还没有解析的情况下会容易出现花屏现象，看到类似于{{message}}的字样，虽然一般情况下这个时间很短暂，但是我们还是有必要让解决这个问题的。
首先：在css里加上[v-cloak] {
display: none;
}。
如果没有彻底解决问题，则在根元素加上style=”display: none;” :style=”{display: ‘block’}”** 39.vue更新数组时触发视图更新的方法**
答:push()；pop()；shift()；unshift()；splice()； sort()；reverse()** 40.vue常用的UI组件库**
答：Mint UI，element，VUX** 41.vue修改打包后静态资源路径的修改**
cli3版本：在根目录下新建vue.config.js 文件，然后加上以下内容：（如果已经有此文件就直接修改）
module.exports = {
publicPath: ‘’, // 相对于 HTML 页面（目录相同） }
生命周期函数面试题** 1.什么是 vue 生命周期？有什么作用？**
答：每个 Vue 实例在被创建时都要经过一系列的初始化过程——例如，需要设置数据监听、编译模板、将实例挂载到 DOM 并在数据变化时更新 DOM 等。同时在这个过程中也会运行一些叫做 生命周期钩子 的函数，这给了用户在不同阶段添加自己的代码的机会。（ps：生命周期钩子就是生命周期函数）例如，如果要通过某些插件操作DOM节点，如想在页面渲染完后弹出广告窗， 那我们最早可在mounted 中进行。** 2.第一次页面加载会触发哪几个钩子？**
答：beforeCreate， created， beforeMount， mounted** 3.简述每个周期具体适合哪些场景**
答：beforeCreate：在new一个vue实例后，只有一些默认的生命周期钩子和默认事件，其他的东西都还没创建。在beforeCreate生命周期执行的时候，data和methods中的数据都还没有初始化。不能在这个阶段使用data中的数据和methods中的方法
create：data 和 methods都已经被初始化好了，如果要调用 methods 中的方法，或者操作 data 中的数据，最早可以在这个阶段中操作
beforeMount：执行到这个钩子的时候，在内存中已经编译好了模板了，但是还没有挂载到页面中，此时，页面还是旧的
mounted：执行到这个钩子的时候，就表示Vue实例已经初始化完成了。此时组件脱离了创建阶段，进入到了运行阶段。 如果我们想要通过插件操作页面上的DOM节点，最早可以在和这个阶段中进行
beforeUpdate： 当执行这个钩子时，页面中的显示的数据还是旧的，data中的数据是更新后的， 页面还没有和最新的数据保持同步
updated：页面显示的数据和data中的数据已经保持同步了，都是最新的
beforeDestory：Vue实例从运行阶段进入到了销毁阶段，这个时候上所有的 data 和 methods ， 指令， 过滤器 ……都是处于可用状态。还没有真正被销毁
destroyed： 这个时候上所有的 data 和 methods ， 指令， 过滤器 ……都是处于不可用状态。组件已经被销毁了。** 4.created和mounted的区别**
答：created:在模板渲染成html前调用，即通常初始化某些属性值，然后再渲染成视图。
mounted:在模板渲染成html后调用，通常是初始化页面完成后，再对html的dom节点进行一些需要的操作。** 5.vue获取数据在哪个周期函数**
答：一般 created/beforeMount/mounted 皆可.
比如如果你要操作 DOM , 那肯定 mounted 时候才能操作.** 6.请详细说下你对vue生命周期的理解？**
答：总共分为8个阶段创建前/后，载入前/后，更新前/后，销毁前/后。
创建前/后： 在beforeCreated阶段，vue实例的挂载元素$el和**数据对象**data都为undefined，还未初始化。在created阶段，vue实例的数据对象data有了，$el还没有。
载入前/后：在beforeMount阶段，vue实例的$el和data都初始化了，但还是挂载之前为虚拟的dom节点，data.message还未替换。在mounted阶段，vue实例挂载完成，data.message成功渲染。
更新前/后：当data变化时，会触发beforeUpdate和updated方法。
销毁前/后：在执行destroy方法后，对data的改变不会再触发周期函数，说明此时vue实例已经解除了事件监听以及和dom的绑定，但是dom结构依然存在。
vue路由面试题** 1.mvvm 框架是什么？**
答：vue是实现了双向数据绑定的mvvm框架，当视图改变更新模型层，当模型层改变更新视图层。在vue中，使用了双向绑定技术，就是View的变化能实时让Model发生变化，而Model的变化也能实时更新到View。** 2.vue-router 是什么?它有哪些组件**
答：vue用来写路由一个插件。router-link、router-view** 3.active-class 是哪个组件的属性？**
答：vue-router模块的router-link组件。children数组来定义子路由** 4.怎么定义 vue-router 的动态路由? 怎么获取传过来的值？**
答：在router目录下的index.js文件中，对path属性加上/:id。 使用router对象的params.id。** 5.vue-router 有哪几种导航钩子?**
答：三种，
第一种：是全局导航钩子：router.beforeEach(to,from,next)，作用：跳转前进行判断拦截。
第二种：组件内的钩子
第三种：单独路由独享组件** 6.$route 和 $router 的区别**
答：$router是VueRouter的实例，在script标签中想要导航到不同的URL,使用$router.push方法。返回上一个历史history用$router.to(-1)
$route为当前router跳转对象。里面可以获取当前路由的name,path,query,parmas等。** 7.vue-router的两种模式**
答:hash模式：即地址栏 URL 中的 # 符号；
history模式：window.history对象打印出来可以看到里边提供的方法和记录长度。利用了 HTML5 History Interface 中新增的 pushState() 和 replaceState() 方法。（需要特定浏览器支持）。** 8.vue-router实现路由懒加载（ 动态加载路由 ）**
答:三种方式
第一种：vue异步组件技术 ==== 异步加载，vue-router配置路由 , 使用vue的异步组件技术 , 可以实现按需加载 .但是,这种情况下一个组件生成一个js文件。
第二种：路由懒加载(使用import)。
第三种：webpack提供的require.ensure()，vue-router配置路由，使用webpack的require.ensure技术，也可以实现按需加载。这种情况下，多个路由指定相同的chunkName，会合并打包成一个js文件。
vuex常见面试题** 1.vuex是什么？怎么使用？哪种功能场景使用它？**
答：vue框架中状态管理。在main.js引入store，注入。
新建了一个目录store.js，….. export 。
场景有：单页应用中，组件之间的状态。音乐播放、登录状态、加入购物车** 2.vuex有哪几种属性？**
答：有五种，分别是 State、 Getter、Mutation 、Action、 Module
state => 基本数据(数据源存放地)
getters => 从基本数据派生出来的数据
mutations => 提交更改数据的方法，同步！
actions => 像一个装饰器，包裹mutations，使之可以异步。
modules => 模块化Vuex** 3.Vue.js中ajax请求代码应该写在组件的methods中还是vuex的actions中？**
答：如果请求来的数据是不是要被其他组件公用，仅仅在请求的组件内使用，就不需要放入vuex 的state里。

# 常见方式自己实现

```javascript
1. 实现new
2. 实现call
```

# 面试方法

```javascript
Boss直聘
```

# 常见算法

```javascript
1. 公共字符串
2. 不重复字串
```

# 各种工具

```javascript
Electron 桌面应用程序center
flutter app开发工具
```

# 项目总结

## 前端

### 1. components使用

```javascript
//组件导出
export default {
    name:'slider', //导出组件
}
//组件导入
import slider from '@/components/slider'
export default {
    components: {
        slider
    },    
}
```

### 2 layouts使用

```javascript
export default {
    layout: 'mainly',
}
```

### 3 vuex Ajax文件使用

```javascript

```



## 后端

### 1---创建端口

```javascript
const express = require('express');
const app = express();
const path = require('path')

app.use('/',express.static(path.join(__dirname)));

app.listen(3000);
```

### 2---连接数据库

```javascript
const mysql:any = require('mysql');
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'ctdms'
});

let db:any = {
  query: function (sql:string, callback:any) {
    pool.getConnection(function (err:any, connection:any) {
        if (err) {
            return callback(err);
        }
        //Use the connection
        connection.query(sql, function (err:any, results:any, fields:any) {
            if (err) {
                return callback(err);
            }
            //只是释放链接，在缓冲池了，没有被销毁
            connection.release();
            //每次查询都会 回调
            callback(err, results);
        });

    });
  }
};

export default db;

```





