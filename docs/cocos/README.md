```ts
import { _decorator, Component, Node, find, v2,Sprite } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Test')
export class Test extends Component {
    start() {
        // 获得子节点
        this.node.children[0];
        this.node.getChildByName("abc");
        find("Canvas/Main Camera")
        // 获得父节点
        this.node.getParent();
        this.node.setParent(ddd);
        // 移除所有子节点
        this.node.removeAllChildren();
        // 移除某个子节点
        this.node.removeChild(ddd);
        // 从父节点中移除
        this.node.removeFromParent();
        // 访问位置
        this.node.x
        this.node.y
        this.node.setPosition(3,3);
        this.node.setPosition(v2(3,4));
        this.node.rotation;

        // 节点开关
        this.node.active = false;
        // 组件开关
        this.enabled = false;

        // 获取组件
        this.getComponent(Sprite);
        this.getComponentInChildren(Sprite);

        resources.load("test/land",SpriteFrame, (err, sp)=>{
            console.log(err);
            console.log(sp);
        })
        // 加载场景
        director.loadScene("game2",()=>{
            // 已经切换
        })
        // 预加载
        director.preloadScene("game2",()=>{
            // 加载未切换
            director.loadScene("game2");
        })

        // 永久存在节点
        game.addPersistRootNode(this.node);
        game.removePersistRootNode(this.node);

        this.node.on(NodeEventType.MOUSE_DOWN,(event)=>{});
        
        systemEvent.on()


        this.node.on(Node.EventType.TOUCH_START,(event)=>{
            console.debug(event.getID());
        });
        this.node.emit('test');
        // 是否冒泡
        this.node.dispatchEvent(new Vevnt.EventCustom("myevent1", false));

    }

    update(deltaTime: number) {
        
    }
}

```