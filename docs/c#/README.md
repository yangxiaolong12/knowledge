# C# 学习

## 打印
```C#
short a;
int b ;
double c;

/* 实际初始化 */
a = 10;
b = 20;
c = a + b;
Console.WriteLine("a = {0}, b = {1}, c = {2}", a, b, c);
```

## 数据类型

### 值类型
```
类型	描述	范围	默认值
bool	布尔值	True 或 False	False
byte	8 位无符号整数	0 到 255	0
char	16 位 Unicode 字符	U +0000 到 U +ffff	'\0'
decimal	128 位精确的十进制值，28-29 有效位数	(-7.9 x 1028 到 7.9 x 1028) / 100 到 28	0.0M
double	64 位双精度浮点型	(+/-)5.0 x 10-324 到 (+/-)1.7 x 10308	0.0D
float	32 位单精度浮点型	-3.4 x 1038 到 + 3.4 x 1038	0.0F
int 	32 位有符号整数类型	-2,147,483,648 到 2,147,483,647	0
long	64 位有符号整数类型	-9,223,372,036,854,775,808 到 9,223,372,036,854,775,807	0L
sbyte	8 位有符号整数类型	-128 到 127	0
short	16 位有符号整数类型	-32,768 到 32,767	0
uint	32 位无符号整数类型	0 到 4,294,967,295	0
ulong	64 位无符号整数类型	0 到 18,446,744,073,709,551,615	0
ushort	16 位无符号整数类型	0 到 65,535	0
```
### 引用类型

#### 字符串类型

string 字符串的前面可以加 @（称作"逐字字符串"）将转义字符（\）当作普通字符对待，比如：string str = @"C:\Windows";


## 类型转换

### 示例

```c#

    double d = 5673.74;
    int i;

    // 强制转换 double 为 int
    i = (int)d;
    Console.WriteLine(i);
    Console.ReadKey();

    5673
```

### 相关方法
    1	ToBoolean
    如果可能的话，把类型转换为布尔型。
    2	ToByte
    把类型转换为字节类型。
    3	ToChar
    如果可能的话，把类型转换为单个 Unicode 字符类型。
    4	ToDateTime
    把类型（整数或字符串类型）转换为 日期-时间 结构。
    5	ToDecimal
    把浮点型或整数类型转换为十进制类型。
    6	ToDouble
    把类型转换为双精度浮点型。
    7	ToInt16
    把类型转换为 16 位整数类型。
    8	ToInt32
    把类型转换为 32 位整数类型。
    9	ToInt64
    把类型转换为 64 位整数类型。
    10	ToSbyte
    把类型转换为有符号字节类型。
    11	ToSingle
    把类型转换为小浮点数类型。
    12	ToString
    把类型转换为字符串类型。
    13	ToType
    把类型转换为指定类型。
    14	ToUInt16
    把类型转换为 16 位无符号整数类型。
    15	ToUInt32
    把类型转换为 32 位无符号整数类型。
    16	ToUInt64
    把类型转换为 64 位无符号整数类型。

```c#

    int i = 75;
    float f = 53.005f;
    double d = 2345.7652;
    bool b = true;

    Console.WriteLine(i.ToString());
    Console.WriteLine(f.ToString());
    Console.WriteLine(d.ToString());
    Console.WriteLine(b.ToString());

    75
    53.005
    2345.7652
    True
```

## 进制
```c#
85         /* 十进制 */
0213       /* 八进制 */
0x4b       /* 十六进制 */

```