# 图形学
## 教学课程
 [闫令奇](https://www.bilibili.com/video/BV1X7411F744?from=search&seid=18307990048383830004&spm_id_from=333.337.0.0)
## 概念
    计算机视觉：图片转模型
    计算机图形学：模型转图片（人的视觉）
## 组成
    （1）光栅化成像，（2）几何表示，（3）光的传播理论，以及（4）动画与模拟。

### 线性代数
#### 矩阵
    没有交换律
##### 矩阵和图像的关系
    1. 矩阵可以让图形进行的缩放与反转
    [[x1],[y1]]=[[0.5,0][0,0.5]][[x],[y]] // 缩放
    [[x1],[y1]]=[[-1,0][0,1]][[x],[y]] // 翻转
    [[x1],[y1]]=[[1,a][0,1]][[x],[y]] // 剪切矩阵
#### 向量
##### 特性
    方向：
    长度：
##### 单位向量
    长度为1的是单位向量
    向量/向量长度=单位向量
##### 向量运算
    向量求和：首位相接向量的首尾就是
![avatar](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/public/assets/img/Snipaste_2021-11-08_16-40-35.png)

    向量乘积（点乘）: 和角度有关，得到数越大，向量越相近
![](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/public/assets/img/Snipaste_2021-11-08_16-47-25.png)
    
    和坐标相关
    ：向量A乘向量B = xa*xb+ya*yb （根据得到的数，判断量向量的角度）
![](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/public/assets/img/Snipaste_2021-11-08_17-05-02.png)

![](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/public/assets/img/Snipaste_2021-11-08_17-05-55.png)

    叉乘，对向量做变换
    ：axb=-bxa 得到后的向量，垂直与a与b的平面内（不满足交换律） 
    axkb=k(axb)

##### 笛卡尔坐标系
    向量从坐标系得原点出发, 得出A=Bx+Cy公司
![](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/public/assets/img/Snipaste_2021-11-08_16-45-12.png)

##### 投影
    b在a上面的投影 = ||b->||*cos@ 的长度加上a->的方向

### 光栅化
#### 概念
    把三维空间的几何形体显示到屏幕上
#### 实时光线追踪

## 骨骼动画
### 组成
    动画数据，骨骼数据，包含Skin info的Mesh数据，以及Bone Offset Matrix
### 基本概念
    1. 骨骼：一个坐标空间。
    2. 骨骼层次：嵌套的坐标空间。
    3. 关节：描述骨骼的位置，即骨骼“自己的坐标系空间原点”在其“父空间”的位置（子骨骼位于父骨骼的坐标系）。
    4. 绕关节旋转：骨骼坐标空间自身的旋转（包括旋转子空间）。
    5. 4X4矩阵: 用于移动、旋转、缩放
    6. 整个模型是一个大的坐标系，每一块骨骼，就是坐标系上的一个向量，4x4的矩阵，会改变这个向量的指向，控制这个向量的旋转, 平移，缩放
### 功能
    1. 骨骼的点，会对mesh上的点有一个权重，骨骼的点移动时，会牵动着mesh上的点
### 知识点
    1. 蒙皮（skinned mesh）: 将MESH中得顶点附着在骨骼上。
    2. 整体有一个大的空间坐标系，骨骼是其中的一块向量 
### 案例
    1. Morph动画



## 章节三：光栅化、采样、走样、反走样、频域、时域、MSAA

### 光栅化

光栅化就是，在前面通过透视投影转正交投影，最后获取的2d图像后，将2d图像转化为一个个栅格组成的图像。

栅格：例如电视上一小块一小块的像素点

光栅化也分为光栅化线和光栅化三角形，下面主要讲光栅化三角形。

光栅化选择三角形的原因：

	1. 三角形是最基础的平面图形
	2. 三角形可以组成任何图形

我们可以通过采样，来得到屏幕上应该如何显示图像。

### 采样

采样就是指把时间域或空间域的连续量转化成离散量的过程，如下图，将整个三角形面，只丢下在三角形内部的点的像素，在这里我们通过对每一个像素进行判断，如果它的中心点在三角形内部，那么它就是属于三角形的，我们应该在屏幕上将其画出来，而判断方法之前也说过，可以通过叉乘来实现，使用右手法则。

<img src="https://img-blog.csdnimg.cn/20200427212515261.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />



### 走样

上图采样会生成下图：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200427232819530.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70#pic_center)

可以看到，采样后显示到屏幕上的三角形，有很严重的锯齿，这种现象就成为走样。

解决走样的方法：

1. 提高分辨率，增加采样点。
2. 使用滤波，模糊化图像，具体操作流程如下：

### 反走样

反走样的思想就是在信号采样前，对齐进行滤波，即将图像模糊化，结果会好很多，如下图：

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020042723474531.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70)

下面将通过频域和时域解释现象产生的原理。

### 频域

我们的世界都是以时间的概念在流逝的，像一首歌一个视频，它是处于时域下的，而频域就是描述信号在频率时用到的一种坐标系，它是另外一种坐标系。而我们常见的正弦波(假定sin 和 cos 都称为正弦波)也是有频率的，如下图：

<img src="https://img-blog.csdnimg.cn/20200428110016236.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

### 傅里叶变换

傅里叶变换是说一个函数能够由多个不同频率的正弦和余弦构成。而这里的正弦波，我们把他看成是不同频率的分量，如下图：

<img src="https://img-blog.csdnimg.cn/20200428110316511.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom: 80%;" />

![image-20211117223849027](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117223849027.png)

所以实质上如果想要把一个函数从时域变到频域就可以通过傅里叶变换，而通过其逆变换就可以把这个函数从频域变换到时域，如下图：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200429220807912.png)

### 采样与滤波

走样：假设我们以一定的频率对下面的函数进行采样，会发现当函数自身频率越高时，由于采样频率不够，恢复的函数与原来的函数也差异过大。

<img src="https://img-blog.csdnimg.cn/20200429221845779.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom: 67%;" />

**滤波**：就是能够过滤掉图像（函数）中的某些频率部分。如高通滤波，低通滤波。卷积其实也是滤波的一种形式，它是对信号周围进行加权平均的一种运算。而卷积定理在频域与时域上也十分重要，即：

![image-20211117224829189](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117224829189.png)

![image-20211117224843848](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117224843848.png)

![image-20211117224858072](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117224858072.png)

平均、模糊、滤波 -》 卷积：

![image-20211117225317235](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117225317235.png)

**在函数在时域上的卷积等于其在频域上的乘积，反之亦然**，如下图：

<img src="https://img-blog.csdnimg.cn/20200429222514241.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:80%;" />

模糊化，高频降低，低频增多

### 频域角度看采样

![image-20211117230036533](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117230036533.png)

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117230420872.png" alt="image-20211117230420872" style="zoom: 67%;" />

采样越小，频域冲击函数上的间隔越大，乘积得到的函数图像，间隔越小

因此，如果可以将重叠的部分去掉，如下图

<img src="https://img-blog.csdnimg.cn/20200429224030159.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:67%;" />

即进行滤波，去掉一些频谱，在进行光栅化，结果会更好一些(低频滤波器)

### MSAA

##### 超采样

超采样就是通过对一个像素内的多个位置进行采样并取其平均值来近似1-pixel 滤波器(卷积,blur)的效果
对于4X MSAA来说，其步骤如下：

	1. 假设每个像素中对四个点进行采样
	2. 判断对于一个像素有多少个点在三角形内，然后根据比例对颜色进行"模糊"
	3. <img src="https://img-blog.csdnimg.cn/20200429224647643.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:67%;" />

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117231120556.png" alt="image-20211117231120556" style="zoom:67%;" />

### 其他反走样策略

FXAA ，TAA，DLSS等

## 章节四：可见度、闭合 （深度缓存）光照、着色、图形管线、纹理映射

上一篇文章说了如何光栅化三角形，当在屏幕上画好了三角形后，我们要确定物体与物体之间的遮挡关系，而这种方法通常使用的是Z-buffering（深度缓冲）

### 问题

解决，映射到画面上，物体的深度顺序。

### 画家算法

模式：如同把画的物体按深度大小的顺序进行排序，然后有远到近的顺序依次画物体。

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116074724402.png" alt="image-20211116074724402" style="zoom:50%;" />

缺点：无法解决深度顺序问题，因为是按照绘制多边形图形的深度进行排序，如下图，

三个多边形分别有部分在其他多边形上面

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116074449847.png" alt="image-20211116074449847" style="zoom:50%;" />

### Z-buffering深度缓存算法

#### 更好的算法

可以解决上图问题，因为它是按像素的深度大小进行排序，脱离了一整个多边形进行深度排序。

#### 主要思想

为每一个像素 存储 当前的 最小深度值（这里课程指的是深度越小，离视点越近，如果z轴的远近定义不一样，则会存储最大深度值），当扫描到新的像素，如果它的深度更小，则用它对应或插值的颜色值来替代当前需要显示的颜色值，流程图如下：

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116075424510.png" alt="image-20211116075424510" style="zoom:50%;" />

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116081328738.png" alt="image-20211116081328738" style="zoom:50%;" />

上图循环含义：循环每一个多边形，判断多边形上每一个点，他的z的大小是否比缓存区存储的更小，即离相机更近，则将其存入缓存区中。

从上面也可以看出，Z-buffering同时也需要**帧缓冲**和**深度缓冲**来分别存储像素的**颜色值**和**深度值**。这里提到的帧缓冲，可以把它理解成一种可以自己定义的缓冲（一块内存区域），即通过定义可以存储颜色，深 度，或者颜色与深度，颜色与纹理的不同组合的缓冲区。

### 着色（shading）

shading的字面意思就是通过对物体进行上色

对于这个课程来说：shading的定义是对物体应用材质的过程。而材质可以理解成物体自身对 光线 的各种反应情况，它反应了物体本身的物理属性

#### Blinn-Phong 光照模型

首先说一下计算光照的一些简单规则：
		①**物体颜色**：我们在现实生活中看到某一物体的颜色并不是这个物体真正拥有的颜色，而是它所反射的颜色。如果想知道物体的颜色，只需要用它的颜色和光的颜色进行逐元素乘即可:

```c++
vec3 lightColor(0.0f, 1.0f, 0.0f);
vec3 objectColor(1.0f, 0.5f, 0.31f);
vec3 lightResult = lightColor * objectColor; // = (0.0f, 0.5f, 0.0f);
```

比如上述例子，光照颜色是绿色，通过用物体颜色和光照颜色进行逐元素乘，发现最后物体吸收了一半的绿色，并且以另一半为结果反射给人眼，所以我们看到的结果应该是暗绿色的物体

②**利用点乘计算光照影响**：漫反射表示光会朝不同的方向进行反射，为了计算光对物体表面的影响，我们只需要光和物体表面法向量进行点乘即可，想想看一束光如果直接垂直打在物体上，其影响是最大的：

**光的衰减**：
光照强度会随着距离衰减，注意衰减范围是以光源为中心的圆展开的

I为光波，r为半径，图中方程式为光波I随着传播半径r越远，光强越小

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200507144939385.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70)



##### 环境光、高光以及漫反射

物体的颜色由环境光、高光以及漫反射，三种光共同决定

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200507143044375.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70)

##### 漫反射

![image-20211116082605381](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116082605381.png)

方程式中，n为平面的法线，v为视角，I为光照，Ld为反射到人眼上展示的物体的颜色，kd为材质本身颜色等因素

##### 高光

![image-20211116234004370](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116234004370.png)

![image-20211116082639381](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116082639381.png)

第一张图中的R是镜面反射，所谓的高光就是光的镜面反射产生的，当相机视角与镜像反射光R相近时，则能观察到高光。

方程中的n为法线，h为向量I+向量v得到的半程向量，两向量夹角与R和v直接的夹角相同，且n与h更好的得到，因此方程中使用n和h，将n和h换成R和v也可以

方程中的p主要是为了减少高光的范围，因为一般高光也就一小块，如下图，p越大，高光可展示的范围越小，一般p为100。高光可展示的范围越小，即为半程向量与法线向量的夹角的就得越小。

![image-20211116233438538](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116233438538.png)

![image-20211116233632349](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116233632349.png)

##### 环境光

![image-20211116233251210](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116233251210.png)

假设，认为某个点接收的环境光都是相同的（假设，不然真实情况很复杂）

##### 三光共同作用

![image-20211116233310942](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116233310942.png)

#### Blinn-Phong模型

和shading frequencies（着色频率）相关

着色频率的不同，会有不同的效果，如下：

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211116235324029.png" alt="image-20211116235324029" style="zoom:50%;" />

上图：球体为多个面构成，分别为对三角形进行着色，对顶点进行着色，对像素进行着色三种着色方案

##### 对三角形进行着色

对每个三角形着色也称为Flat shading ，它是指每项属性例如纹理，法向，颜色，都是属于一个三角形的，所以三角形内部不会有插值变化。所以，三角形内部只会根据面法向量对光线反射进行计算，所有的三角形反射都看做一个平面，给人以一种块状的效果（三角形较少时），如下图：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200509002929261.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70)

##### 对每个顶点着色

对顶点进行着色也称为**gouraud** shading，它发生在顶点着色器阶段。对于每个多边形的顶点都存在一个法向量，但是他的着色是先通过这些法向量对顶点计算出光照颜色，然后用光照颜色来进行三角形内部插值，如下图：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200509004740123.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70)

##### 对每个像素着色

对每个像素着色也称**phong** shading，它发生在片段着色器阶段。它是通过对多边形每个顶点的法向量进行插值，然后我们通过插值得到的法向量再去计算光照颜色，如下图：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200509004755573.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70)

由于片段着色器阶段计算的坐标都是处于世界坐标系中，所以在计算光照时，所用的法向量也要转换到世界坐标系下。还记得之前我们把坐标转换到世界坐标是乘以model矩阵，但是法向量是不可以直接乘以model矩阵的，因为它仅仅是一个向量，而且也不是齐次坐标表示，所以这里需要一个法线矩阵（它是model矩阵逆的转置）来进行转换，使法向量也处于世界坐标下。

使用不同的着色方案会得到不同的结果，但是也不能说某种方案就一定差于另一种方案，当顶点数足够多时，对三角形进行着色不一定会比对像素进行着色效果差，对应的内存开销也不一定比对像素进行着色小

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117234432927.png" alt="image-20211117234432927" style="zoom:50%;" />

如何求顶点法向量：顶点法向量，就是相邻面的法线向量求加权平均，权重，跟面的面积有关。

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117234515205.png" alt="image-20211117234515205" style="zoom:50%;" />

如何求像素法向量：

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117234637072.png" alt="image-20211117234637072" style="zoom:50%;" />

使用重心坐标，之后会降到。

#### 图形管线(实时渲染管线)

如何从场景到最后一张图，中间的过程。

##### 渲染管线流程

通过图形渲染管线都是流水线形式，上一阶段的输出作为下一阶段的输入，该课程的一个图形渲染管线（也是前向渲染管线）流程如下：

1. 输入空间中的点
2. 投影到屏幕上
3. 点会形成三角形
4. 屏幕是离散的，所以要光栅化，离散三角形
5. 着色
6. 产出

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200509005047419.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70)

​	每个顶点做模型、视图、投影变换

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200509010152509.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70)

光栅化

<img src="https://img-blog.csdnimg.cn/20200509120013783.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

对每个像素点进行操作

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118000957406.png" alt="image-20211118000957406" style="zoom:50%;" />



根据需求，对顶点着色、或像素着色

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118000944694.png" alt="image-20211118000944694" style="zoom:50%;" />

三角形的点都对应一个纹理上的点

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118001238873.png" alt="image-20211118001238873" style="zoom:50%;" />

##### 渲染管线的编程

通常可以在Vertex Processing阶段（顶点着色器） 和 Fragment Processing阶段（片段着色器） 进行编程，这些编程的程序被称为Shader Programs，里面常用的语言有glsl等

下面的示例程序简单描述了一个着色器的工作，uniform表示从cpu传来的变量，varying 表示从上一个阶段传来的变量，void diffuseShader() 为着色函数， texture2d是内置函数，它表示将纹理myTexture 对应到uv向量，gl_FragColor 可以看做是内置参数，它是屏幕上用来显示的最终颜色

```c++
uniform sampler2D myTexture; // program parameter 
uniform vec3 lightDir;   // program parameter 光照方向
varying vec2 uv;    // per fragment value (interp. by rasterizer)
varying vec3 norm;   // per fragment value (interp. by rasterizer)  法线
void diffuseShader() 
{   
	vec3 kd;   
	kd = texture2d(myTexture, uv);    // material color from texture  
	kd *= clamp(dot(–lightDir, norm), 0.0, 1.0);  // Lambertian shading model   着色
	gl_FragColor = vec4(kd, 1.0);    // output fragment color 
}
```

这里有一个链接可以用于学习如何编写shader: https://www.shadertoy.com/.

#### 纹理映射

纹理映射，赋予像素点不同的属性

任何一个三维物体的表面是二维的

纹理映射就是将纹理空间中的纹理像素映射到屏幕空间中的像素的过程。通俗来说可以认为是一张二维纹理把一个三维物体“包裹”了起来，因此三维物体获得了一些表面纹理：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200509122955508.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70)

纹理也是有坐标的，它的坐标空间是由uv构成的，里面对应的元素是纹素，是计算机图形纹理空间中的基本单元，如下图：



<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118002948242.png" alt="image-20211118002948242" style="zoom: 67%;" />

<img src="https://img-blog.csdnimg.cn/20200509123359414.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:67%;" />

uv坐标，规定范围，v在0到1，u在0到1

纹理可以复用，所以最好设计成可以衔接的。

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118003348902.png" alt="image-20211118003348902" style="zoom:67%;" />

实质上纹理映射就是把uv坐标下的纹素映射到图像像素的过程。而我们在画三角形的过程中通常只是定义了三个顶点的属性，其内部属性往往需要通过三个顶点插值得来，像法向量，颜色值，深度值，还有纹理坐标等。所以我们需要一个插值的方式来解决这个问题，常用的有利用重心坐标进行插值

##### 重心坐标

为了做三角形内的插值，包括像素着色如何实现的等。

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118004413065.png" alt="image-20211118004413065" style="zoom:150%;" />

三角形内，三个数均大于0

<img src="https://img-blog.csdnimg.cn/20200512212030468.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

A、B、C点上对应的重心坐标

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118010255514.png" alt="image-20211118010255514" style="zoom:50%;" />

Aa Ab Ac 为面积：重心坐标的公式如下

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118010407647.png" alt="image-20211118010407647" style="zoom:50%;" />

三角形的重心点得到的三个面积相等：可以得出三角形重心坐标如下：

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118010623063.png" alt="image-20211118010623063" style="zoom:50%;" />

重心坐标公式：

<img src="https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118010825987.png" alt="image-20211118010825987" style="zoom:50%;" />



##### 重心坐标作用

##### 纹理映射过程

简单的纹理映射的过程：在光栅化过程中，对当前扫描到的点的uv坐标进行采样，得到的颜色直接赋给物体

```c++
for each rasterized screen sample (x,y):    //光栅化图像上的像素点
	(u,v) = evaluate texture coordinate at (x,y)    // 通过使用重心坐标等，获取对应的uv点
	texcolor = texture.sample(u,v);    // 获取u v点的颜色
	set sample’s color to texcolor
```

###### 纹理分辨率过小

但是这样会带来一些问题，就是当纹理分辨率过小，而需要用来覆盖的物体过大时，多个像素坐标(x，y)都会采样到同一个纹素，产生一些锯齿的效果，这时我们就有一些解决方法，如：最近邻插值，双线性插值，双三次插值，如下图：

<img src="https://img-blog.csdnimg.cn/20200512214711318.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

###### 最近邻插值

假设红点是像素坐标点对应的纹理像素坐标，那么最近邻插值就是把离它最近的纹素分配给当前像素点：

<img src="https://img-blog.csdnimg.cn/20200512215919707.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

上图的结果就是红点的纹素应该是它右上角黑点纹素对应的值

###### 双线性插值

双线性插值就是找它周围四个点对应的纹素进行插值，如下图：

<img src="https://img-blog.csdnimg.cn/20200512215937777.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

那么如何进行插值？

<img src="https://img-blog.csdnimg.cn/20200512220750178.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom: 67%;" />

![image-20211117000421273](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211117000421273.png)

<img src="https://img-blog.csdnimg.cn/20200512221059455.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />



###### 双三次插值

这里的双三次不同于三维空间中的三线性插值，应该叫做双三次插值（Bicubic），它是对二维空间的插值，考虑了邻近十六个采样点的插值，具体方法同双线性插值

###### 纹理分辨率过大

同样，当纹理分辨率过大时，如果以某种角度或者由于物体过远，对纹理进行相同的采样率得到的结果却会远远不同（往往会很差）。通俗来说就是如果我们由近到远看一个物体，同时它的纹理分辨率十分高。当它离我们比较近时，它在我们人眼中的占比非常大，我们需要对纹理有很高的采样率才能把它的很多细节看得清楚。但是当它离我们很远时，假如在肉眼中只有非常小的比例，我们就不需要看到它的很多细节，但是这时我们还是用之前的采样率去对它纹理进行采样，得到的结果就会“乱掉”（摩尔纹），可以看下图：

<img src="https://img-blog.csdnimg.cn/20200512222341154.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

实际上一个像素，所覆盖的纹理区域也是不同的，所以当发生这种变化或者一个像素覆盖纹理区域过大时，我们也不应该用同样的采样率对纹理进行采样，因为它：

<img src="https://img-blog.csdnimg.cn/2020051222271075.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

解决办法

1. **超采样**

2. 像素覆盖纹理的平均值

可以用mipmap去解决，但是在看mipmap之前需要先了解一下范围查询

###### 范围查询

 	范围查询：不知道哪个点，但是给定了你一个区域，可以直接得到对应的值（平均值之类的），而mipmap就是基于范围查询的

###### Mipmap

它是一种快速，近似，仅限于正方形范围的范围查询

Mipmap就是通过一张纹理，生成许多高层（分辨率每次缩小到一半）的纹理，如下图：

<img src="https://img-blog.csdnimg.cn/20200512223806345.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

![image-20211118021505433](https://gitlab.com/yangxiaolong12/knowledge/-/raw/main/docs/graphics/README.assets/image-20211118021505433.png)

<img src="https://img-blog.csdnimg.cn/2020051222520115.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

当我们得到了层级后，在这一层上进行纹理插值即可。但是我们发现，层级之间是离散的，也就是说，一些像素在0层插值，一些像素在1层插值，这种变化也会使得纹理“割裂”：

<img src="https://img-blog.csdnimg.cn/20200512230222759.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

解决方法就是，每次插值，现在D层插值纹理（可以用双线性之类的方法），再在D+1层插值纹理，把两层结果再进行插值（把log的结果去和前一层和后一层进行插值比较），这样的结果就是平滑的了：

<img src="https://img-blog.csdnimg.cn/20200512230429239.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />

但是Mipmap也会有缺点，因为它的范围只是限定在正方形范围内的，所以会产生over-blur的问题：

<img src="https://img-blog.csdnimg.cn/20200512230623300.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />



###### 各向异性滤波

各向异性滤波能够部分解决这种问题，因为它的范围不是限定在正方形范围内的，而是矩形范围。如果说下图中Mipmap的图片缩小是沿着对角线进行的，而各向异性滤波的压缩就是沿着水平和竖直线进行的：



![在这里插入图片描述](https://img-blog.csdnimg.cn/20200512231024212.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70)

所以它是使得原图的像素可以查询纹理中一些矩形区域，这样就能部分解决一些形变压缩的纹理查询问题：<img src="https://img-blog.csdnimg.cn/20200512231133787.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2MjQyMzEy,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述" style="zoom:50%;" />


```
    //设置相机缩放比数值越大缩放越明显
    let factor = 100;
    //从鼠标位置转化为webgl屏幕坐标位置
    let glScreenX = (event.clientX / scope.domElement.width) * 2 - 1;
    let glScreenY = -(event.clientY / scope.domElement.height) * 2 + 1;
    let vector = new THREE.Vector3(glScreenX, glScreenY, 0);
    //从屏幕向量转为3d空间向量
    vector.unproject(scope.object);
    //相机偏移量
    vector.sub(scope.object.position).setLength(factor);
    if (event.deltaY < 0) {
      if (scope.target.z < -800) return;
      scope.object.position.add(vector);
      scope.target.add(vector);
    } else {
      scope.object.position.sub(vector);
      scope.target.sub(vector);
    }
    scope.update();
```
