# 贝塞尔曲线
## 三次贝塞尔公式
[image](https://bkimg.cdn.bcebos.com/formula/0f72d0377d19fdbb9ec87ea0146a9175.svg)

## 贝塞尔曲面
### 概念
```
    https://zhuanlan.zhihu.com/p/144399638
    贝塞尔曲面，由八条贝塞尔曲线构成，八条贝塞尔曲线的变化，会影响范围内面上点的变化。

```
### live2d贝塞尔曲面的实现
```
    https://zhuanlan.zhihu.com/p/144399638

    V方向
    点----点----点----点
    |  B  |     |     |    
    点----点----点----点
    |     |     |     |  
    点----点----点----点
    |     |     |     |   
    点----点----点----点 U方向

    贝塞尔曲面，主要由16个控制点构成，八条三次bezier曲线构成。（八条三次bezier曲线统称面A）

    A会影响范围内所有的点，例如B点，在U方向做横线，便会与V方向的四个bezier曲面有四个交点，
    四个交点，也同时构成了一个bezier曲线，B点V坐标便会受这个这条曲线的影响，生成一个新的V
    坐标。同理，B点在V方向做横线，便会生成一个新的U方向坐标。

    这样当16个控制点移动时，B点的坐标也会进行相应的移动。

    live2d 只有12个控制点
    V方向
    点----点----点----点
    |  B  |     |     |    
    点----------------点
    |     |     |     |  
    点----------------点
    |     |     |     |   
    点----点----点----点 U方向

    两者唯一的区别是，live2d贝塞尔曲面是由，四条bezier曲线与四条直线，而不是八条bezier曲线。

    求B点的但是方法相同
```

算法
```javascript
// 计算线性贝塞尔曲线
function computeCubBezierLine(pArr, t) {
    const [P0, P1] = pArr;
    return P0 + (P1 - P0) * t;
}
// 计算三次贝塞尔曲线
function computeCubBezier(pArr, t) {
    const [P0, P1, P2, P3] = pArr;
    return (
        P0 * (1 - t) ** 3 +
        3 * P1 * t * (1 - t) ** 2 +
        3 * P2 * t ** 2 * (1 - t) +
        P3 * t ** 3
    );
}

// 计算 横向交点 与交点构成bzier曲面对该点得影响,获取新值
function middleX(gemArrX, xRate, yRate) {
  const x1 = computeCubBezier(
    [gemArrX[0], gemArrX[1], gemArrX[2], gemArrX[3]],
    xRate
  );
  const x2 = computeCubBezierLine([gemArrX[4], gemArrX[5]], xRate);
  const x3 = computeCubBezierLine([gemArrX[6], gemArrX[7]], xRate);
  const x4 = computeCubBezier(
    [gemArrX[8], gemArrX[9], gemArrX[10], gemArrX[11]],
    xRate
  );
  return computeCubBezier([x1, x2, x3, x4], yRate);
}
// 计算 纵向交点 与交点构成bzier曲面对该点得影响,获取新值
function middleY(gemArrY, yRate, xRate) {
  const x1 = computeCubBezier(
    [gemArrY[0], gemArrY[4], gemArrY[6], gemArrY[8]],
    yRate
  );
  const x2 = computeCubBezierLine([gemArrY[1], gemArrY[9]], yRate);
  const x3 = computeCubBezierLine([gemArrY[2], gemArrY[10]], yRate);
  const x4 = computeCubBezier(
    [gemArrY[3], gemArrY[5], gemArrY[7], gemArrY[11]],
    yRate
  );
  return computeCubBezier([x1, x2, x3, x4], xRate);
}


// 创建贝塞尔曲线, 输出更新后得点
function createBezierSingle(gemArr, rate) {
  const layer = gemArr[0][2];
  const gemArrX = gemArr.map((item) => {
    return item[0];
  }); // 获取顶点的x坐标数组
  const gemArrY = gemArr.map((item) => {
    return item[1];
  }); // 获取顶点得y坐标数组
  const arr = [];
  rate.forEach((item, index) => {
    const xRate = item[0];
    const yRate = item[1];
    if (xRate > 1 || xRate < 0 || yRate > 1 || yRate < 0) return;
    const xResult = middleX(gemArrX, xRate, yRate);
    const yResult = middleY(gemArrY, yRate, xRate);
    arr.push([xResult, yResult, layer, index]);
  });
  return arr;
}
```









