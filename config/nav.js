const fs = require('fs');

let data = fs.readdirSync('./docs')

let white = ['.vuepress', 'README.md']
let obj = [];
for (let i of data) {
  if (!white.includes(i)) {
    obj.push({
      text: i,
      link: `/${i}/`
    })
  }
}


module.exports = obj;